#ifndef COLOUR_RECT_H
#define COLOUR_RECT_H

#include "GUIElement.h"

class ColourRect : public GUIElement {
public:
	ColourRect(const vector2df& pos, const vector2df& size, const GLColour& col, GUIElement* parent = nullptr, bool force_mesh = false);
	~ColourRect();

	virtual void build();

	void set_colour(const GLColour& col);
	const GLColour& get_colour() const;

	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:
	virtual void update_mesh();

private:
	GLColour _colour;

	static std::unique_ptr<Texture> _texture;
};

#endif