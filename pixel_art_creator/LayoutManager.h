#ifndef LAYOUT_MANAGER_H
#define LAYOUT_MANAGER_H

#include "Button.h"
#include "ColourPicker.h"
#include "ColourRect.h"
#include "FileDialog.h"
#include "ImageLayerManipulator.h"
#include "ImagePreview.h"
#include "ItemList.h"
#include "MenuBar.h"
#include "Pane.h"
#include "ScrollView.h"
#include "Slider.h"
#include "TextEdit.h"

#include "EditState.h"

#include <functional>

class ProgramManager;

class LayoutManager {
public:
	LayoutManager(ProgramManager* program);

	void setup_gui(const vector2df& screen_size);
	void screen_resized(const vector2df& screen_size);

	void open_file_at_start(const std::string& filename);
	
protected:

private:
	ProgramManager* _program;
	ImageLayerManipulator* _image;

	// Any extra textures we might need
	std::unique_ptr<Texture> _eye_texture;
	std::unique_ptr<Texture> _paint_mode_texture;
	std::unique_ptr<Texture> _erase_mode_texture;
	std::unique_ptr<Texture> _fill_mode_texture;
	std::unique_ptr<Texture> _select_mode_texture;
	
	// Bottom pane
	Pane* _gui_bottom_pane;
	Text* _gui_zoom_text;
	Text* _gui_bottom_left_text;
	std::vector<Button*> _gui_mode_buttons;
	void arrange_mode_buttons(const vector2df& screen_size);

	// Top pane
	Pane* _gui_top_pane;
	MenuBar* _gui_menu_bar;

	// Left pane
	Pane* _gui_left_pane;
	ColourPicker* _gui_colour_picker;

	// Right pane
	Pane* _gui_right_pane;
	Button* _gui_new_layer;
	Button* _gui_delete_layer;
	ScrollView* _gui_layer_scroll;
	ItemList* _gui_layer_list;
	Slider* _gui_layer_alpha;
	TextEdit* _gui_layer_alpha_edit;
	void create_new_layer(const std::string& name);
	void delete_layer(size_t index);
	void move_layer(size_t index, size_t dest);

	// Misc
	WindowPane* _gui_preview_window;
	ImagePreview* _gui_image_preview;
	WindowPane* _gui_grid_window;
	WindowPane* _gui_resize_window;
	TextEdit* _gui_resize_x_edit;
	TextEdit* _gui_resize_y_edit;
	WindowPane* _gui_hsv_shift_window;
	Slider* _gui_hsv_shift_hue;
	Slider* _gui_hsv_shift_sat;
	Slider* _gui_hsv_shift_val;
	TextEdit* _gui_hsv_shift_hue_edit;
	TextEdit* _gui_hsv_shift_sat_edit;
	TextEdit* _gui_hsv_shift_val_edit;
	WindowPane* _gui_rotate_window;
	Slider* _gui_rotate;
	TextEdit* _gui_rotate_edit;
	FileDialog* _gui_file_dialog;

	vector2df _grid;
	float _grid_enabled;

	std::string _save_file_path;
	std::string _export_file_path;

	std::function<void(const std::string&)> _open_file_function;
	std::function<void(const std::string&)> _save_file_function;
	std::function<void(const std::string&)> _import_file_function;
	std::function<void(const std::string&)> _export_file_function;

	void open_file(const std::string& filename);
	void save_file(const std::string& filename);
	void import_file(const std::string& filename);
	void export_file(const std::string& filename);

	friend class LayerMoveState;
	friend class LayerImportState;
	friend class LayerExistenceState;
	friend class LayerDetailsState;
};

// EditStates handled by the LayoutManager
class LayerMoveState : public EditState {
public:
	LayerMoveState(LayoutManager* layout, size_t one, size_t two);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	LayoutManager* _layout;
	size_t _dest;
};

class LayerImportState : public EditState {
public:
	LayerImportState(LayoutManager* layout, const std::string& filename);

	virtual void undo();
	virtual void redo();

protected:

private:
	LayoutManager* _layout;
	std::string _filename;
	vector2df _prev_size;
};

class LayerExistenceState : public EditState {
public:
	LayerExistenceState(LayoutManager* layout, size_t index, const std::string& name, bool creation);

	virtual void undo();
	virtual void redo();

protected:

private:
	LayoutManager* _layout;
	bool _creation;
	std::vector<GLColour> _data;
	std::string _name;
	float _alpha;
	bool _visible;
	bool _one_layer;

	void create_layer();
};

class LayerDetailsState : public EditState {
public:
	LayerDetailsState(LayoutManager* layout, size_t index);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	LayoutManager* _layout;
	bool _original_visible;
	float _original_alpha;
	std::string _original_name;
	bool _new_visible;
	float _new_alpha;
	std::string _new_name;

	void set_visibility(bool visible);
	void set_alpha(float alpha);

	friend class LayoutManager;
};

#endif