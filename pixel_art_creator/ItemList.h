#ifndef ITEM_LIST_H
#define ITEM_LIST_H

#include "GUIElement.h"

#include <functional>

class ItemList : public GUIElement {
public:
	ItemList(const vector2df& pos, const vector2df& size, float row_size, GUIElement* parent = nullptr);
	~ItemList();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	size_t add_item(GUIElement* element, int64_t where = -1);
	void remove_item(size_t where);

	void clear();
	bool empty() const;

	void set_can_swap(bool swap);
	void set_move_instead_of_swap(bool move);
	void set_reverse(bool reverse);

	void select_item(size_t where);
	size_t get_selected_index() const;
	const std::vector<GUIElement*>& get_selected_item() const;
	const std::vector<GUIElement*>& get_item(size_t where) const;

	void swap_items(size_t one, size_t two, bool do_callback = true);
	void move_items(size_t one, size_t two, bool do_callback = true);

	size_t get_element_list_index(GUIElement* element) const;

	size_t get_list_size() const;

	void set_select_function(const std::function<void(size_t, bool)>& func);
	void set_swap_function(const std::function<void(size_t, size_t)>& func);
	void set_double_click_function(const std::function<void(size_t)>& func);

	virtual bool mouse_event(int button, int action, int mods);

protected:

private:
	float _row_size;
	size_t _selected;
	size_t _hovered;
	bool _mouse_hovered;
	bool _can_swap;
	bool _swapping;
	bool _move_instead_of_swap;
	bool _just_selected;
	bool _reverse;
	std::vector<std::vector<GUIElement*>> _list;

	std::function<void(size_t, bool)> _select_function;
	std::function<void(size_t, size_t)> _swap_function;
	std::function<void(size_t)> _double_click_function;

	size_t get_hovered_index() const;
};

#endif