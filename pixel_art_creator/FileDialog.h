#ifndef FILE_DIALOG_H
#define FILE_DIALOG_H

#include "WindowPane.h"

#include <filesystem>
#include <functional>

class Button;
class ItemList;
class ScrollView;
class TextEdit;

enum class FileDialogMode {
	file_open,
	file_save,
	file_save_as,
	file_import,
	file_export,
	file_export_as
};

class FileDialog : public WindowPane {
public:
	FileDialog(const vector2df& pos, const Font* font);
	~FileDialog();

	void list_current_directory();

	// Provide in the format ".png .jpg .bmp"
	void set_filters(const std::string& filters);

	void open(FileDialogMode mode);

	void set_ok_function(const std::function<void(const std::string&)>& func);

protected:

private:
	std::filesystem::path _path;
	std::vector<std::filesystem::path> _history;
	size_t _history_position;
	const Font* _font;
	FileDialogMode _mode;
	std::vector<std::string> _filters;
	std::string _previous_path_text;

	ScrollView* _directory_scroll;
	ItemList* _directory;
	Button* _back_button;
	Button* _forward_button;
	Button* _up_button;
	Button* _new_folder_button;
	Button* _ok_button;
	Button* _cancel_button;
	TextEdit* _path_text;
	TextEdit* _filename_text;
	WindowPane* _confirm_window;

	std::function<void(const std::string&)> _ok_function;

	std::unique_ptr<Texture> _folder_texture;
	std::unique_ptr<Texture> _png_file_texture;
	std::unique_ptr<Texture> _pxla_file_texture;

	void erase_the_future();
	void go_to_path(const std::filesystem::path& path);
};

#endif