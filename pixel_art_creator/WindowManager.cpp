#include "WindowManager.h"

#include <chrono>
#include <iostream>
#include <thread>

WindowManager::WindowManager()
	: _window(nullptr)
{

}

WindowManager::~WindowManager()
{
	if (_window)
		glfwDestroyWindow(_window);

	glfwTerminate();
}

// Initialises the window, including GLFW and GLEW
bool WindowManager::init()
{
	// GLFW init first
	if (!glfwInit())
		return false;

	// Read our configuration file
	///TODO: This

	// Create the window
	_window = glfwCreateWindow(1600, 900, "Pixel Art Creator", NULL, NULL);
	glfwSetWindowSizeLimits(_window, 1280, 720, GLFW_DONT_CARE, GLFW_DONT_CARE);

	// If we haven't successfully created the window quit now
	if (!_window)
	{
		std::cout << "Failed to create window" << std::endl;
		return false;
	}

	// Set the current context
	glfwMakeContextCurrent(_window);

	// Set up V-Sync
	glfwSwapInterval(1);

	// Initialise GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return false;

	// Enable transparency in textures
	glEnable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	// Initialise the program
	if (!ProgramManager::instance().init())
		return false;

	// Create the GUI
	ProgramManager::instance().screen_resized(_window);
	ProgramManager::instance().setup_gui();

	// Set up callbacks for window events
	glfwSetWindowUserPointer(_window, this);

	glfwSetWindowFocusCallback(_window, [](GLFWwindow* window, int focus) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			man->screen_resized();
		}
	});
	glfwSetWindowSizeCallback(_window, [](GLFWwindow* window, int width, int height) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			man->screen_resized();
		}
	});

	// Set up callbacks for input events
	static std::chrono::time_point previous_click = std::chrono::system_clock::now();
	static int previous_button = GLFW_MOUSE_BUTTON_LEFT;
	glfwSetMouseButtonCallback(_window, [](GLFWwindow* window, int button, int action, int mods) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			if (action == GLFW_PRESS && button == previous_button)
			{
				std::chrono::time_point time_now = std::chrono::system_clock::now();

				if ((std::chrono::duration_cast<std::chrono::milliseconds>(time_now.time_since_epoch()) -
					std::chrono::duration_cast<std::chrono::milliseconds>(previous_click.time_since_epoch())).count() < 240)
					action = GLFW_DOUBLE_CLICK;

				previous_click = time_now;
			}
			previous_button = button;
			
			man->mouse_event(button, action, mods);
		}
	});
	glfwSetScrollCallback(_window, [](GLFWwindow* window, double x, double y) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			man->scroll_event(x, y);
		}
	});
	glfwSetKeyCallback(_window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			man->key_event(key, scancode, action, mods);
		}
	});
	glfwSetCharCallback(_window, [](GLFWwindow* window, unsigned int codepoint) {
		WindowManager* man = (WindowManager*)glfwGetWindowUserPointer(window);
		if (man)
		{
			man->char_event(codepoint);
		}
	});

	return true;
}

// The main loop of the program
void WindowManager::run(const std::string& filename)
{
	bool is_dirty = true;
	double mouse_x, mouse_y;
	
	if (!filename.empty())
		ProgramManager::instance().open_file_at_start(filename);

	while (!glfwWindowShouldClose(_window) && ProgramManager::instance().is_running())
	{
		// Get the mouse coordinates
		glfwGetCursorPos(_window, &mouse_x, &mouse_y);
		
		// Run the update for the program
		is_dirty = ProgramManager::instance().update_loop(static_cast<int>(mouse_x), static_cast<int>(mouse_y));

		// Update the mouse cursor if we need to
		ProgramManager::instance().set_mouse_cursor(_window);

		// Run the render for the program
		// If nothing has changed, we will not swap buffers and will instead sleep this thread for a short period
		// Hopefully this keeps GPU and CPU usage down when we're not actively using the program
		if (is_dirty)
		{
			ProgramManager::instance().render_loop();
			glfwSwapBuffers(_window);
			is_dirty = false;
		}
		else
			std::this_thread::sleep_for(std::chrono::milliseconds(16));

		// Poll for events (for input etc)
		glfwPollEvents();
	}
}

void WindowManager::screen_resized()
{
	// We need to redraw during this as PollEvents is blocked by the resizing
	ProgramManager::instance().screen_resized(_window);

	ProgramManager::instance().update_loop(-1, -1);
	ProgramManager::instance().render_loop();
	glfwSwapBuffers(_window);
}

void WindowManager::mouse_event(int button, int action, int mods)
{
	ProgramManager::instance().mouse_event(button, action, mods);
}

void WindowManager::scroll_event(double x, double y)
{
	ProgramManager::instance().scroll_event(x, y);
}

void WindowManager::key_event(int key, int scancode, int action, int mods)
{
	ProgramManager::instance().key_event(key, scancode, action, mods);
}

void WindowManager::char_event(unsigned int codepoint)
{
	ProgramManager::instance().char_event(codepoint);
}
