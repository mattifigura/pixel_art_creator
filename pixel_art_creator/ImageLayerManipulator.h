#ifndef IMAGE_LAYER_MANIPULATOR_H
#define IMAGE_LAYER_MANIPULATOR_H

#include "ImageLayer.h"
#include "EditState.h"

#include <functional>

class ColourRect;

enum class ManipulationMode {
	paint = 0,
	erase = 1,
	fill = 2,
	select = 3,
	line = 4,
	rect = 5,
	circle = 6
};

// This is a "GUIElement" which handles the manipulation of each layer - ie this class controls the image editing process

class ImageLayerManipulator : public GUIElement {
public:
	ImageLayerManipulator(const vector2df& screen_size);
	~ImageLayerManipulator();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	void add_layer();
	void remove_layer(size_t index);

	void set_active_layer(size_t index);
	size_t get_active_layer() const;

	const ImageLayer* get_layer(size_t index) const;
	const std::vector<ImageLayer*>& get_layers() const;

	void swap_layers(size_t one, size_t two);
	void move_layer(size_t original, size_t dest);

	void set_layer_size(const vector2di& size, bool resize_layers = true, bool from_state_change = false);
	const vector2df& get_layer_size() const;

	void set_layer_alpha(size_t index, float alpha);
	void set_layer_visibility(size_t index, bool visible);

	void load_from_buffer(const std::vector<GLColour>& data, int width, int height);
	void load_from_buffer(const GLColour* data, int width, int height);

	float get_zoom() const;

	void set_paint_colour(const GLColour& col);
	const GLColour& get_paint_colour() const;

	void get_complete_image_buffer(std::vector<GLColour>& data) const;
	void get_working_hsv();
	void hsv_shift(float hue_shift, float sat_shift, float val_shift);
	void reset_adjustments(bool finished);
	void finished_adjustments();

	void copy_selection(bool cut = false);
	void paste_selection();
	void delete_selection();

	void flip(bool vertical);

	void start_rotation();
	void rotate(float degrees);
	void finish_rotation(bool save);

	void set_mode(ManipulationMode mode);

	void set_zoom_function_callback(const std::function<void(float)>& func);
	void set_colour_picked_function_callback(const std::function<void(const GLColour&)>& func);
	void set_layer_size_changed_function_callback(const std::function<void(const vector2di&)>& func);
	void set_mode_changed_function_callback(const std::function<void(ManipulationMode)>& func);
	void set_mouse_position_function_callback(const std::function<void(const vector2di&)>& func);

	virtual bool mouse_event(int button, int action, int mods);
	virtual bool scroll_event(double x, double y);
	virtual void screen_resized(int width, int height);
	virtual bool key_event(int key, int scancode, int action, int mods);
	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:

private:
	vector2df _layer_size;
	vector2df _layer_position;
	vector2df _layer_adjust;
	std::unique_ptr<Texture> _texture;

	ManipulationMode _mode;

	vector2di _pixel_position;
	vector2di _pixel_edit_position;
	vector2di _previous_mouse_position;
	GLColour _paint_colour;

	std::vector<ImageLayer*> _layers;
	size_t _active_layer;
	std::vector<bool> _painted;
	std::vector<HSV> _hsv_pixels;

	float _zoom;
	int _zoom_index;
	float _previous_zoom;
	bool _moving_perspective;
	bool _mouse_down;
	bool _just_clicked;
	bool _cursor_visible;
	bool _working_on_selection;

	vector2di _select_start;
	vector2di _select_end;
	std::vector<GLColour> _copied_pixels;
	vector2di _copied_size;
	std::unique_ptr<Texture> _pasted_pixels;
	ColourRect* _pasted_image;
	vector2di _paste_move_offset;

	std::vector<GLColour> _rotate_pixels;
	std::unique_ptr<Texture> _rotate_texture;
	ColourRect* _rotate_image;
	float _selection_expand;

	std::function<void(float)> _zoom_callback;
	std::function<void(const GLColour&)> _colour_picked_callback;
	std::function<void(const vector2di&)> _layer_size_changed_callback;
	std::function<void(ManipulationMode)> _mode_changed_callback;
	std::function<void(const vector2di&)> _mouse_position_callback;

	EditState* _working_state;

	inline ImageLayer* active_layer() { return _layers[_active_layer]; }

	void update_mesh(bool visible, bool update_border);

	void fill();
	void write_pasted();

	friend class PaintState;
	friend class FillState;
	friend class AreaState;
	friend class SizeState;
	friend class FlipState;
};

// EditStates related to image data
class PaintState : public EditState {
public:
	PaintState(ImageLayerManipulator* image);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	ImageLayerManipulator* _image;
	std::vector<vector2di> _positions;
	GLColour _painted_colour;
	std::vector<GLColour> _original_colours;
	bool _erasing;

	friend class ImageLayerManipulator;
};

class FillState : public EditState {
public:
	FillState(ImageLayerManipulator* image);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	ImageLayerManipulator* _image;
	std::vector<vector2di> _positions;
	GLColour _filled_colour;
	GLColour _original_colour;

	void fill(const GLColour& col);

	friend class ImageLayerManipulator;
};

class AreaState : public EditState {
public:
	AreaState(ImageLayerManipulator* image);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	ImageLayerManipulator* _image;
	vector2di _start_position;
	vector2di _size;
	std::vector<GLColour> _original;
	std::vector<GLColour> _new;

	void update_area(const std::vector<GLColour>& data);

	friend class ImageLayerManipulator;
};

class SizeState : public EditState {
public:
	SizeState(ImageLayerManipulator* image);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();
	
protected:

private:
	ImageLayerManipulator* _image;
	vector2di _original;
	vector2di _new;
	std::vector<std::vector<GLColour>> _original_data;

	friend class ImageLayerManipulator;
};

class FlipState : public EditState {
public:
	FlipState(ImageLayerManipulator* image, bool vertical);

	virtual void undo();
	virtual void redo();

	virtual bool validate_state();

protected:

private:
	ImageLayerManipulator* _image;
	bool _vertical;
	vector2di _start;
	vector2di _size;

	void flip();
};

#endif