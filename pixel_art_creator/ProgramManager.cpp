#include "ProgramManager.h"
#include "FileManager.h"
#include "FontManager.h"
#include "Util.h"

#include <algorithm>
#include <ranges>
#include <sstream>

ProgramManager::ProgramManager()
	: _vao(0u)
	, _dirty(true)
	, _shader()
	, _cull_box_uniform(-1)
	, _grid_uniform(-1)
	, _alpha_uniform(-1)
	, _mouse_cursor(cursor::last_cursor)
	, _cursors()
	, _running(true)
	, _need_to_sort(false)
	, _layout(this)
	, _screen_size()
	, _grid()
	, _elements()
	, _element_add_queue()
	, _default_texture()
	, _lock(nullptr)
{

}

ProgramManager::~ProgramManager()
{
	
}

bool ProgramManager::init()
{
	// Initialise the shaders used for this application
	std::string vertex_shader, fragment_shader;
	
	if (util::file_to_string(FileManager::_executable_directory + "/media/vertex_shader", vertex_shader) && util::file_to_string(FileManager::_executable_directory + "/media/fragment_shader", fragment_shader))
		_shader.set_shaders(vertex_shader, fragment_shader);

	if (!_shader.compile_shaders())
		return false;
	
	// We only need 1 VAO for this application, as we are only using 1 shader and 1 vertex format
	// So we will set it up here and bind different VBOs when we are needing to draw
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	_shader.bind();
	glUniform1i(_shader.get_uniform_location("tex_image"), 0);
	_cull_box_uniform = _shader.get_uniform_location("cull_box");
	_grid_uniform = _shader.get_uniform_location("grid");
	_alpha_uniform = _shader.get_uniform_location("alpha");

	int one = _shader.get_attrib_location("position");
	int two = _shader.get_attrib_location("colour");
	int three = _shader.get_attrib_location("tex_coord");

	glVertexAttribFormat(one, 2, GL_FLOAT, GL_FALSE, offsetof(GLVertex2D, pos));
	glVertexAttribBinding(one, 0);
	glEnableVertexAttribArray(one);

	glVertexAttribFormat(two, 4, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(GLVertex2D, col));
	glVertexAttribBinding(two, 0);
	glEnableVertexAttribArray(two);

	glVertexAttribFormat(three, 2, GL_SHORT, GL_FALSE, offsetof(GLVertex2D, uv));
	glVertexAttribBinding(three, 0);
	glEnableVertexAttribArray(three);

	// Create our default texture
	//_default_texture.create_colour_image(16, 16, GLColour(255, 255, 255, 255));
	_default_texture.create_gradient_image(64, 64, GLColour(150, 150, 150, 255), GLColour());

	// Load our mouse cursors
	int width, height;
	std::vector<GLColour> cursor_image;
	if (!FileManager::read_png(FileManager::_executable_directory + "media/cursors.png", cursor_image, width, height)) // This image was made in this program :)
		return false;

	GLFWimage image;
	unsigned char pixels[16 * 16 * 4];
	image.width = 16;
	image.height = 16;

	auto copy_cursor_to_pixels = [&](size_t x, size_t y) {
		for (size_t i = 0; i < 16; ++i) {
			for (size_t j = 0; j < 16; ++j) {
				const GLColour col = cursor_image[(y + j) * static_cast<size_t>(width) + (x + i)];
				pixels[j * (16 * 4) + (i * 4)] = col.r;
				pixels[j * (16 * 4) + (i * 4) + 1] = col.g;
				pixels[j * (16 * 4) + (i * 4) + 2] = col.b;
				pixels[j * (16 * 4) + (i * 4) + 3] = col.a;
			}
		}
		image.pixels = pixels;
	};

	// cursor::arrow
	copy_cursor_to_pixels(0, 0);
	_cursors.push_back(glfwCreateCursor(&image, 0, 0));

	// cursor::hresize
	copy_cursor_to_pixels(16, 0);
	_cursors.push_back(glfwCreateCursor(&image, 8, 8));

	// cursor::vresize
	copy_cursor_to_pixels(32, 0);
	_cursors.push_back(glfwCreateCursor(&image, 8, 8));

	// cursor::tlbrresize
	copy_cursor_to_pixels(48, 0);
	_cursors.push_back(glfwCreateCursor(&image, 8, 8));

	// cursor::trblresize
	copy_cursor_to_pixels(0, 16);
	_cursors.push_back(glfwCreateCursor(&image, 8, 8));

	// cursor::ibeam
	copy_cursor_to_pixels(16, 16);
	_cursors.push_back(glfwCreateCursor(&image, 8, 8));

	// cursor::erase
	copy_cursor_to_pixels(32, 16);
	_cursors.push_back(glfwCreateCursor(&image, 0, 0));

	// cursor::fill
	copy_cursor_to_pixels(48, 16);
	_cursors.push_back(glfwCreateCursor(&image, 3, 11));

	// cursor::select
	copy_cursor_to_pixels(0, 32);
	_cursors.push_back(glfwCreateCursor(&image, 7, 7));

	// Success
	return true;
}

void ProgramManager::setup_gui()
{
	_layout.setup_gui(static_cast<vector2df>(_screen_size));
}

bool ProgramManager::update_loop(int mouse_x, int mouse_y)
{
	// Create any new elements here
	if (!_element_add_queue.empty())
	{
		for (auto& element : _element_add_queue) {
			add_gui_element(element);
		}
		_element_add_queue.clear();
		_element_add_queue.shrink_to_fit();
	}
	
	// Do we need to sort?
	if (_need_to_sort)
		sort();

	// Have to use the older iterator method to update our elements in case we need to kill any
	bool removed_elements = false;
	std::vector<std::shared_ptr<GUIElement>>::iterator it = _elements.begin();
	while (it != _elements.end())
	{
		GUIElement* element = (*it).get();
		if (element->is_alive())
		{
			_dirty |= element->update(mouse_x, mouse_y);
			++it;
		}
		else
		{
			it = _elements.erase(it);
			removed_elements = true;
		}
	}
	
	if (removed_elements)
		_elements.shrink_to_fit();

	return _dirty;
}

void ProgramManager::render_loop()
{
	glClearColor(0.6f, 0.6f, 0.6f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	GLuint texture = -1;
	vector4df default_cull_box(0.f, 0.f, static_cast<float>(_screen_size.x), static_cast<float>(_screen_size.y));
	vector4df cull_box(default_cull_box);

	std::ranges::reverse_view elements_reverse(_elements);
	for (auto& element : elements_reverse) {
		if (!element->is_visible())
			continue;

		const Mesh* mesh = element->get_mesh_ptr();

		if (mesh && VALID_BUFFER(mesh->get_vbo()))
		{
			// Change texture if we need to
			const Texture* tex = element->get_texture();
			GLuint new_texture = 0;

			if (tex)
				new_texture = tex->get_index();

			if (new_texture == 0)
				new_texture = _default_texture.get_index();

			if (new_texture != texture)
			{
				texture = new_texture;
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture);
			}

			if (!element->get_cull_box(cull_box))
				cull_box = default_cull_box;

			if (dynamic_cast<ImageLayer*>(element.get()) && _grid.x > 0 && _grid.y > 0)
				glUniform2fv(_grid_uniform, 1, &_grid.x);
			else
			{
				static vector2df no_grid;
				glUniform2fv(_grid_uniform, 1, &no_grid.x);
			}

			glUniform1f(_alpha_uniform, element->get_alpha());

			glUniform4fv(_cull_box_uniform, 1, &cull_box.x);
			glUniformMatrix4fv(_shader.get_uniform_location("model"), 1, GL_TRUE, element->get_transform().get_data_pointer());
			glBindVertexBuffer(0, mesh->get_vbo(), 0, sizeof(GLVertex2D));
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->get_ebo());
			glDrawElements(GL_TRIANGLES, mesh->get_index_buffer_size(), GL_UNSIGNED_INT, 0);
		}
	}

	// We have now redrawn and are no longer dirty
	glBindVertexBuffer(0, 0, 0, sizeof(GLVertex2D));
	_dirty = false;
}

void ProgramManager::stop_running()
{
	_running = false;
}

bool ProgramManager::is_running() const
{
	return _running;
}

void ProgramManager::screen_resized(GLFWwindow* window)
{
	// We need to update the projection matrix of our shader, and the viewport
	vector2di prev_screen_size(_screen_size);
	glfwGetWindowSize(window, &_screen_size.x, &_screen_size.y);

	if (_screen_size.x < 1280 || _screen_size.y < 720)
		_screen_size = prev_screen_size;

	_shader.set_ortho_projection(_screen_size.x, _screen_size.y);
	glViewport(0, 0, _screen_size.x, _screen_size.y);

	// Our layout manager will need to update
	_layout.screen_resized(static_cast<vector2df>(_screen_size));

	// Pass the event onto our elements
	for (auto& element : _elements) {
		element->screen_resized(_screen_size.x, _screen_size.y);
	}

	// Rebind the shader as it get unbound by the set_ortho call
	_shader.bind();
	_dirty = true;
}

GUIElement* ProgramManager::add_gui_element(GUIElement* element)
{
	_elements.push_back(std::shared_ptr<GUIElement>(element));
	element->build();

	// Now go through its children and add them
	for (auto child_element : element->get_children()) {
		add_gui_element(child_element);
	}

	sort();

	return element;
}

GUIElement* ProgramManager::add_gui_element_queued(GUIElement* element)
{
	_element_add_queue.push_back(element);

	return element;
}

bool ProgramManager::lock(GUIElement* element)
{
	if (!_lock || _lock == element)
	{
		_lock = element;
		return true;
	}

	return false;
}

bool ProgramManager::unlock(GUIElement* element)
{
	if (element == _lock)
	{
		_lock = nullptr;
		return true;
	}

	return false;
}

GUIElement* ProgramManager::get_lock() const
{
	return _lock;
}

void ProgramManager::sort_gui_elements()
{
	_need_to_sort = true;
}

void ProgramManager::sort()
{
	std::sort(_elements.begin(), _elements.end(), [](const std::shared_ptr<GUIElement>& one, const std::shared_ptr<GUIElement>& two) {
		return (*one < *two);
	});
	_need_to_sort = false;
}

void ProgramManager::set_grid(const vector2df& grid)
{
	_grid = grid;
	_dirty = true;
}

const vector2df& ProgramManager::get_grid() const
{
	return _grid;
}

void ProgramManager::mouse_event(int button, int action, int mods)
{
	for (auto& element : _elements) {
		if (element->is_visible() && (!_lock || _lock == element.get()) && element->mouse_event(button, action, mods))
			break;
	}
}

void ProgramManager::scroll_event(double x, double y)
{
	for (auto& element : _elements) {
		if (element->is_visible() && (!_lock || _lock == element.get()) && element->scroll_event(x, y))
			break;
	}
}

void ProgramManager::key_event(int key, int scancode, int action, int mods)
{
	for (auto& element : _elements) {
		if (element->is_visible() && (!_lock || _lock == element.get()) && element->key_event(key, scancode, action, mods))
			break;
	}
}

void ProgramManager::char_event(unsigned int codepoint)
{
	for (auto& element : _elements) {
		if (element->is_visible() && (!_lock || _lock == element.get()) && element->char_event(codepoint))
			break;
	}
}

void ProgramManager::set_mouse_cursor(GLFWwindow* window)
{
	size_t current_cursor = _mouse_cursor;
	for (auto& element : _elements) {
		if (element->is_visible() && (!_lock || _lock == element.get()) && element->set_mouse_cursor(_mouse_cursor))
			break;
	}

	if (_mouse_cursor > cursor::last_cursor)
		_mouse_cursor = cursor::arrow;

	if (current_cursor != _mouse_cursor)
	{
		glfwSetCursor(window, _cursors[_mouse_cursor]);
	}
}

void ProgramManager::open_file_at_start(const std::string& filename)
{
	_layout.open_file_at_start(filename);
}