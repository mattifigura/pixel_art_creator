#include "WindowManager.h"
#include "FileManager.h"

#include <iostream>

#ifdef _WIN32
#ifdef NDEBUG
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif
#endif

int main(int argc, char** argv)
{
	std::cout << "Pixel Art Creator" << std::endl;

	// Check if we've been passed a filename to open
	std::string filename;
	if (argc > 1)
	{
		filename = argv[1];
		std::cout << "Opening file " << filename << std::endl;
	}

	// Set the executable directory - if this is a debug build, use current directory so I don't have to copy the media folder
#ifdef NDEBUG
	FileManager::_executable_directory = std::filesystem::path(argv[0]).parent_path().string();
#else
	FileManager::_executable_directory = std::filesystem::current_path().string();
#endif

#ifdef _WIN32
	FileManager::_executable_directory += "\\";
#else
	FileManager::_executable_directory += "/";
#endif

	// Create and run the application
	WindowManager window_man;
	if (window_man.init())
		window_man.run(filename);
	else
		return -1;

	return 0;
}