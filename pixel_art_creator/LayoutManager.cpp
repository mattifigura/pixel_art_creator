#include "FileManager.h"
#include "EditStateManager.h"
#include "FontManager.h"
#include "LayoutManager.h"
#include "ProgramManager.h"
#include "Util.h"

#include <sstream>

LayoutManager::LayoutManager(ProgramManager* program)
	: _program(program)
	, _image(nullptr)
	, _eye_texture(new Texture)
	, _paint_mode_texture(new Texture)
	, _erase_mode_texture(new Texture)
	, _fill_mode_texture(new Texture)
	, _select_mode_texture(new Texture)
	, _gui_bottom_pane(nullptr)
	, _gui_zoom_text(nullptr)
	, _gui_bottom_left_text(nullptr)
	, _gui_mode_buttons()
	, _gui_top_pane(nullptr)
	, _gui_menu_bar(nullptr)
	, _gui_left_pane(nullptr)
	, _gui_colour_picker(nullptr)
	, _gui_right_pane(nullptr)
	, _gui_new_layer(nullptr)
	, _gui_delete_layer(nullptr)
	, _gui_layer_scroll(nullptr)
	, _gui_layer_list(nullptr)
	, _gui_layer_alpha(nullptr)
	, _gui_layer_alpha_edit(nullptr)
	, _gui_preview_window(nullptr)
	, _gui_image_preview(nullptr)
	, _gui_grid_window(nullptr)
	, _gui_resize_window(nullptr)
	, _gui_resize_x_edit(nullptr)
	, _gui_resize_y_edit(nullptr)
	, _gui_hsv_shift_window(nullptr)
	, _gui_hsv_shift_hue(nullptr)
	, _gui_hsv_shift_sat(nullptr)
	, _gui_hsv_shift_val(nullptr)
	, _gui_hsv_shift_hue_edit(nullptr)
	, _gui_hsv_shift_sat_edit(nullptr)
	, _gui_hsv_shift_val_edit(nullptr)
	, _gui_rotate_window(nullptr)
	, _gui_rotate(nullptr)
	, _gui_rotate_edit(nullptr)
	, _gui_file_dialog(nullptr)
	, _grid(16.f, 16.f)
	, _grid_enabled(0.f)
	, _save_file_path("")
	, _export_file_path("")
	, _open_file_function(nullptr)
	, _save_file_function(nullptr)
	, _import_file_function(nullptr)
	, _export_file_function(nullptr)
{

}

void LayoutManager::setup_gui(const vector2df& screen_size)
{
	// Load our font
	const Font& font = FontManager::instance().get_resource(FileManager::_executable_directory + "media/OpenSans.ttf:{16");

	// Load any textures we need
	auto load_texture = [](const std::string& filename, std::unique_ptr<Texture>& tex) {
		int tex_width, tex_height;
		std::vector<GLColour> tex_data;
		if (FileManager::read_png(FileManager::_executable_directory + "media/" + filename, tex_data, tex_width, tex_height))
			tex->create_texture_from_buffer(tex_data, tex_width, tex_height);
		else
			tex.reset(nullptr);
	};

	load_texture("eye.png", _eye_texture);

	// Time to create the GUI of the application!
	_image = new ImageLayerManipulator(screen_size);
	_image->set_colour_picked_function_callback([this](const GLColour& col) {
		_gui_colour_picker->set_colour(col);
	});
	_image->set_zoom_function_callback([this](float zoom) {
		std::stringstream ss;
		ss << "Zoom: " << zoom << "x";

		_gui_zoom_text->set_text(ss.str());

		_program->set_grid(_grid * zoom * _grid_enabled);
	});
	_image->set_layer_size_changed_function_callback([this](const vector2di& size) {
		_gui_image_preview->set_layer_size(size);

		// Adjust the layer size in the bottom left text
		std::string text = _gui_bottom_left_text->get_text();
		text = text.substr(text.find_first_of('|'));
		_gui_bottom_left_text->set_text("Image: " + std::to_string(size.x) + " x " + std::to_string(size.y) + "px " + text);

		_gui_resize_x_edit->set_text(std::to_string(size.x));
		_gui_resize_y_edit->set_text(std::to_string(size.y));
	});
	_image->set_mode_changed_function_callback([this](ManipulationMode mode) {
		auto mode_num = static_cast<size_t>(mode);

		for (size_t i = 0; i < _gui_mode_buttons.size(); ++i) {
			_gui_mode_buttons[i]->set_pressed(i == mode_num);
		}
	});
	_image->set_mouse_position_function_callback([this](const vector2di& pos) {
		// Adjust the cursor position in the bottom left text
		std::string text = _gui_bottom_left_text->get_text();
		text = text.substr(0, text.find_last_of('|'));
		_gui_bottom_left_text->set_text(text + "| Cursor: " + std::to_string(pos.x) + ", " + std::to_string(pos.y));
		_gui_bottom_left_text->buffer();
	});
	_program->add_gui_element(_image);


	// Add a bottom pane
	_gui_bottom_pane = dynamic_cast<Pane*>(_program->add_gui_element(new Pane(vector2df(0.f, screen_size.y - 32.f), vector2df(screen_size.x, 32.f))));
	_gui_bottom_pane->set_mouse_block(GLFW_PRESS, true);
	_gui_bottom_pane->set_mouse_block(GLFW_DOUBLE_CLICK, true);

	_gui_zoom_text = new Text(vector2df(screen_size.x - 200.f, 6.f), vector2df(190.f, 24.f), "Zoom: 8x", &font, _gui_bottom_pane);
	_gui_zoom_text->set_alignment(TextAlignment::right, false);
	_gui_zoom_text->set_shadowed(true, false);
	_program->add_gui_element(_gui_zoom_text);

	_gui_bottom_left_text = new Text(vector2df(8.f, 6.f), vector2df(screen_size.x, 24.f), "Image: 64 x 64px | Grid: 16 x 16px | Cursor: 0, 0", &font, _gui_bottom_pane);
	_gui_bottom_left_text->set_shadowed(true, false);
	_program->add_gui_element(_gui_bottom_left_text);


	// Mode change buttons
	auto new_mode_change_button = [&font, load_texture, this](const std::string& filename, std::unique_ptr<Texture>& tex) {
		load_texture(filename, tex);

		Button* button = new Button(vector2df(), vector2df(24.f, 24.f), filename.substr(0, 1), &font, _gui_bottom_pane);
		button->set_image_mode(true, tex.get());
		button->set_toggle_mode(true);

		size_t index = _gui_mode_buttons.size();
		button->set_function([index, this](GUIElement* button, bool ignored) {
			_image->set_mode(static_cast<ManipulationMode>(index));
		});

		_program->add_gui_element(button);
		_gui_mode_buttons.push_back(button);
	};

	new_mode_change_button("paint_button.png", _paint_mode_texture);
	new_mode_change_button("erase_button.png", _erase_mode_texture);
	new_mode_change_button("fill_button.png", _fill_mode_texture);
	new_mode_change_button("select_button.png", _select_mode_texture);

	arrange_mode_buttons(screen_size);
	_gui_mode_buttons[0]->set_pressed(true);


	// Add a top pane
	_gui_top_pane = dynamic_cast<Pane*>(_program->add_gui_element(new Pane(vector2df(), vector2df(screen_size.x, 32.f))));
	_gui_top_pane->set_depth(10000);
	_gui_top_pane->set_mouse_block(GLFW_PRESS, true);
	_gui_top_pane->set_mouse_block(GLFW_DOUBLE_CLICK, true);

	_gui_menu_bar = new MenuBar(vector2df(4.f, 0.f), vector2df(0.f, 32.f), &font, _gui_top_pane);
	_program->add_gui_element(_gui_menu_bar);

	_gui_menu_bar->add_menu("File");
	_gui_menu_bar->add_menu_item("File", "New", GLFW_KEY_N, GLFW_MOD_CONTROL);
	_gui_menu_bar->add_menu_item("File", "Open", GLFW_KEY_O, GLFW_MOD_CONTROL, [this]() {
		_gui_file_dialog->set_ok_function(_open_file_function);
		_gui_file_dialog->set_filters(".pxla");
		_gui_file_dialog->open(FileDialogMode::file_open);
	});
	_gui_menu_bar->add_menu_item("File", "Save", GLFW_KEY_S, GLFW_MOD_CONTROL, [this]() {
		if (_save_file_path.empty())
		{
			_gui_file_dialog->set_ok_function(_save_file_function);
			_gui_file_dialog->set_filters(".pxla");
			_gui_file_dialog->open(FileDialogMode::file_save);
		}
		else
			_save_file_function(_save_file_path);
	});
	_gui_menu_bar->add_menu_item("File", "Save As", GLFW_KEY_S, GLFW_MOD_CONTROL | GLFW_MOD_SHIFT, [this]() {
		_gui_file_dialog->set_ok_function(_save_file_function);
		_gui_file_dialog->set_filters(".pxla");
		_gui_file_dialog->open(FileDialogMode::file_save_as);
	});
	_gui_menu_bar->add_menu_item("File", "Import", GLFW_KEY_I, GLFW_MOD_CONTROL, [this]() {
		_gui_file_dialog->set_ok_function(_import_file_function);
		_gui_file_dialog->set_filters(".png");
		_gui_file_dialog->open(FileDialogMode::file_import);
	});
	_gui_menu_bar->add_menu_item("File", "Export", GLFW_KEY_E, GLFW_MOD_CONTROL, [this]() {
		if (_export_file_path.empty())
		{
			_gui_file_dialog->set_ok_function(_export_file_function);
			_gui_file_dialog->set_filters(".png");
			_gui_file_dialog->open(FileDialogMode::file_export);
		}
		else
			_export_file_function(_export_file_path);
	});
	_gui_menu_bar->add_menu_item("File", "Export As", GLFW_KEY_E, GLFW_MOD_CONTROL | GLFW_MOD_SHIFT, [this]() {
		_gui_file_dialog->set_ok_function(_export_file_function);
		_gui_file_dialog->set_filters(".png");
		_gui_file_dialog->open(FileDialogMode::file_export_as);
	});
	_gui_menu_bar->add_menu_item("File", "Exit", GLFW_KEY_Q, GLFW_MOD_CONTROL, [this]() { _program->stop_running(); });

	_gui_menu_bar->add_menu("Edit");
	_gui_menu_bar->add_menu_item("Edit", "Undo", GLFW_KEY_Z, GLFW_MOD_CONTROL, []() { EditStateManager::instance().undo(); });
	_gui_menu_bar->add_menu_item("Edit", "Redo", GLFW_KEY_Y, GLFW_MOD_CONTROL, []() { EditStateManager::instance().redo(); });
	_gui_menu_bar->add_menu_item("Edit", "Copy", GLFW_KEY_C, GLFW_MOD_CONTROL, [this]() { _image->copy_selection(); });
	_gui_menu_bar->add_menu_item("Edit", "Cut", GLFW_KEY_X, GLFW_MOD_CONTROL, [this]() { _image->copy_selection(true); });
	_gui_menu_bar->add_menu_item("Edit", "Paste", GLFW_KEY_V, GLFW_MOD_CONTROL, [this]() { _image->paste_selection(); });
	_gui_menu_bar->add_menu_item("Edit", "Delete", GLFW_KEY_DELETE, -1, [this]() { _image->delete_selection(); });

	_gui_menu_bar->add_menu("View");
	_gui_menu_bar->add_menu_item("View", "Preview Window", GLFW_KEY_P, -1, [this]() {
		_gui_preview_window->set_visible(!_gui_preview_window->is_visible());
		_gui_image_preview->update_visibility(_image->get_layers());
	});
	_gui_menu_bar->add_menu_item("View", "Grid", GLFW_KEY_G, -1, [this]() {
		_grid_enabled = 1.f - _grid_enabled;
		_program->set_grid(_grid * _image->get_zoom() * _grid_enabled);
	});
	_gui_menu_bar->add_menu_item("View", "Configure Grid", GLFW_KEY_G, GLFW_MOD_CONTROL, [this]() { _gui_grid_window->set_visible(!_gui_grid_window->is_visible()); });

	_gui_menu_bar->add_menu("Tools");
	_gui_menu_bar->add_menu_item("Tools", "Options", GLFW_KEY_F1);
	_gui_menu_bar->add_menu_item("Tools", "Resize Image", -1, -1, [this]() { _gui_resize_window->set_visible(true); });
	_gui_menu_bar->add_menu_item("Tools", "HSV Shift", -1, -1, [this]() {
		_gui_hsv_shift_window->set_visible(true);
		_image->get_working_hsv();
	});
	_gui_menu_bar->add_menu_item("Tools", "Flip Horizontal", -1, -1, [this]() { _image->flip(false); });
	_gui_menu_bar->add_menu_item("Tools", "Flip Vertical", -1, -1, [this]() { _image->flip(true); });
	_gui_menu_bar->add_menu_item("Tools", "Rotate", -1, -1, [this]() {
		_gui_rotate_window->set_visible(true);
		_image->start_rotation();
	});

	_gui_menu_bar->add_menu("Help");
	_gui_menu_bar->add_menu_item("Help", "About", GLFW_KEY_F12);


	// Add a left pane
	_gui_left_pane = dynamic_cast<Pane*>(_program->add_gui_element(new Pane(vector2df(0.f, 32.f), vector2df(296.f, screen_size.y - 64.f))));
	_gui_left_pane->set_mouse_block(GLFW_PRESS, true);
	_gui_left_pane->set_mouse_block(GLFW_DOUBLE_CLICK, true);

	_gui_colour_picker = new ColourPicker(vector2df(8.f, 8.f), _gui_left_pane);
	_gui_colour_picker->set_colour_picked_function_callback([this](const GLColour& col) {
		_image->set_paint_colour(col);
	});
	_program->add_gui_element(_gui_colour_picker);


	// Temporary button to randomise the colour picker
	Button* random_button = new Button(vector2df(8.f, 406.f), vector2df(280.f, 32.f), "Random Colour", &font, _gui_left_pane);
	random_button->set_function([this](GUIElement* e, bool ignored) {
		GLColour col(rand() % 255, rand() % 255, rand() % 255, 255);
		_gui_colour_picker->set_colour(col);
	});
	_program->add_gui_element(random_button);


	// Add a right pane
	_gui_right_pane = dynamic_cast<Pane*>(_program->add_gui_element(new Pane(vector2df(screen_size.x - 296.f, 32.f), vector2df(296.f, screen_size.y - 64.f))));
	_gui_right_pane->set_mouse_block(GLFW_PRESS, true);
	_gui_right_pane->set_mouse_block(GLFW_DOUBLE_CLICK, true);


	// New layer button
	_gui_new_layer = new Button(vector2df(8.f, 8.f), vector2df(136.f, 32.f), "New Layer", &font, _gui_right_pane);
	_gui_new_layer->set_function([this](GUIElement* element, bool ignored) {
		_image->add_layer();
		create_new_layer("New Layer");
		_gui_image_preview->add_layer(_image->get_layer(_gui_layer_list->get_list_size() - 1)->get_texture());
		_gui_layer_scroll->recalculate_size();

		EditStateManager::instance().add_state(new LayerExistenceState(this, _gui_layer_list->get_list_size() - 1, "New Layer", true));
	});
	_program->add_gui_element(_gui_new_layer);


	// Delete layer button
	_gui_delete_layer = new Button(vector2df(152.f, 8.f), vector2df(136.f, 32.f), "Delete Layer", &font, _gui_right_pane);
	_gui_delete_layer->set_function([this](GUIElement* element, bool ignored) {
		size_t index = _gui_layer_list->get_selected_index();
		EditStateManager::instance().add_state(new LayerExistenceState(this, index, dynamic_cast<Text*>(_gui_layer_list->get_item(index)[0])->get_text(), false));

		delete_layer(index);
	});
	_program->add_gui_element(_gui_delete_layer);


	// The layer list
	_program->add_gui_element(new ColourRect(vector2df(8.f, 48.f), vector2df(280.f, 320.f), GLColour(80, 180), _gui_right_pane));
	_gui_layer_scroll = new ScrollView(vector2df(8.f, 48.f), vector2df(280.f, 320.f), _gui_right_pane);
	_gui_layer_scroll->set_scroll_amount(16.f);
	_gui_layer_scroll->set_vertical_scrollbar_toggle_function([this](bool visible) {
		_gui_layer_list->set_size(vector2df(visible ? 260.f : 280.f, _gui_layer_list->get_size().y));
	});
	_program->add_gui_element(_gui_layer_scroll);

	_gui_layer_list = new ItemList(vector2df(), vector2df(280.f, 320.f), 36.f, _gui_layer_scroll);
	_gui_layer_list->set_move_instead_of_swap(true);
	_gui_layer_list->set_reverse(true);
	_gui_layer_list->set_select_function([this](size_t selected, bool same_selected) {
		_image->set_active_layer(selected);

		if (_gui_layer_alpha)
		{
			float value = _image->get_layer(_gui_layer_list->get_selected_index())->get_alpha();
			_gui_layer_alpha_edit->set_text(util::float_to_string(value));
			_gui_layer_alpha_edit->buffer();
			_gui_layer_alpha->set_value(value);
		}

		if (same_selected)
		{
			auto list = _gui_layer_list->get_item(selected);
			TextEdit* layer_name_edit = dynamic_cast<TextEdit*>(list[2]);
			layer_name_edit->set_visible(!layer_name_edit->is_visible());
			if (layer_name_edit->is_visible())
				layer_name_edit->focus();
		}
	});
	_gui_layer_list->set_swap_function([this](size_t one, size_t two) {
		_image->move_layer(one, two);
		_gui_image_preview->move_layer(one, two);

		float value = _image->get_layer(_gui_layer_list->get_selected_index())->get_alpha();
		_gui_layer_alpha_edit->set_text(util::float_to_string(value));
		_gui_layer_alpha_edit->buffer();
		_gui_layer_alpha->set_value(value);

		EditStateManager::instance().add_state(new LayerMoveState(this, one, two));
	});
	_program->add_gui_element(_gui_layer_list);
	_gui_layer_scroll->add_element(_gui_layer_list);

	create_new_layer("Background");


	// Layer alpha bar
	_gui_layer_alpha = new Slider(vector2df(30.f, 372.f), vector2df(200.f, 24.f), _gui_right_pane, true);
	_gui_layer_alpha->set_value(1.f);
	_gui_layer_alpha->set_value_update_function([this](GUIElement* element, float value) {
		_image->set_layer_alpha(_gui_layer_list->get_selected_index(), value);
		_gui_image_preview->get_layer(_gui_layer_list->get_selected_index())->set_alpha(value);

		_gui_layer_alpha_edit->set_text(util::float_to_string(value));
		_gui_layer_alpha_edit->buffer();
	});
	_gui_layer_alpha->set_finished_changing_function([this](GUIElement* element, float original_value, float new_value) {
		LayerDetailsState* layer_state = new LayerDetailsState(this, _gui_layer_list->get_selected_index());
		layer_state->_original_alpha = original_value;
		layer_state->_new_alpha = new_value;
		EditStateManager::instance().add_state(layer_state);
	});
	_program->add_gui_element(_gui_layer_alpha);

	Text* layer_alpha_text = new Text(vector2df(8.f, 372.f), vector2df(32.f, 24.f), "A: ", &font, _gui_right_pane);
	layer_alpha_text->set_shadowed(true, false);
	_program->add_gui_element(layer_alpha_text);

	_gui_layer_alpha_edit = new TextEdit(vector2df(238.f, 372.f), vector2df(50.f, 24.f), &font, _gui_right_pane, true);
	_gui_layer_alpha_edit->set_text("1.0");
	_gui_layer_alpha_edit->set_text_updated_function([this](GUIElement* element, const std::string& text) {
		float value = static_cast<float>(atof(text.c_str()));
		_gui_layer_alpha->set_value(value);

		_image->set_layer_alpha(_gui_layer_list->get_selected_index(), value);
		_gui_image_preview->get_layer(_gui_layer_list->get_selected_index())->set_alpha(value);
	});
	_program->add_gui_element(_gui_layer_alpha_edit);


	// Preview window
	_gui_preview_window = new WindowPane(vector2df(960.f, 60.f), vector2df(300.f, 200.f), &font, "Preview");
	_gui_preview_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_preview_window->set_visible(false);
	_program->add_gui_element(_gui_preview_window);

	_gui_image_preview = new ImagePreview(_gui_preview_window);
	_program->add_gui_element(_gui_image_preview);
	_gui_image_preview->add_layer(_image->get_layer(0)->get_texture());


	// Grid size window
	_gui_grid_window = new WindowPane(vector2df(400.f, 100.f), vector2df(250.f, 70.f), &font, "Grid size");
	_gui_grid_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_grid_window->set_visible(false);
	_gui_grid_window->set_can_resize(false);
	_gui_grid_window->set_depth(_gui_preview_window->get_depth() + 1000000);

	auto set_grid_text = [this]() {
		// Adjust the grid size in the bottom left text
		const std::string text = _gui_bottom_left_text->get_text();
		std::string text1 = text.substr(0, text.find_first_of('|'));
		std::string text2 = text.substr(text.find_last_of('|'));
		_gui_bottom_left_text->set_text(text1 + "| Grid: " + std::to_string(static_cast<int>(_grid.x)) + " x " + std::to_string(static_cast<int>(_grid.y)) + "px " + text2);
	};

	TextEdit* grid_x_edit = new TextEdit(vector2df(6.f, _gui_grid_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), &font, _gui_grid_window);
	grid_x_edit->set_numbers_only(true, 1, 1280, false);
	grid_x_edit->set_text("16");
	grid_x_edit->set_text_updated_function([set_grid_text, this](GUIElement* element, const std::string& text) {
		_grid.x = static_cast<float>(atof(text.c_str()));
		_program->set_grid(_grid* _image->get_zoom()* _grid_enabled);
		set_grid_text();
	});
	grid_x_edit->set_focus_function([set_grid_text, this](GUIElement* element, bool focused) {
		TextEdit* edit = dynamic_cast<TextEdit*>(element);

		if (edit->get_text().empty())
		{
			edit->set_text("1");
			_grid.x = 1.f;
			_program->set_grid(_grid * _image->get_zoom() * _grid_enabled);
			set_grid_text();
		}
	});

	Text* temp_text = new Text(vector2df(70.f, _gui_grid_window->get_titlebar_height() + 8.f), vector2df(19.f, 32.f), "x", &font, _gui_grid_window);
	temp_text->set_shadowed(true, false);
	temp_text->set_alignment(TextAlignment::centre);

	TextEdit* grid_y_edit = new TextEdit(vector2df(90.f, _gui_grid_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), &font, _gui_grid_window);
	grid_y_edit->set_numbers_only(true, 1, 1280, false);
	grid_y_edit->set_text("16");
	grid_y_edit->set_text_updated_function([set_grid_text, this](GUIElement* element, const std::string& text) {
		_grid.y = static_cast<float>(atof(text.c_str()));
		_program->set_grid(_grid * _image->get_zoom() * _grid_enabled);
		set_grid_text();
	});
	grid_y_edit->set_focus_function([set_grid_text, this](GUIElement* element, bool focused) {
		TextEdit* edit = dynamic_cast<TextEdit*>(element);

		if (edit->get_text().empty())
		{
			edit->set_text("1");
			_grid.y = 1.f;
			_program->set_grid(_grid* _image->get_zoom()* _grid_enabled);
			set_grid_text();
		}
	});

	Button* grid_ok_button = new Button(vector2df(180.f, _gui_grid_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), "OK", &font, _gui_grid_window);
	grid_ok_button->set_function([this](GUIElement* element, bool ignored) {
		_gui_grid_window->set_visible(false);
	});

	_program->add_gui_element(_gui_grid_window);


	// Resize window
	_gui_resize_window = new WindowPane(vector2df(400.f, 200.f), vector2df(250.f, 70.f), &font, "Resize Image");
	_gui_resize_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_resize_window->set_visible(false);
	_gui_resize_window->set_can_resize(false);
	_gui_resize_window->set_depth(_gui_preview_window->get_depth() + 1000000);
	_gui_resize_window->set_close_button_function([this]() {
		vector2di size = _image->get_layer_size();
		_gui_resize_x_edit->set_text(std::to_string(size.x));
		_gui_resize_y_edit->set_text(std::to_string(size.y));
	});

	_gui_resize_x_edit = new TextEdit(vector2df(6.f, _gui_resize_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), &font, _gui_resize_window);
	_gui_resize_x_edit->set_numbers_only(true, 1, 8192, false);
	_gui_resize_x_edit->set_text("64");
	_gui_resize_x_edit->set_focus_function([this](GUIElement* element, bool focused) {
		if (_gui_resize_x_edit->get_text().empty())
			_gui_resize_x_edit->set_text("1");
	});

	temp_text = new Text(vector2df(70.f, _gui_resize_window->get_titlebar_height() + 8.f), vector2df(19.f, 32.f), "x", &font, _gui_resize_window);
	temp_text->set_shadowed(true, false);
	temp_text->set_alignment(TextAlignment::centre);

	_gui_resize_y_edit = new TextEdit(vector2df(90.f, _gui_resize_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), &font, _gui_resize_window);
	_gui_resize_y_edit->set_numbers_only(true, 1, 8192, false);
	_gui_resize_y_edit->set_text("64");
	_gui_resize_y_edit->set_focus_function([this](GUIElement* element, bool focused) {
		if (_gui_resize_y_edit->get_text().empty())
			_gui_resize_y_edit->set_text("1");
	});

	Button* resize_ok_button = new Button(vector2df(180.f, _gui_resize_window->get_titlebar_height() + 4.f), vector2df(64.f, 32.f), "OK", &font, _gui_resize_window);
	resize_ok_button->set_function([this](GUIElement* element, bool ignored) {
		vector2di new_layer_size(atoi(_gui_resize_x_edit->get_text().c_str()), atoi(_gui_resize_y_edit->get_text().c_str()));
		_image->set_layer_size(new_layer_size);

		_gui_resize_window->set_visible(false);
	});

	_program->add_gui_element(_gui_resize_window);


	// HSV Shift window
	_gui_hsv_shift_window = new WindowPane(vector2df(400.f, 200.f), vector2df(370.f, 228.f), &font, "HSV Shift");
	_gui_hsv_shift_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_hsv_shift_window->set_visible(false);
	_gui_hsv_shift_window->set_can_resize(false);
	_gui_hsv_shift_window->set_keep_focus(true);
	_gui_hsv_shift_window->set_depth(_gui_preview_window->get_depth() + 1100000);

	temp_text = new Text(vector2df(6.f, _gui_hsv_shift_window->get_titlebar_height() + 8.f), vector2df(358.f, 96.f), " Hue \n\n Saturation \n\n Value ", &font, _gui_hsv_shift_window);
	temp_text->set_shadowed(true, false);
	temp_text->set_alignment(TextAlignment::centre);

	auto hsv_shift = [this](GUIElement* element, float value) {
		float hue_shift = (_gui_hsv_shift_hue->get_value() - 0.5f) * 360.f;
		float sat_shift = (_gui_hsv_shift_sat->get_value() - 0.5f) * 2.f;
		float val_shift = (_gui_hsv_shift_val->get_value() - 0.5f) * 2.f;
		
		_gui_hsv_shift_hue_edit->set_text(util::float_to_string(hue_shift));
		_gui_hsv_shift_sat_edit->set_text(util::float_to_string(sat_shift));
		_gui_hsv_shift_val_edit->set_text(util::float_to_string(val_shift));

		_gui_hsv_shift_hue_edit->buffer();
		_gui_hsv_shift_sat_edit->buffer();
		_gui_hsv_shift_val_edit->buffer();

		_image->hsv_shift(hue_shift, sat_shift, val_shift);
	};

	auto hsv_shift_edit = [this](GUIElement* element, const std::string& text) {
		float hue_shift = static_cast<float>(atof(_gui_hsv_shift_hue_edit->get_text().c_str()));
		float sat_shift = static_cast<float>(atof(_gui_hsv_shift_sat_edit->get_text().c_str()));
		float val_shift = static_cast<float>(atof(_gui_hsv_shift_val_edit->get_text().c_str()));

		_gui_hsv_shift_hue->set_value((hue_shift / 360.f) + 0.5f);
		_gui_hsv_shift_sat->set_value((sat_shift / 2.f) + 0.5f);
		_gui_hsv_shift_val->set_value((val_shift / 2.f) + 0.5f);

		_image->hsv_shift(hue_shift, sat_shift, val_shift);
	};

	_gui_hsv_shift_hue = new Slider(vector2df(10.f, _gui_hsv_shift_window->get_titlebar_height() + 32.f), vector2df(350.f, 24.f), _gui_hsv_shift_window, true);
	_gui_hsv_shift_hue->set_value(0.5f);
	_gui_hsv_shift_hue->set_value_update_function(hsv_shift);

	_gui_hsv_shift_sat = new Slider(vector2df(10.f, _gui_hsv_shift_window->get_titlebar_height() + 82.f), vector2df(350.f, 24.f), _gui_hsv_shift_window, true);
	_gui_hsv_shift_sat->set_value(0.5f);
	_gui_hsv_shift_sat->set_value_update_function(hsv_shift);

	_gui_hsv_shift_val = new Slider(vector2df(10.f, _gui_hsv_shift_window->get_titlebar_height() + 132.f), vector2df(350.f, 24.f), _gui_hsv_shift_window, true);
	_gui_hsv_shift_val->set_value(0.5f);
	_gui_hsv_shift_val->set_value_update_function(hsv_shift);

	_gui_hsv_shift_hue_edit = new TextEdit(vector2df(260.f, _gui_hsv_shift_window->get_titlebar_height() + 8.f), vector2df(100.f, 24.f), &font, _gui_hsv_shift_window);
	_gui_hsv_shift_hue_edit->set_numbers_only(true, -180.f, 180.f, true);
	_gui_hsv_shift_hue_edit->set_text("0.0");
	_gui_hsv_shift_hue_edit->set_text_updated_function(hsv_shift_edit);

	_gui_hsv_shift_sat_edit = new TextEdit(vector2df(260.f, _gui_hsv_shift_window->get_titlebar_height() + 58.f), vector2df(100.f, 24.f), &font, _gui_hsv_shift_window);
	_gui_hsv_shift_sat_edit->set_numbers_only(true, -1.f, 1.f, true);
	_gui_hsv_shift_sat_edit->set_text("0.0");
	_gui_hsv_shift_sat_edit->set_text_updated_function(hsv_shift_edit);

	_gui_hsv_shift_val_edit = new TextEdit(vector2df(260.f, _gui_hsv_shift_window->get_titlebar_height() + 108.f), vector2df(100.f, 24.f), &font, _gui_hsv_shift_window);
	_gui_hsv_shift_val_edit->set_numbers_only(true, -1.f, 1.f, true);
	_gui_hsv_shift_val_edit->set_text("0.0");
	_gui_hsv_shift_val_edit->set_text_updated_function(hsv_shift_edit);

	auto reset_values = [this]() {
		_gui_hsv_shift_hue->set_value(0.5f);
		_gui_hsv_shift_sat->set_value(0.5f);
		_gui_hsv_shift_val->set_value(0.5f);

		_gui_hsv_shift_hue_edit->set_text("0.0");
		_gui_hsv_shift_sat_edit->set_text("0.0");
		_gui_hsv_shift_val_edit->set_text("0.0");

		_gui_hsv_shift_hue_edit->buffer();
		_gui_hsv_shift_sat_edit->buffer();
		_gui_hsv_shift_val_edit->buffer();
	};

	_gui_hsv_shift_window->set_close_button_function([reset_values, this]() {
		_image->reset_adjustments(true);
		_gui_hsv_shift_window->set_visible(false);
		reset_values();
	});

	Button* recolour_cancel_button = new Button(vector2df(264.f, _gui_hsv_shift_window->get_titlebar_height() + 160.f), vector2df(100.f, 32.f), "Cancel", &font, _gui_hsv_shift_window);
	recolour_cancel_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		_image->reset_adjustments(true);
		_gui_hsv_shift_window->set_visible(false);
		reset_values();
	});

	Button* recolour_ok_button = new Button(vector2df(160.f, _gui_hsv_shift_window->get_titlebar_height() + 160.f), vector2df(100.f, 32.f), "OK", &font, _gui_hsv_shift_window);
	recolour_ok_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		if (_gui_hsv_shift_hue->get_value() != 0.5f || _gui_hsv_shift_sat->get_value() != 0.5f || _gui_hsv_shift_val->get_value() != 0.5f)
			_image->finished_adjustments();
		else
			_image->reset_adjustments(true);
		_gui_hsv_shift_window->set_visible(false);
		reset_values();
	});

	Button* recolour_reset_button = new Button(vector2df(56.f, _gui_hsv_shift_window->get_titlebar_height() + 160.f), vector2df(100.f, 32.f), "Reset", &font, _gui_hsv_shift_window);
	recolour_reset_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		_image->reset_adjustments(false);
		reset_values();
	});

	_program->add_gui_element(_gui_hsv_shift_window);


	// Rotate window
	_gui_rotate_window = new WindowPane(vector2df(300.f, 300.f), vector2df(380.f, 128.f), &font, "Rotate");
	_gui_rotate_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_rotate_window->set_visible(false);
	_gui_rotate_window->set_can_resize(false);
	_gui_rotate_window->set_keep_focus(true);
	_gui_rotate_window->set_depth(_gui_preview_window->get_depth() + 1200000);
	_gui_rotate_window->set_close_button_function([this]() {
		_image->finish_rotation(false);
		_gui_rotate->set_value(0.5f);
	});

	temp_text = new Text(vector2df(6.f, _gui_hsv_shift_window->get_titlebar_height() + 8.f), vector2df(368.f, 96.f), " Rotation (Degrees) ", &font, _gui_rotate_window);
	temp_text->set_shadowed(true, false);
	temp_text->set_alignment(TextAlignment::centre);

	_gui_rotate = new Slider(vector2df(10.f, _gui_rotate_window->get_titlebar_height() + 32.f), vector2df(360.f, 24.f), _gui_rotate_window, true);
	_gui_rotate->set_value(0.5f);
	_gui_rotate->set_value_update_function([this](GUIElement* element, float value) {
		float degrees = round((value - 0.5f) * 360.f);
		_image->rotate(degrees);
		_gui_rotate_edit->set_text(util::float_to_string(degrees));
		_gui_rotate_edit->buffer();
	});

	_gui_rotate_edit = new TextEdit(vector2df(310.f, _gui_rotate_window->get_titlebar_height() + 8.f), vector2df(60.f, 24.f), &font, _gui_rotate_window);
	_gui_rotate_edit->set_numbers_only(true, -180.f, 180.f, true);
	_gui_rotate_edit->set_text("0.0");
	_gui_rotate_edit->set_text_updated_function([this](GUIElement* element, const std::string& text) {
		float degrees = static_cast<float>(atof(text.c_str()));

		_gui_rotate->set_value((degrees / 360.f) + 0.5f);
		_image->rotate(degrees);
	});

	Button* rotate_cancel_button = new Button(vector2df(274.f, _gui_rotate_window->get_titlebar_height() + 60.f), vector2df(100.f, 32.f), "Cancel", &font, _gui_rotate_window);
	rotate_cancel_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		_image->finish_rotation(false);
		_gui_rotate->set_value(0.5f);
		_gui_rotate_edit->set_text("0.0");
		_gui_rotate_window->set_visible(false);
	});

	Button* rotate_ok_button = new Button(vector2df(170.f, _gui_rotate_window->get_titlebar_height() + 60.f), vector2df(100.f, 32.f), "OK", &font, _gui_rotate_window);
	rotate_ok_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		_image->finish_rotation(_gui_rotate->get_value() != 0.5f);
		_gui_rotate->set_value(0.5f);
		_gui_rotate_edit->set_text("0.0");
		_gui_rotate_window->set_visible(false);
	});

	Button* rotate_reset_button = new Button(vector2df(66.f, _gui_rotate_window->get_titlebar_height() + 60.f), vector2df(100.f, 32.f), "Reset", &font, _gui_rotate_window);
	rotate_reset_button->set_function([reset_values, this](GUIElement* element, bool ignored) {
		_image->rotate(0.f);
		_gui_rotate->set_value(0.5f);
		_gui_rotate_edit->set_text("0.0");
	});

	_program->add_gui_element(_gui_rotate_window);


	// File dialog
	_gui_file_dialog = new FileDialog(vector2df(300.f, 300.f), &font);
	_gui_file_dialog->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
	_gui_file_dialog->set_visible(false);
	_gui_file_dialog->set_can_resize(false);
	_program->add_gui_element(_gui_file_dialog);

	// File dialog functions
	_open_file_function = [this](const std::string& filename) {
		open_file(filename);
	};

	_save_file_function = [this](const std::string& filename) {
		save_file(filename);
	};

	_import_file_function = [this](const std::string& filename) {
		EditStateManager::instance().add_state(new LayerImportState(this, filename));
		import_file(filename);
	};

	_export_file_function = [this](const std::string& filename) {
		export_file(filename);
	};
}

void LayoutManager::screen_resized(const vector2df& screen_size)
{
	// Now adjust all our GUI elements
	if (_gui_bottom_pane)
	{
		_gui_bottom_pane->set_position(vector2df(0.f, screen_size.y - 32.f));
		_gui_bottom_pane->set_size(vector2df(screen_size.x, 32.f));

		_gui_zoom_text->set_position(vector2df(screen_size.x - 200.f, 6.f));
		_gui_bottom_left_text->set_position(vector2df(8.f, 6.f));
		_gui_bottom_left_text->set_size(vector2df(screen_size.x, 24.f));

		arrange_mode_buttons(screen_size);
	}

	if (_gui_top_pane)
	{
		_gui_top_pane->set_size(vector2df(screen_size.x, 32.f));
	}

	if (_gui_left_pane)
	{
		_gui_left_pane->set_size(vector2df(296.f, screen_size.y - 64.f));
	}

	if (_gui_right_pane)
	{
		_gui_right_pane->set_position(vector2df(screen_size.x - 296.f, 32.f));
		_gui_right_pane->set_size(vector2df(296.f, screen_size.y - 64.f));
	}

	if (_gui_preview_window)
	{
		_gui_preview_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
		_gui_image_preview->set_position(_gui_image_preview->get_position());
	}

	if (_gui_grid_window)
		_gui_grid_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));

	if (_gui_resize_window)
		_gui_resize_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));

	if (_gui_hsv_shift_window)
		_gui_hsv_shift_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));

	if (_gui_rotate_window)
		_gui_rotate_window->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));

	if (_gui_file_dialog)
		_gui_file_dialog->set_move_limits(vector4df(0.f, 32.f, screen_size.x, screen_size.y));
}

void LayoutManager::open_file_at_start(const std::string& filename)
{
	std::filesystem::path path(filename);
	std::string extension = path.extension().string();

	if (extension == ".pxla")
		_open_file_function(filename);
	else if (extension == ".png")
		_import_file_function(filename);
}

void LayoutManager::arrange_mode_buttons(const vector2df& screen_size)
{
	float width = static_cast<float>(_gui_mode_buttons.size()) * 28.f;
	float start_x = (screen_size.x - width) * 0.5f;
	for (size_t i = 0; i < _gui_mode_buttons.size(); ++i) {
		_gui_mode_buttons[i]->set_position(vector2df(start_x + static_cast<float>(i) * 28.f, 4.f));
	}
}

void LayoutManager::create_new_layer(const std::string& name)
{
	// So we don't duplicate code, add a new layer
	const Font& font = FontManager::instance().get_resource(FileManager::_executable_directory + "media/OpenSans.ttf:{16");
	Text* layer_name = new Text(vector2df(38.f, 8.f), vector2df(200.f, 0.f), name, &font, _gui_layer_list);
	layer_name->set_expand_past_width(true, false);
	layer_name->set_shadowed(true, false);
	_gui_layer_list->add_item(layer_name);

	// Hide button and the function to toggle the visibility of its layer
	Button* hide_button = new Button(vector2df(0.f, 2.f), vector2df(_eye_texture ? 32.f : 28.f, 0.f), "H", &font, _gui_layer_list);
	if (_eye_texture)
		hide_button->set_image_mode(true, _eye_texture.get());
	hide_button->set_toggle_mode(true);
	hide_button->set_function([this](GUIElement* element, bool pressed) {
		size_t index = dynamic_cast<ItemList*>(element->get_parent())->get_element_list_index(element);
		const ImageLayer* layer = _image->get_layer(index);
		_image->set_layer_visibility(index, !pressed);
		_gui_image_preview->get_layer(index)->set_visible(layer->is_visible());

		LayerDetailsState* layer_state = new LayerDetailsState(this, index);
		layer_state->_original_visible = !layer->is_visible();
		EditStateManager::instance().add_state(layer_state);
	});
	_gui_layer_list->add_item(hide_button, _gui_layer_list->get_list_size() - 1);

	// Text edit and the function to rename the layer
	TextEdit* layer_name_edit = new TextEdit(vector2df(34.f, 2.f), vector2df(220.f, 0.f), &font, _gui_layer_list, true);
	_gui_layer_list->add_item(layer_name_edit, _gui_layer_list->get_list_size() - 1);
	layer_name_edit->set_text(layer_name->get_text());
	layer_name_edit->set_visible(false);
	layer_name_edit->set_focus_function([this](GUIElement* edit, bool focused) {
		size_t index = _gui_layer_list->get_element_list_index(edit);
		auto list = _gui_layer_list->get_item(index);
		Text* layer_name = dynamic_cast<Text*>(list[0]);
		if (!focused)
		{
			TextEdit* layer_name_edit = dynamic_cast<TextEdit*>(list[2]);

			LayerDetailsState* layer_state = new LayerDetailsState(this, index);
			layer_state->_new_name = layer_name_edit->get_text();
			EditStateManager::instance().add_state(layer_state);
			
			layer_name->set_text(layer_name_edit->get_text());
			layer_name_edit->set_visible(false);
		}

		layer_name->set_visible(!focused);
	});
}

void LayoutManager::delete_layer(size_t index)
{
	if (_gui_layer_list->get_list_size() > 1)
	{
		_gui_layer_list->remove_item(index);
		_gui_layer_scroll->recalculate_size();
	}

	_image->remove_layer(index);
	_gui_image_preview->remove_layer(index);
}

void LayoutManager::move_layer(size_t index, size_t dest)
{
	_gui_layer_list->move_items(index, dest, false);

	_image->move_layer(index, dest);
	_gui_image_preview->move_layer(index, dest);

	size_t selected = _gui_layer_list->get_selected_index();
	if (selected == index || selected == dest)
	{
		float value = _image->get_layer(selected)->get_alpha();
		_gui_layer_alpha_edit->set_text(util::float_to_string(value));
		_gui_layer_alpha_edit->buffer();
		_gui_layer_alpha->set_value(value);
	}
}

void LayoutManager::open_file(const std::string& filename)
{
	std::vector<LayerDetails> layers;
	int width, height;
	if (FileManager::read_pxla(filename, layers, width, height))
	{
		size_t current_layer_count = _gui_layer_list->get_list_size();

		vector2di new_size(width, height);
		_image->set_layer_size(new_size, false, true); // Don't need to resize current layers as they're being deleted

		for (auto& layer_details : layers) {
			_image->add_layer();
			create_new_layer(layer_details.name);
			const ImageLayer* layer = _image->get_layer(_gui_layer_list->get_list_size() - 1);
			_gui_image_preview->add_layer(layer->get_texture());
			_gui_layer_scroll->recalculate_size();

			vector2di current_size = _image->get_layer_size();
			vector2di new_size(width, height);

			_image->load_from_buffer(layer_details.data, width, height);
			delete[] layer_details.data;

			_image->set_layer_visibility(_gui_layer_list->get_list_size() - 1, layer_details.visible);
			_image->set_layer_alpha(_gui_layer_list->get_list_size() - 1, layer_details.alpha);

			if (!layer_details.visible)
				static_cast<Button*>(_gui_layer_list->get_item(_gui_layer_list->get_list_size() - 1)[1])->set_pressed(true);

			_gui_image_preview->get_layer(_gui_layer_list->get_list_size() - 1)->set_visible(layer_details.visible);
			_gui_image_preview->get_layer(_gui_layer_list->get_list_size() - 1)->set_alpha(layer_details.alpha);
		}

		// Delete any layers that were existing before we opened this file as they are not part of the file
		for (size_t index = 0; index < current_layer_count; ++index) {
			delete_layer(0);
		}

		_gui_layer_list->select_item(0);
		EditStateManager::instance().clear_states();
	}

	_save_file_path = filename;
}

void LayoutManager::save_file(const std::string& filename)
{
	std::vector<LayerDetails> layers;

	for (size_t i = 0; i < _gui_layer_list->get_list_size(); ++i) {
		LayerDetails layer_details;
		layer_details.name = dynamic_cast<Text*>(_gui_layer_list->get_item(i)[0])->get_text();

		const ImageLayer* layer = _image->get_layer(i);
		layer_details.alpha = layer->get_alpha();
		layer_details.visible = layer->is_visible();
		layer_details.data = &layer->get_texture()->get_data()[0];

		layers.push_back(layer_details);
	}

	FileManager::write_pxla(filename, layers, static_cast<int>(_image->get_layer_size().x), static_cast<int>(_image->get_layer_size().y));

	_save_file_path = filename;
}

void LayoutManager::import_file(const std::string& filename)
{
	std::vector<GLColour> data;
	int width, height;
	if (FileManager::read_png(filename, data, width, height))
	{
		_image->add_layer();
		create_new_layer(std::filesystem::path(filename).filename().string());
		_gui_image_preview->add_layer(_image->get_layer(_gui_layer_list->get_list_size() - 1)->get_texture());
		_gui_layer_scroll->recalculate_size();

		vector2di current_size = _image->get_layer_size();
		vector2di new_size(width, height);

		_image->load_from_buffer(data, width, height);

		if (current_size.x < new_size.x || current_size.y < new_size.y)
			_image->set_layer_size(vector2di(std::max(current_size.x, new_size.x), std::max(current_size.y, new_size.y)), true, true);
		else
			_image->set_layer_size(current_size, true, true);
	}
}

void LayoutManager::export_file(const std::string& filename)
{
	std::vector<GLColour> data;
	_image->get_complete_image_buffer(data);
	FileManager::write_png(filename, data, static_cast<int>(_image->get_layer_size().x), static_cast<int>(_image->get_layer_size().y));

	_export_file_path = filename;
}

LayerMoveState::LayerMoveState(LayoutManager* layout, size_t one, size_t two)
	: EditState(one)
	, _layout(layout)
	, _dest(two)
{

}

void LayerMoveState::undo()
{
	_layout->move_layer(_dest, _layer_index);
}

void LayerMoveState::redo()
{
	_layout->move_layer(_layer_index, _dest);
}

bool LayerMoveState::validate_state()
{
	return _layer_index != _dest;
}

LayerImportState::LayerImportState(LayoutManager* layout, const std::string& filename)
	: EditState(0)
	, _layout(layout)
	, _filename(filename)
	, _prev_size(_layout->_image->get_layer_size())
{

}

void LayerImportState::undo()
{
	_layout->delete_layer(_layout->_gui_layer_list->get_list_size() - 1);

	if (_prev_size != _layout->_image->get_layer_size())
		_layout->_image->set_layer_size(_prev_size, true, true);
}

void LayerImportState::redo()
{
	_layout->import_file(_filename);
}

LayerExistenceState::LayerExistenceState(LayoutManager* layout, size_t index, const std::string& name, bool creation)
	: EditState(index)
	, _layout(layout)
	, _creation(creation)
	, _data(_layout->_image->get_layer(index)->get_texture()->get_data())
	, _name(name)
	, _alpha(_layout->_image->get_layer(index)->get_alpha())
	, _visible(_layout->_image->get_layer(index)->is_visible())
	, _one_layer(_layout->_gui_layer_list->get_list_size() == 1)
{

}

void LayerExistenceState::undo()
{
	if (_creation)
		_layout->delete_layer(_layer_index);
	else
	{
		if (_one_layer)
			_layout->_image->load_from_buffer(_data, static_cast<int>(_layout->_image->get_layer_size().x), static_cast<int>(_layout->_image->get_layer_size().y));
		else
			create_layer();
	}
}

void LayerExistenceState::redo()
{
	if (_creation)
		create_layer();
	else
		_layout->delete_layer(_layer_index);
}

void LayerExistenceState::create_layer()
{
	_layout->_image->add_layer();
	_layout->create_new_layer(_name);

	size_t new_index = _layout->_gui_layer_list->get_list_size() - 1;
	_layout->_gui_image_preview->add_layer(_layout->_image->get_layer(new_index)->get_texture());
	_layout->_gui_layer_scroll->recalculate_size();

	if (!_creation)
	{
		_layout->_image->load_from_buffer(_data, static_cast<int>(_layout->_image->get_layer_size().x), static_cast<int>(_layout->_image->get_layer_size().y));
		_layout->_image->set_layer_alpha(new_index, _alpha);
		_layout->_image->set_layer_visibility(new_index, _visible);
	}

	if (_layer_index != new_index)
		_layout->move_layer(new_index, _layer_index);
}

LayerDetailsState::LayerDetailsState(LayoutManager* layout, size_t index)
	: EditState(index)
	, _layout(layout)
	, _original_visible(_layout->_image->get_layer(index)->is_visible())
	, _original_alpha(_layout->_image->get_layer(index)->get_alpha())
	, _original_name(dynamic_cast<Text*>(_layout->_gui_layer_list->get_item(index)[0])->get_text())
	, _new_visible(_original_visible)
	, _new_alpha(_original_alpha)
	, _new_name(_original_name)
{

}

void LayerDetailsState::undo()
{
	if (_original_visible != _new_visible)
		set_visibility(_original_visible);
	if (_original_alpha != _new_alpha)
		set_alpha(_original_alpha);
	if (_original_name != _new_name)
		dynamic_cast<Text*>(_layout->_gui_layer_list->get_item(_layer_index)[0])->set_text(_original_name);
}

void LayerDetailsState::redo()
{
	if (_original_visible != _new_visible)
		set_visibility(_new_visible);
	if (_original_alpha != _new_alpha)
		set_alpha(_new_alpha);
	if (_original_name != _new_name)
		dynamic_cast<Text*>(_layout->_gui_layer_list->get_item(_layer_index)[0])->set_text(_new_name);
}

void LayerDetailsState::set_visibility(bool visible)
{
	_layout->_image->set_layer_visibility(_layer_index, visible);
	_layout->_gui_image_preview->get_layer(_layer_index)->set_visible(visible);

	dynamic_cast<Button*>(_layout->_gui_layer_list->get_item(_layer_index)[1])->set_pressed(!visible);
}

void LayerDetailsState::set_alpha(float alpha)
{
	_layout->_image->set_layer_alpha(_layer_index, alpha);
	_layout->_gui_image_preview->get_layer(_layer_index)->set_alpha(alpha);

	if (_layout->_gui_layer_list->get_selected_index() == _layer_index)
	{
		_layout->_gui_layer_alpha->set_value(alpha);
		_layout->_gui_layer_alpha_edit->set_text(util::float_to_string(alpha));
		_layout->_gui_layer_alpha_edit->buffer();
	}
}

bool LayerDetailsState::validate_state()
{
	return (_original_visible != _new_visible || _original_alpha != _new_alpha || _original_name != _new_name);
}