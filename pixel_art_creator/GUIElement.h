#ifndef GUI_ELEMENT_H
#define GUI_ELEMENT_H

#include "Matrix.h"
#include "Mesh.h"

#include <memory>

namespace cursor
{
	constexpr size_t arrow = 0;
	constexpr size_t hresize = 1;
	constexpr size_t vresize = 2;
	constexpr size_t tlbrresize = 3;
	constexpr size_t trblresize = 4;
	constexpr size_t ibeam = 5;
	constexpr size_t erase = 6;
	constexpr size_t fill = 7;
	constexpr size_t select = 8;
	constexpr size_t last_cursor = select;
}

class Texture;

class GUIElement {
public:
	GUIElement(const vector2df& pos, const vector2df& size, GUIElement* parent = nullptr, bool force_mesh = false);
	virtual ~GUIElement();
	
	// This must be overridden as it is where the appearance of the element is created
	virtual void build() = 0;

	virtual bool update(int mouse_x, int mouse_y);
	void update_children(int mouse_x, int mouse_y);

	const Mesh* get_mesh_ptr() const;
	void force_mesh();
	void delete_mesh();

	GUIElement* get_parent() const;
	const std::vector<GUIElement*>& get_children() const;

	void set_depth(int depth);
	int get_depth() const;

	void set_priority(bool priority);
	bool has_priority() const;

	const bool operator<(const GUIElement& element) const;

	virtual void set_position(const vector2df& pos);
	const vector2df& get_position() const;
	vector2df get_absolute_position() const;

	virtual void set_size(const vector2df& size);
	const vector2df& get_size() const;

	virtual void set_scale(const vector2df& scale);
	const vector2df& get_scale() const;
	vector2df get_absolute_scale() const;

	void set_cull_box(const vector4df& cull);
	bool get_cull_box(vector4df& out) const;

	const matrix4f& get_transform() const;

	void set_texture(const Texture* tex);
	const Texture* get_texture() const;

	void set_visible(bool visible = true);
	bool is_visible() const;

	void kill();
	bool is_alive() const;

	void set_alpha(float alpha);
	float get_alpha() const;

	void set_mouse_block(int action, bool block);

	const bool is_point_inside(float x, float y) const;
	bool is_mouse_hovered() const;

	virtual bool mouse_hover(bool entering);
	virtual bool mouse_event(int button, int action, int mods);
	virtual bool scroll_event(double x, double y);
	virtual void screen_resized(int width, int height);
	virtual bool key_event(int key, int scancode, int action, int mods);
	virtual bool char_event(unsigned int codepoint);
	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:
	Mesh& get_mesh();
	void add_child(GUIElement* element);
	void remove_child(GUIElement* element);

	virtual void update_mesh();
	virtual void update_transforms();

	size_t _mesh_start;
	size_t _cursor;

private:
	GUIElement* _parent;
	std::vector<GUIElement*> _children;

	std::unique_ptr<Mesh> _mesh;
	const Texture* _texture;
	int _depth;
	bool _visible;
	bool _alive;
	float _alpha;
	bool _priority;

	vector2df _position;
	vector2df _size;
	vector2df _scale;
	vector4df _cull_box;

	matrix4f _transform;
	bool _update_transform;

	bool _mouse_hover;
	bool _mouse_block[4];
};

#endif