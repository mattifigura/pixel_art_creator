#include "ScrollView.h"
#include "Util.h"

ScrollView::ScrollView(const vector2df& pos, const vector2df& size, GUIElement* parent)
	: GUIElement(pos, size, parent, true)
	, _places()
	, _content_size()
	, _mouse_position(0.f, 0.f)
	, _scroll_start_position(0.f, 0.f)
	, _scroll_amount(12.f)
	, _vertical_position(0.f)
	, _vertical_scrollbar_size(0.f)
	, _scrolling_vertical(false)
	, _vertical_hovered(false)
	, _vertical_scrollbar_toggled_function(nullptr)
	, _horizontal_position(0.f)
	, _horizontal_scrollbar_size(0.f)
	, _scrolling_horizontal(false)
	, _horizontal_hovered(false)
{
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));
	set_depth(100);
}

ScrollView::~ScrollView()
{

}

void ScrollView::build()
{
	Mesh& mesh = get_mesh();
	vector2df size = get_size();
	GLColour back_colour(0, 0);

	// Vertical scrollbar (hidden at the start) - bar is index 1
	mesh.add_quad(GLVertex2D(vector2df(size.x - 20.f, 0.f), back_colour),
		GLVertex2D(vector2df(size.x, 0.f), back_colour),
		GLVertex2D(size, back_colour),
		GLVertex2D(vector2df(size.x - 20.f, size.y), back_colour));
	mesh.add_quad(GLVertex2D(vector2df(size.x - 20.f, 0.f), back_colour),
		GLVertex2D(vector2df(size.x, 0.f), back_colour),
		GLVertex2D(size, back_colour),
		GLVertex2D(vector2df(size.x - 20.f, size.y), back_colour));

	// Horizontal scrollbar (hidden at the start) - bar is index 3
	///NOTE: Horizontal scrollbar functionality is not yet implemented!!!!
	//mesh.add_quad(GLVertex2D(vector2df(0.f, size.y - 20.f), back_colour),
	//	GLVertex2D(vector2df(size.x - 20.f, size.y - 20.f), back_colour),
	//	GLVertex2D(vector2df(size.x - 20.f, size.y), back_colour),
	//	GLVertex2D(vector2df(0.f, size.y), back_colour));
	//mesh.add_quad(GLVertex2D(vector2df(0.f, size.y - 20.f), back_colour),
	//	GLVertex2D(vector2df(size.x - 20.f, size.y - 20.f), back_colour),
	//	GLVertex2D(vector2df(size.x - 20.f, size.y), back_colour),
	//	GLVertex2D(vector2df(0.f, size.y), back_colour));
}

bool ScrollView::update(int mouse_x, int mouse_y)
{
	_mouse_position.set(static_cast<float>(mouse_x), static_cast<float>(mouse_y));
	
	if (is_visible())
	{
		// Handle scrolling
		Mesh& mesh = get_mesh();
		vector2df size = get_size();
		vector2df pos = get_absolute_position();
		bool update = false;
		if (_scrolling_vertical)
		{
			float y_diff = _mouse_position.y - (pos.y + _vertical_position + _scroll_start_position.y);
			_vertical_position += y_diff;

			_vertical_position = std::max(std::min(_vertical_position, size.y - _vertical_scrollbar_size), 0.f);
			update = true;
		}

		// Check for mouse hover changes
		bool vertical_was_hovered(_vertical_hovered);
		_vertical_hovered = util::is_point_inside(_mouse_position, pos + vector2df(size.x - 20.f, _vertical_position), pos + vector2df(size.x, _vertical_position + _vertical_scrollbar_size));
		if (vertical_was_hovered != _vertical_hovered)
		{
			GLColour front_colour(255, _vertical_hovered ? 220 : 150);
			mesh.set_quad_colour(1, front_colour);
		}

		if (update)
			update_positions();
	}

	return GUIElement::update(mouse_x, mouse_y);
}

void ScrollView::set_position(const vector2df& pos)
{
	GUIElement::set_position(pos);
	
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + get_size().x, abs_pos.y + get_size().y));
}

void ScrollView::add_element(GUIElement* element)
{
	_places.emplace(element, element->get_position());

	element->set_depth(-99);

	recalculate_size();
}

void ScrollView::remove_element(GUIElement* element)
{
	remove_child(element);
	_places.erase(element);

	recalculate_size();
}

void ScrollView::scroll_to_top()
{
	_vertical_position = 0;
	update_positions();
}

void ScrollView::set_scroll_amount(float amount)
{
	_scroll_amount = amount;
}

void ScrollView::set_vertical_scrollbar_toggle_function(const std::function<void(bool)>& func)
{
	_vertical_scrollbar_toggled_function = func;
}

void ScrollView::recalculate_size()
{
	// Updates our scroll bars based on the size of our children
	vector2df previous_content_size(_content_size);
	_content_size.set(0.f, 0.f);
	for (auto& it : _places) {
		_content_size += it.second + it.first->get_size();
	}

	Mesh& mesh = get_mesh();
	vector2df size = get_size();
	GLColour back_colour(70, 230);
	GLColour front_colour(255, 150);
	if (_content_size.y > size.y)
	{
		// We need a vertical scroll bar
		_vertical_scrollbar_size = (size.y / _content_size.y) * size.y;
		_vertical_position *= previous_content_size.y / _content_size.y;
		_vertical_position = std::max(std::min(_vertical_position, size.y - _vertical_scrollbar_size), 0.f);

		if (previous_content_size.y <= size.y && _vertical_scrollbar_toggled_function)
			_vertical_scrollbar_toggled_function(true);

		if (_vertical_hovered)
			front_colour.a = 220;
	}
	else
	{
		// Remove the vertical scroll bar
		back_colour.a = 0;
		front_colour.a = 0;
		_vertical_scrollbar_size = 0.f;
		_vertical_position = 0.f;

		if (previous_content_size.y > size.y && _vertical_scrollbar_toggled_function)
			_vertical_scrollbar_toggled_function(false);
	}

	if (mesh.get_index_buffer_size() > 0)
	{
		mesh.set_quad_colour(0, back_colour);
		mesh.set_quad_colour(1, front_colour);
	}

	// If we need horizontal scrollbar functionality, I will implement it
	//if (_content_size.x > size.x)
	//{
	//	// We need a horizontal scroll bar
	//	back_colour.a = 230;
	//	front_colour = 180;
	//}
	//else
	//{
	//	// Remove the horizontal scroll bar
	//	back_colour.a = 0;
	//	front_colour.a = 0;
	//	_horizontal_scrollbar_size = 0.f;
	//	_horizontal_position = 0.f;
	//}
	//mesh.set_quad_colour(2, back_colour);
	//mesh.set_quad_colour(3, front_colour);

	if (mesh.get_index_buffer_size() > 0)
		update_positions();
}

bool ScrollView::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		vector2df pos = get_absolute_position();
		vector2df size = get_size();
		if (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK)
		{
			if (_vertical_scrollbar_size > 0.f && util::is_point_inside(_mouse_position, pos + vector2df(size.x - 20.f, 0.f), pos + size))
			{
				_scrolling_vertical = true;
				_scroll_start_position = _mouse_position - pos - _vertical_position;

				if (_scroll_start_position.y < 0.f || _scroll_start_position.y > _vertical_scrollbar_size)
					_scroll_start_position.y = _vertical_scrollbar_size * 0.5f;

				return true;
			}
		}
		else if (action == GLFW_RELEASE)
		{
			_scrolling_vertical = false;
			_scrolling_horizontal = false;
		}
	}
	return false;
}

bool ScrollView::scroll_event(double x, double y)
{
	if (is_mouse_hovered() && _content_size.y > get_size().y)
	{
		
		_vertical_position -= static_cast<float>(y) * _scroll_amount;

		_vertical_position = std::max(std::min(_vertical_position, get_size().y - _vertical_scrollbar_size), 0.f);
		update_positions();

		return true;
	}

	return false;
}

void ScrollView::screen_resized(int width, int height)
{
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + get_size().x, abs_pos.y + get_size().y));
}

bool ScrollView::set_mouse_cursor(size_t& cursor) const
{
	return false;
}

void ScrollView::update_positions()
{
	Mesh& mesh = get_mesh();
	vector2df size = get_size();
	mesh.set_quad_position(1, vector2df(size.x - 20.f, _vertical_position),
		vector2df(size.x, _vertical_position),
		vector2df(size.x, _vertical_position + _vertical_scrollbar_size),
		vector2df(size.x - 20.f, _vertical_position + _vertical_scrollbar_size));

	// Update our elements
	float amount = _content_size.y / size.y;
	for (auto& it : _places) {
		it.first->set_position(vector2df(0.f, -_vertical_position * amount) + it.second);
	}
}