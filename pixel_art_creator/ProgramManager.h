#ifndef PROGRAM_MANAGER_H
#define PROGRAM_MANAGER_H

#include "GLShaderProgram.h"
#include "LayoutManager.h"

#include <memory>
#include <vector>

class ProgramManager {
public:
	ProgramManager(ProgramManager const&) = delete;
	void operator=(ProgramManager const&) = delete;

	static ProgramManager& instance()
	{
		static ProgramManager singleton;
		return singleton;
	}

	~ProgramManager();

	bool init();
	void setup_gui();

	bool update_loop(int mouse_x, int mouse_y);
	void render_loop();

	void stop_running();
	bool is_running() const;

	void screen_resized(GLFWwindow* window);

	GUIElement* add_gui_element(GUIElement* element);
	GUIElement* add_gui_element_queued(GUIElement* element);

	bool lock(GUIElement* element);
	bool unlock(GUIElement* element);
	GUIElement* get_lock() const;

	void sort_gui_elements();

	void set_grid(const vector2df& grid);
	const vector2df& get_grid() const;

	void mouse_event(int button, int action, int mods);
	void scroll_event(double x, double y);
	void key_event(int key, int scancode, int action, int mods);
	void char_event(unsigned int codepoint);
	void set_mouse_cursor(GLFWwindow* window);

	void open_file_at_start(const std::string& filename);

protected:

private:
	ProgramManager();

	GLuint _vao;
	bool _dirty;
	GLShaderProgram _shader;
	int _cull_box_uniform;
	int _grid_uniform;
	int _alpha_uniform;
	size_t _mouse_cursor;
	std::vector<GLFWcursor*> _cursors;

	bool _running;
	bool _need_to_sort;

	LayoutManager _layout;

	vector2di _screen_size;
	vector2df _grid;

	Texture _default_texture;

	std::vector<std::shared_ptr<GUIElement>> _elements;
	std::vector<GUIElement*> _element_add_queue;

	GUIElement* _lock;

	void sort();
};

#endif