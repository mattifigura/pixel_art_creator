#include "FileManager.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <png.h>
#include <zlib.h>

#ifdef _WIN32
#include <stdint.h>
#endif

static const unsigned short file_version = 1;
const char identifier[] = { 'i', 'a', 'm', 'a', 'p', 'x', 'l', 'a' };

std::string FileManager::_executable_directory("");

bool FileManager::read_png(const std::string& filename, std::vector<GLColour>& data, int& width, int& height)
{
	std::cout << "Loading PNG file " << filename << std::endl;

#ifdef _WIN32
	FILE* file = nullptr;
	fopen_s(&file, filename.c_str(), "rb");
#else
	FILE* file = fopen(filename.c_str(), "rb");
#endif

	if (!file)
		return false;

	png_byte header[8];
	fread(header, 1, 8, file);
	if (png_sig_cmp(header, 0, 8) != 0)
	{
		std::cout << filename << " is not a valid PNG file." << std::endl;
		fclose(file);
		return false;
	}

	png_structp png = nullptr;
	png_infop info = nullptr;
	png_infop end = nullptr;

	auto cleanup = [&]() {
		png_destroy_read_struct(png ? &png : NULL, info ? &info : NULL, end ? &end : NULL);
		fclose(file);
		return false;
	};

	// Initialise structures
	png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)
		return cleanup();

	info = png_create_info_struct(png);
	if (!info)
		return cleanup();

	end = png_create_info_struct(png);
	if (!end)
		return cleanup();

	// Set the PNG error jump point
	if (setjmp(png_jmpbuf(png)))
	{
		std::cout << "Error loading PNG file " << filename << std::endl;
		return cleanup();
	}

	// Open for reading and check colour and bit depth
	png_init_io(png, file);
	png_set_sig_bytes(png, 8);
	png_read_info(png, info);

	int bit_depth = 0;
	int colour_type = 0;
	unsigned int u_width = 0;
	unsigned int u_height = 0;

	png_get_IHDR(png, info, &u_width, &u_height, &bit_depth, &colour_type, NULL, NULL, NULL);

	if (bit_depth == 16)
		png_set_strip_16(png);
	else if (bit_depth < 8)
	{
		if (colour_type == PNG_COLOR_TYPE_GRAY)
			png_set_expand_gray_1_2_4_to_8(png);
		else
			png_set_expand(png);
	}

	// Convert palette to RGBA
	if (colour_type == PNG_COLOR_TYPE_PALETTE)
	{
		png_set_palette_to_rgb(png);
		png_set_add_alpha(png, 255, PNG_FILLER_AFTER);
	}

	// If this is an RGB file with no alpha, add alpha
	if (colour_type == PNG_COLOR_TYPE_RGB)
		png_set_add_alpha(png, 255, PNG_FILLER_AFTER);

	// Read the file
	png_read_update_info(png, info);

	size_t row_bytes = png_get_rowbytes(png, info);
	data.resize(row_bytes * u_height);
	data.shrink_to_fit();

	std::vector<png_byte*> row_ptrs;
	for (size_t i = 0; i < u_height; ++i) {
		row_ptrs.push_back(reinterpret_cast<png_byte*>(&data[i * (row_bytes / 4)]));
	}

	png_read_image(png, &row_ptrs[0]);
	png_read_end(png, end);

	cleanup();

	width = static_cast<int>(u_width);
	height = static_cast<int>(u_height);

	return true;
}

bool FileManager::write_png(const std::string& filename, std::vector<GLColour>& data, int width, int height)
{
	std::cout << "Saving PNG file " << filename << std::endl;

#ifdef _WIN32
	FILE* file = nullptr;
	fopen_s(&file, filename.c_str(), "wb");
#else
	FILE* file = fopen(filename.c_str(), "wb");
#endif

	if (!file)
		return false;

	png_structp png = nullptr;
	png_infop info = nullptr;

	auto cleanup = [&]() {
		png_destroy_write_struct(png ? &png : NULL, info ? &info : NULL);
		fclose(file);
		return false;
	};

	// Initialise structures
	png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)
		return cleanup();

	info = png_create_info_struct(png);
	if (!info)
		return cleanup();

	// Set the PNG error jump point
	if (setjmp(png_jmpbuf(png)))
	{
		std::cout << "Error loading PNG file " << filename << std::endl;
		return cleanup();
	}

	// Open for writing and set info
	png_init_io(png, file);
	png_set_IHDR(png, info, static_cast<unsigned int>(width), static_cast<unsigned int>(height), 8, PNG_COLOR_TYPE_RGB_ALPHA,
		PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	png_write_info(png, info);

	// Write the file
	std::vector<png_byte*> row_ptrs;
	for (size_t i = 0; i < static_cast<size_t>(height); ++i) {
		row_ptrs.push_back(reinterpret_cast<png_byte*>(&data[i * width]));
	}

	png_write_image(png, &row_ptrs[0]);
	png_write_end(png, NULL);

	cleanup();

	return true;
}

// Helper function to read a variable
template<typename T>
void read_data(std::ifstream& file, T& data)
{
	file.read(reinterpret_cast<char*>(&data), sizeof(T));
}

bool FileManager::read_pxla(const std::string& filename, std::vector<LayerDetails>& data, int& width, int& height)
{
	std::cout << "Reading PXLA file " << filename << std::endl;

	std::ifstream file(filename, std::ios::in | std::ios::binary);
	if (!file.is_open())
		return false;

	// Identifier sits at the top
	char identifier_check[8] = {};
	file.read(identifier_check, 8);

	bool is_pxla = true;
	for (int i = 0; i < 8; ++i) {
		if (identifier[i] != identifier_check[i])
			is_pxla = false;
	}

	if (!is_pxla)
	{
		std::cout << "File " << filename << " is not a valid PXLA file." << std::endl;
		file.close();
		return false;
	}

	// File version sits at the top
	unsigned short version = 0;
	read_data(file, version);

	// Then the image dimensions
	read_data(file, width);
	read_data(file, height);

	// Then the number of layers
	int layer_count = 0;
	read_data(file, layer_count);

	// Now go through each layer, read the data and uncompress
	size_t image_size = static_cast<size_t>(width) * static_cast<size_t>(height) * 4; // 4 - RGBA, each being 1 byte
	for (int i = 0; i < layer_count; ++i) {
		LayerDetails layer;

		// Name of the layer
		int name_len = 0;
		read_data(file, name_len);

		char* name = new char[name_len];
		file.read(name, name_len);
		for (int i = 0; i < name_len; ++i) {
			layer.name += name[i];
		}
		delete[] name;

		// Alpha
		read_data(file, layer.alpha);

		// Visibiliy
		read_data(file, layer.visible);

		// Read the compressed data - remember to delete[] layer.data when done!
		uint32_t size = 0;
		read_data(file, size);

		GLColour* image_data = new GLColour[image_size / 4];
		unsigned char* compressed = new unsigned char[size];

		file.read(reinterpret_cast<char*>(compressed), size);

		unsigned long uncompressed_size = static_cast<unsigned long>(image_size);
		uncompress(reinterpret_cast<unsigned char*>(image_data), &uncompressed_size, compressed, size);

		delete[] compressed;

		layer.data = image_data;
		data.push_back(layer);
	}

	file.close();
	return true;
}

// Helper function to write a variable
template<typename T>
void write_data(std::ofstream& file, const T& data)
{
	file.write(reinterpret_cast<const char*>(&data), sizeof(T));
}

bool FileManager::write_pxla(const std::string& filename, std::vector<LayerDetails>& data, int width, int height)
{
	std::cout << "Saving PXLA file " << filename << std::endl;

	std::ofstream file(filename, std::ios::out | std::ios::binary);
	if (!file.is_open())
		return false;

	// Identifier sits at the top
	file.write(identifier, 8);

	// File version goes next
	write_data(file, file_version);

	// Then the image dimensions
	write_data(file, width);
	write_data(file, height);

	// Then the number of layers
	write_data(file, static_cast<int>(data.size()));

	// Now go through each layer, compress data and write
	size_t image_size = static_cast<size_t>(width) * static_cast<size_t>(height) * 4; // 4 - RGBA, each being 1 byte
	unsigned char* compressed_data = new unsigned char[image_size];
	for (auto& layer : data) {
		// Name
		write_data(file, static_cast<int>(layer.name.length()));
		file.write(layer.name.c_str(), layer.name.length());

		// Alpha
		write_data(file, layer.alpha);

		// Visibility
		write_data(file, layer.visible);

		// The actual image data
		memset(compressed_data, 0, image_size);
		unsigned long size = static_cast<unsigned long>(image_size);
		compress(compressed_data, &size, reinterpret_cast<const unsigned char*>(layer.data), static_cast<unsigned long>(image_size));

		write_data(file, static_cast<uint32_t>(size));
		file.write(reinterpret_cast<char*>(compressed_data), size);
	}

	delete[] compressed_data;
	file.close();
	return true;
}
