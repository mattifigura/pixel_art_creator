#include "FileDialog.h"
#include "Button.h"
#include "FileManager.h"
#include "ItemList.h"
#include "ProgramManager.h"
#include "ScrollView.h"
#include "TextEdit.h"
#include "Util.h"

// Stupid Windows backspace in filenames
#ifdef _WIN32
const std::string file_seperator = "\\";
#else
const std::string file_seperator = "/";
#endif

FileDialog::FileDialog(const vector2df& pos, const Font* font)
	: WindowPane(pos, vector2df(600.f, 420.f), font, "Open File")
	, _path(std::filesystem::current_path())
	, _history()
	, _history_position(0)
	, _font(font)
	, _mode(FileDialogMode::file_open)
	, _filters()
	, _previous_path_text("")
	, _directory_scroll(new ScrollView(vector2df(6.f, get_titlebar_height() + 40.f), get_content_size() - vector2df(8.f, 78.f), this))
	, _directory(new ItemList(vector2df(), _directory_scroll->get_size(), 24.f, _directory_scroll))
	, _back_button(new Button(vector2df(6.f, get_titlebar_height() + 4.f), vector2df(32.f, 32.f), "B", font, this))
	, _forward_button(new Button(vector2df(42.f, get_titlebar_height() + 4.f), vector2df(32.f, 32.f), "F", font, this))
	, _up_button(new Button(vector2df(78.f, get_titlebar_height() + 4.f), vector2df(32.f, 32.f), "U", font, this))
	, _new_folder_button(nullptr) // Are we going to bother?
	, _ok_button(new Button(vector2df(get_content_size().x - 206.f, get_titlebar_height() + get_content_size().y - 34.f), vector2df(100.f, 32.f), "Open", font, this))
	, _cancel_button(new Button(vector2df(get_content_size().x - 102.f, get_titlebar_height() + get_content_size().y - 34.f), vector2df(100.f, 32.f), "Cancel", font, this))
	, _path_text(new TextEdit(vector2df(114.f, get_titlebar_height() + 4.f), vector2df(get_content_size().x - 118.f, 32.f), font, this))
	, _filename_text(new TextEdit(vector2df(vector2df(6.f, get_titlebar_height() + get_content_size().y - 34.f)), vector2df(get_content_size().x - 216.f, 32.f), font, this))
	, _confirm_window(new WindowPane(vector2df(), vector2df(300.f, 120.f), font, "Overwrite?"))
	, _ok_function(nullptr)
	, _folder_texture(new Texture)
	, _png_file_texture(new Texture)
	, _pxla_file_texture(new Texture)
{
	set_depth(10000000);
	
	{
		int width, height;
		std::vector<GLColour> data;
		if (FileManager::read_png(FileManager::_executable_directory + "media/folder.png", data, width, height))
			_folder_texture->create_texture_from_buffer(data, width, height);

		if (FileManager::read_png(FileManager::_executable_directory + "media/png_file.png", data, width, height))
			_png_file_texture->create_texture_from_buffer(data, width, height);

		if (FileManager::read_png(FileManager::_executable_directory + "media/pxla_file.png", data, width, height))
			_pxla_file_texture->create_texture_from_buffer(data, width, height);
	}

	set_keep_focus(true);
	_history.push_back(_path);

	_path_text->set_text(_path.string());
	_previous_path_text = _path_text->get_text();

	_directory_scroll->add_element(_directory);
	
	_directory_scroll->set_vertical_scrollbar_toggle_function([this](bool visible) {
		float width = _directory_scroll->get_size().x;
		_directory->set_size(vector2df(visible ? width - 20.f : width, _directory->get_size().y));
	});

	_directory->set_can_swap(false);
	_directory->set_double_click_function([this](size_t selected) {
		if (_confirm_window->is_visible() || _directory->empty())
			return;

		const std::vector<GUIElement*>& items = _directory->get_item(selected);
		std::string filename = dynamic_cast<Text*>(items[0])->get_text();
		std::filesystem::directory_entry entry(_path.string() + file_seperator + filename);

		// If a directory has been chosen, navigate into the directory
		if (entry.is_directory())
			go_to_path(entry.path());
		else
			_ok_button->press();
	});
	_directory->set_select_function([this](size_t selected, bool not_used) {
		if (_confirm_window->is_visible() || _directory->empty())
			return;

		const std::vector<GUIElement*>& items = _directory->get_item(selected);
		std::string filename = dynamic_cast<Text*>(items[0])->get_text();
		std::filesystem::directory_entry entry(_path.string() + file_seperator + filename);

		if (!entry.is_directory())
			_filename_text->set_text(filename);
	});

	_back_button->set_function([this](GUIElement* button, bool ignored) {
		if (_confirm_window->is_visible())
			return;

		if (_history_position > 0)
		{
			_path = _history[--_history_position];
			list_current_directory();
		}
	});

	_forward_button->set_function([this](GUIElement* button, bool ignored) {
		if (_confirm_window->is_visible())
			return;

		if (_history_position < _history.size() - 1)
		{
			_path = _history[++_history_position];
			list_current_directory();
		}
	});

	_up_button->set_function([this](GUIElement* button, bool ignored) {
		if (_confirm_window->is_visible())
			return;

		std::filesystem::path up_path = _path.parent_path();

		if (up_path != _path)
			go_to_path(up_path);
	});

	_ok_button->set_function([this](GUIElement* button, bool ignored) {
		if (_confirm_window->is_visible())
			return;

		if (_ok_function)
		{
			std::filesystem::path file_path(_path.string() + file_seperator + _filename_text->get_text());

			if (!_filename_text->get_text().empty())
			{
				if (_mode == FileDialogMode::file_open || _mode == FileDialogMode::file_import)
				{
					_ok_function(file_path.string());
					set_visible(false);
				}
				else
				{
					// Ask to overwrite if the file exists
					if (std::filesystem::directory_entry(file_path).exists())
					{
						_confirm_window->set_position(get_position() + (get_size() - _confirm_window->get_size()) * 0.5f);
						_confirm_window->set_visible(true);
					}
					else
					{
						_ok_function(file_path.string());
						set_visible(false);
					}
				}
			}
		}
		else
		{
			std::cout << "FileDialog has no OK function set, will do nothing" << std::endl;
			set_visible(false);
		}
	});

	_cancel_button->set_function([this](GUIElement* button, bool ignored) {
		if (_confirm_window->is_visible())
			return;

		set_visible(false);
	});

	_path_text->set_focus_function([this](GUIElement* element, bool focused) {
		if (_confirm_window->is_visible() || focused)
			return;

		if (_path_text->get_text().empty())
		{
			_path_text->set_text(_path.string());
			return;
		}

		std::string new_path_text = _path_text->get_text();

#ifdef _WIN32
		// Deal with shitty Windows file system, if we type C: we don't want that to be valid
		if (new_path_text.length() < 3)
		{
			_path_text->set_text(_path.string());
			return;
		}
#endif

		if (new_path_text.substr(new_path_text.length() - 1) == file_seperator)
		{
			std::filesystem::path new_path_test(new_path_text);
			if (new_path_test != new_path_test.root_path())
				new_path_text = new_path_text.substr(0, new_path_text.length() - 1);
		}

		// See if we've typed a valid path
		std::filesystem::path new_path(new_path_text);
		std::filesystem::directory_entry new_entry(new_path);
		if (new_entry.exists() && new_entry.is_directory())
			go_to_path(new_path);
			
		_path_text->set_text(_path.string());
		_previous_path_text = _path_text->get_text();
	});
		
	_confirm_window->set_depth(get_depth() + 100);
	_confirm_window->set_can_resize(false);

	vector2df confirm_content_size = _confirm_window->get_content_size();
	float confirm_title_height = _confirm_window->get_titlebar_height();

	new Text(vector2df(6.f, confirm_title_height + 4.f), confirm_content_size, "File exists. Overwrite?", font, _confirm_window);

	Button* confirm_yes_button = new Button(vector2df(confirm_content_size.x - 206.f, confirm_title_height + confirm_content_size.y - 34.f), vector2df(100.f, 32.f), "Yes", font, _confirm_window);
	confirm_yes_button->set_function([this](GUIElement* button, bool ignored) {
		_ok_function(_path.string() + file_seperator + _filename_text->get_text());
		_confirm_window->set_visible(false);
		set_visible(false);
	});

	Button* confirm_no_button = new Button(vector2df(confirm_content_size.x - 102.f, confirm_title_height + confirm_content_size.y - 34.f), vector2df(100.f, 32.f), "No", font, _confirm_window);
	confirm_no_button->set_function([this](GUIElement* button, bool ignored) {
		_confirm_window->set_visible(false);
	});

	ProgramManager::instance().add_gui_element(_confirm_window);
	_confirm_window->set_visible(false);
}

FileDialog::~FileDialog()
{

}

void FileDialog::list_current_directory()
{
	_directory->clear();
	_directory_scroll->scroll_to_top();
	_directory_scroll->set_position(_directory_scroll->get_position());
	_path_text->set_text(_path.string());
	_filename_text->set_text("");
	
	std::vector<std::filesystem::directory_entry> entries;
	std::filesystem::directory_iterator it(_path);
	for (auto& entry : it) {
#ifdef _WIN32
		if (entry._Is_symlink_or_junction())
#else
		if (entry.is_symlink())
#endif
			continue;

		if (!_filters.empty() && !entry.is_directory())
		{
			bool passes = false;
			for (auto filter : _filters) {
				if (entry.path().extension() == filter)
					passes = true;
			}

			if (!passes)
				continue;
		}

		entries.push_back(entry);
	}

	std::sort(entries.begin(), entries.end(), [](const std::filesystem::directory_entry& one, const std::filesystem::directory_entry& two) {
		// Sort by folders first
		if (one.is_directory() == two.is_directory())
			return one.path() < two.path();
		else
			return one.is_directory();
	});

	for (auto& entry : entries) {
		Text* file_name_text = new Text(vector2df(24.f, 0.f), vector2df(584.f, 24.f), entry.path().filename().string(), _font, _directory);
		file_name_text->set_expand_past_width(true, false);
		file_name_text->set_shadowed(true, false);
		_directory->add_item(file_name_text);

		ColourRect* image = new ColourRect(vector2df(0.f, 2.f), vector2df(20.f, 16.f), GLColour(), _directory, true);
		if (entry.is_directory())
			image->set_texture(_folder_texture.get());
		else if (entry.path().extension() == ".pxla")
			image->set_texture(_pxla_file_texture.get());
		else if (entry.path().extension() == ".png")
			image->set_texture(_png_file_texture.get());
		_directory->add_item(image, _directory->get_list_size() - 1);
	}

	_directory_scroll->recalculate_size();
	_directory->select_item(0);
}

void FileDialog::set_filters(const std::string& filters)
{
	_filters.clear();
	_filters = util::split_by_regex(filters, " +");
}

void FileDialog::open(FileDialogMode mode)
{
	_mode = mode;

	_history.clear();
	_history.push_back(_path);
	_history_position = 0;

	switch (_mode)
	{
	case FileDialogMode::file_open:
		set_title("Open");
		break;
	case FileDialogMode::file_save:
		set_title("Save");
		break;
	case FileDialogMode::file_save_as:
		set_title("Save As");
		break;
	case FileDialogMode::file_import:
		set_title("Import");
		break;
	case FileDialogMode::file_export:
		set_title("Export");
		break;
	case FileDialogMode::file_export_as:
		set_title("Export As");
		break;
	default:
		break;
	}

	_ok_button->set_text(get_title());

	set_visible(true);
	list_current_directory();
}

void FileDialog::set_ok_function(const std::function<void(const std::string&)>& func)
{
	_ok_function = func;
}

void FileDialog::erase_the_future()
{
	// Ensure the history in the future (hehe) is clear
	if (_history_position < _history.size() - 1)
	{
		for (auto it = std::next(_history.begin(), _history_position + 1); it != _history.end(); ) {
			it = _history.erase(it);
		}
	}
}

void FileDialog::go_to_path(const std::filesystem::path& path)
{
	if (path != _path)
	{
		_path = path;

		erase_the_future();

		_history.push_back(_path);
		++_history_position;

		list_current_directory();
	}
}
