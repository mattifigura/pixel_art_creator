#ifndef IMAGE_PREVIEW_H
#define IMAGE_PREVIEW_H

#include "GUIElement.h"

class ColourRect;
class ImageLayer;
class WindowPane;

class ImagePreview : public GUIElement {
public:
	ImagePreview(WindowPane* window);
	~ImagePreview();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	void add_layer(const Texture* texture);
	void remove_layer(size_t index);

	ColourRect* get_layer(size_t index) const;

	void move_layer(size_t original, size_t dest);

	void set_layer_size(const vector2di& size);

	void update_visibility(const std::vector<ImageLayer*>& layers);

	virtual void set_position(const vector2df& pos);

	virtual bool mouse_event(int button, int action, int mods);
	virtual bool scroll_event(double x, double y);
	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:

private:
	WindowPane* _window;
	std::vector<ColourRect*> _layers;

	float _zoom;
	int _zoom_index;
	float _previous_zoom;
	vector2df _layer_size;
	vector2df _layer_position;
	vector2df _layer_adjust;
	vector2df _previous_mouse_position;
	bool _moving_perspective;
};

#endif