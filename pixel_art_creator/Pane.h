#ifndef PANE_H
#define PANE_H

#include "GUIElement.h"

class Pane : public GUIElement {
public:
	Pane(const vector2df& pos, const vector2df& size, GUIElement* parent = nullptr, bool force_mesh = false);
	~Pane();
	
	virtual void build();

protected:
	virtual void update_mesh();

private:

};

#endif