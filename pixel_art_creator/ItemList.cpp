#include "ItemList.h"
#include "ProgramManager.h"

ItemList::ItemList(const vector2df& pos, const vector2df& size, float row_size, GUIElement* parent)
	: GUIElement(pos, size, parent, true)
	, _row_size(row_size)
	, _selected(0)
	, _hovered(0)
	, _mouse_hovered(false)
	, _can_swap(true)
	, _swapping(false)
	, _move_instead_of_swap(false)
	, _just_selected(false)
	, _reverse(false)
	, _list()
	, _select_function(nullptr)
	, _swap_function(nullptr)
	, _double_click_function(nullptr)
{

}

ItemList::~ItemList()
{
	for (auto list : _list) {
		list.clear();
	}

	_list.clear();
}

void ItemList::build()
{
	GLColour selected_colour(255, 170);
	get_mesh().add_quad(GLVertex2D(vector2df(), selected_colour),
		GLVertex2D(vector2df(get_size().x, 0.f), selected_colour),
		GLVertex2D(vector2df(get_size().x, _row_size), selected_colour),
		GLVertex2D(vector2df(0.f, _row_size), selected_colour));

	GLColour hovered_colour(0, 0);
	get_mesh().add_quad(GLVertex2D(vector2df(), hovered_colour),
		GLVertex2D(vector2df(get_size().x, 0.f), hovered_colour),
		GLVertex2D(vector2df(get_size().x, _row_size), hovered_colour),
		GLVertex2D(vector2df(0.f, _row_size), hovered_colour));
}

bool ItemList::update(int mouse_x, int mouse_y)
{
	if (is_visible())
	{
		// Update the hovered index
		size_t previous_hovered(_hovered);
		_hovered = std::min(static_cast<size_t>((mouse_y - get_absolute_position().y) / _row_size), _list.size() - 1);

		bool previous_mouse_hovered(_mouse_hovered);
		_mouse_hovered = is_mouse_hovered();

		if ((previous_hovered != _hovered && _mouse_hovered) || previous_mouse_hovered != _mouse_hovered)
		{
			GLColour hovered_colour(255, _mouse_hovered ? 80 : 0);
			float y_pos = static_cast<float>(_hovered) * _row_size;

			get_mesh().set_quad_position(1, vector2df(0.f, y_pos), vector2df(get_size().x, y_pos), vector2df(get_size().x, y_pos + _row_size), vector2df(0.f, y_pos + _row_size));
			get_mesh().set_quad_colour(1, hovered_colour);
		}
	}

	return GUIElement::update(mouse_x, mouse_y);
}

size_t ItemList::add_item(GUIElement* element, int64_t where)
{
	// If where < 0 this will add a new list item, otherwise it will add it to an existing row
	bool new_item = false;
	size_t position = (where > -1 && static_cast<size_t>(where) < _list.size()) ? static_cast<size_t>(where) : _list.size();
	if (where < 0 || static_cast<size_t>(where) >= _list.size())
	{
		_list.push_back(std::vector<GUIElement*>());
		new_item = true;
	}

	element->force_mesh();
	element->set_size(vector2df(element->get_size().x, _row_size - 4.f));

	if (_reverse)
	{
		if (new_item)
		{
			for (size_t i = 0; i < _list.size() - 1; ++i) {
				for (auto& item : _list[i]) {
					item->set_position(item->get_position() + vector2df(0.f, _row_size));
				}
			}
		}
		
		element->set_position(vector2df(4.f, static_cast<float>(_list.size() - 1 - position) * _row_size) + element->get_position());
	}
	else
		element->set_position(vector2df(4.f, static_cast<float>(position) * _row_size) + element->get_position());

	ProgramManager::instance().add_gui_element_queued(element);

	_list[position].push_back(element);
	select_item(_selected);

	set_size(vector2df(get_size().x, static_cast<float>(_list.size()) * _row_size));

	return where;
}

void ItemList::remove_item(size_t where)
{
	// Deletes an entire row of items
	if (where < _list.size())
	{
		for (auto& element : _list[where]) {
			element->kill();
			remove_child(element);
		}
		
		_list[where].clear();

		if (_reverse)
		{
			for (int i = std::max(static_cast<int>(where) - 1, 0); i >= 0; --i) {
				for (auto& element : _list[i]) {
					element->set_position(element->get_position() - vector2df(0.f, _row_size));
				}
			}
			_list.erase(std::next(_list.begin(), where));
		}
		else
		{
			_list.erase(std::next(_list.begin(), where));
			for (size_t i = where; i < _list.size(); ++i) {
				for (auto& element : _list[i]) {
					element->set_position(element->get_position() - vector2df(0.f, _row_size));
				}
			}
		}

		if (_selected >= _list.size())
			select_item(_selected - 1);
		else
			select_item(_selected);

		_list.shrink_to_fit();
	}

	set_size(vector2df(get_size().x, static_cast<float>(_list.size()) * _row_size));
}

void ItemList::clear()
{
	for (auto list : _list) {
		for (auto& element : list) {
			element->kill();
			remove_child(element);
		}
		list.clear();
	}

	_list.clear();
	_list.shrink_to_fit();

	select_item(0);
}

bool ItemList::empty() const
{
	return _list.empty();
}

void ItemList::set_can_swap(bool swap)
{
	_can_swap = swap;
}

void ItemList::set_move_instead_of_swap(bool move)
{
	_move_instead_of_swap = move;
}

void ItemList::set_reverse(bool reverse)
{
	_reverse = reverse;
}

void ItemList::select_item(size_t where)
{
	if (where < _list.size() || (_list.empty() && where == 0))
	{
		_selected = where;

		float y_pos = static_cast<float>(where) * _row_size;
		get_mesh().set_quad_position(0, vector2df(0.f, y_pos), vector2df(get_size().x, y_pos), vector2df(get_size().x, y_pos + _row_size), vector2df(0.f, y_pos + _row_size));

		if (where < _list.size() && _select_function)
			_select_function(get_selected_index(), false);
	}
}

size_t ItemList::get_selected_index() const
{
	return _reverse ? _list.size() - 1 - _selected : _selected;
}

const std::vector<GUIElement*>& ItemList::get_selected_item() const
{
	return _list[get_selected_index()];
}

const std::vector<GUIElement*>& ItemList::get_item(size_t where) const
{
	return _list[std::min(where, _list.size() - 1)];
}

void ItemList::swap_items(size_t one, size_t two, bool do_callback)
{
	// Swap two items
	std::swap(_list[one], _list[two]);

	for (auto& element : _list[one]) {
		element->set_position(element->get_position() - vector2df(0.f, (static_cast<float>(one) - static_cast<float>(two)) * _row_size));
	}
	for (auto& element : _list[two]) {
		element->set_position(element->get_position() - vector2df(0.f, (static_cast<float>(two) - static_cast<float>(one)) * _row_size));
	}

	if (do_callback && _swap_function)
		_swap_function(one, two);
}

void ItemList::move_items(size_t one, size_t two, bool do_callback)
{
	// Move the item from one to two
	auto list_item = _list[one];
	if (two > one)
	{
		for (size_t i = one; i < two; ++i) {
			_list[i] = _list[i + 1];

			for (auto& element : _list[i]) {
				element->set_position(element->get_position() + vector2df(0.f, _reverse ? _row_size : -_row_size));
			}
		}
	}
	else
	{
		for (size_t i = one; i > two; --i) {
			_list[i] = _list[i - 1];

			for (auto& element : _list[i]) {
				element->set_position(element->get_position() + vector2df(0.f, _reverse ? -_row_size : _row_size));
			}
		}
	}

	_list[two] = list_item;
	for (auto& element : _list[two]) {
		element->set_position(element->get_position() - vector2df(0.f, (static_cast<float>(two) - static_cast<float>(one)) * _row_size));
	}

	if (do_callback && _swap_function)
		_swap_function(one, two);
}

size_t ItemList::get_element_list_index(GUIElement* element) const
{
	size_t list_index = get_list_size();

	for (size_t i = 0; i < get_list_size(); ++i) {
		if (std::find(_list[i].begin(), _list[i].end(), element) != _list[i].end())
		{
			list_index = i;
			break;
		}
	}

	return list_index;
}

size_t ItemList::get_list_size() const
{
	return _list.size();
}

void ItemList::set_select_function(const std::function<void(size_t, bool)>& func)
{
	_select_function = func;
}

void ItemList::set_swap_function(const std::function<void(size_t, size_t)>& func)
{
	_swap_function = func;
}

void ItemList::set_double_click_function(const std::function<void(size_t)>& func)
{
	_double_click_function = func;
}

bool ItemList::mouse_event(int button, int action, int mods)
{
	if (action == GLFW_DOUBLE_CLICK && button == GLFW_MOUSE_BUTTON_LEFT && _mouse_hovered && _selected == _hovered)
	{
		if (_double_click_function)
			_double_click_function(get_selected_index());

		return true;
	}
	else if ((action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK) && button == GLFW_MOUSE_BUTTON_LEFT && _mouse_hovered)
	{
		// Figure out which element is pressed
		_just_selected = _selected != _hovered;
		select_item(_hovered);
		_swapping = _can_swap;
		return true;
	}
	else if (action == GLFW_RELEASE && button == GLFW_MOUSE_BUTTON_LEFT)
	{
		bool ret_value = false;
		
		if (_mouse_hovered && _swapping && _selected != _hovered)
		{
			if (_move_instead_of_swap)
				move_items(get_selected_index(), get_hovered_index());
			else
				swap_items(get_selected_index(), get_hovered_index());
			ret_value = true;
		}
		else if (!_just_selected && _mouse_hovered && _selected == _hovered && _select_function)
		{
			_select_function(get_selected_index(), true);
			ret_value = true;
		}

		_swapping = false;
		_just_selected = false;

		return ret_value;
	}

	return false;
}

size_t ItemList::get_hovered_index() const
{
	return _reverse ? _list.size() - 1 - _hovered : _hovered;
}