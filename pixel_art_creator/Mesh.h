#ifndef MESH_H
#define MESH_H

#include "GLBase.h"
#include <vector>

class Mesh {
public:
	Mesh();
	~Mesh();

	GLuint get_vbo() const;
	GLuint get_ebo() const;
	GLuint get_index_buffer_size() const;

	bool buffer();
	bool update_vbo();

	bool is_dirty();
	void clear_data();

	size_t add_quad(const GLVertex2D& one, const GLVertex2D& two, const GLVertex2D& three, const GLVertex2D& four, bool flip = false);
	void set_quad_colour(size_t quad, const GLColour& colour);
	void set_quad_colour(size_t quad, const GLColour& colour1, const GLColour& colour2, const GLColour& colour3, const GLColour& colour4);
	void set_quad_position(size_t quad, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4);
	void translate_quad_position(size_t quad, const vector2df& translate);
	void set_quad_uv(size_t quad, const vector2df& uv1, const vector2df& uv2, const vector2df& uv3, const vector2df& uv4);
	void set_quad_uv(size_t quad, const vector4df& uv);

	size_t get_quad_count() const;
	bool get_quad_position(size_t quad, vector2df& pos1, vector2df& pos2, vector2df& pos3, vector2df& pos4) const;

protected:

private:
	GLuint _vbo;
	GLuint _ebo;

	std::vector<GLVertex2D> _vertices;
	std::vector<GLuint> _indices;

	bool _dirty;

	void clear_buffers();
};

#endif