#ifndef GL_SHADER_PROGRAM_H_
#define GL_SHADER_PROGRAM_H_

#include "GLBase.h"
#include "Matrix.h"
#include <string>

class GLShaderProgram {
public:
	GLShaderProgram();
	GLShaderProgram(const std::string& vertex, const std::string& fragment, const std::string& geometry = std::string(""));
	~GLShaderProgram();

	void clear();
	bool compile_shaders();
	bool recompile_shaders();

	void set_shaders(const std::string& vertex, const std::string& fragment, const std::string& geometry = std::string(""));

	void bind();
	void unbind();

	GLint get_attrib_location(const GLchar* attrib);
	GLint get_uniform_location(const GLchar* attrib);

	void set_ortho_projection(int width, int height);
	void set_perspective_projection(float angle, float ratio, float near, float far, bool defaults = true);

	const matrix4f& get_projection_matrix() const;

protected:

private:
	GLuint _vertex_shader;
	GLuint _fragment_shader;
	GLuint _geometry_shader;
	GLuint _shader_program;
	matrix4f _proj_matrix;

	GLfloat _angle;
	GLfloat _ratio;
	GLfloat _near;
	GLfloat _far;
	GLfloat _current_angle;

	std::string _vertex;
	std::string _fragment;
	std::string _geometry;
};

#endif
