#ifndef EDIT_STATE_H
#define EDIT_STATE_H

// Base class for edit states - used for undoing and redoing

#include <stddef.h>

class EditState {
public:
	EditState(size_t index) : _layer_index(index) {}
	virtual ~EditState() {}

	virtual void undo() = 0;
	virtual void redo() = 0;

	virtual bool validate_state() { return true; } // Used to make sure we're not undoing no actual change
	virtual bool compare_to(EditState* other) { return false; } // Used to make sure we're not undoing no actual change, and to compress the same two repeated actions

protected:
	size_t _layer_index; // Every EditState will need to know which layer it is working on

private:
	
};

#endif
