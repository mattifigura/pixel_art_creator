#include "ImageLayer.h"
#include "Texture.h"
#include "Util.h"

ImageLayer::ImageLayer(const vector2df& pos, const vector2df& size, GUIElement* parent)
	: GUIElement(pos, size, parent, true)
	, _layer(new Texture())
{
	// We have our own texture per layer
	_layer->create_colour_image(static_cast<int>(size.x), static_cast<int>(size.y), GLColour());
	set_texture(_layer.get());
}

ImageLayer::~ImageLayer()
{

}

void ImageLayer::build()
{
	Mesh& mesh = get_mesh();
	vector2df size = get_size();

	GLColour col;

	_mesh_start = mesh.add_quad(GLVertex2D(vector2df(), col, vector2df()),
		GLVertex2D(vector2df(size.x, 0.f), col, vector2df(1.f, 0.f)),
		GLVertex2D(size, col, vector2df(1.f, 1.f)),
		GLVertex2D(vector2df(0.f, size.y), col, vector2df(0.f, 1.f)));
}

void ImageLayer::clear_to_colour(const GLColour& col)
{
	_layer->create_colour_image(static_cast<int>(get_size().x), static_cast<int>(get_size().y), col);
}

void ImageLayer::load_from_buffer(const std::vector<GLColour>& data, int width, int height)
{
	_layer->create_texture_from_buffer(data, width, height);
}

void ImageLayer::load_from_buffer(const GLColour* data, int width, int height)
{
	_layer->create_texture_from_buffer(reinterpret_cast<const unsigned char*>(data), width, height, GL_RGBA);
}

void ImageLayer::set_pixel(int x, int y, const GLColour& col)
{
	_layer->set_pixel(x, y, col);
}

const GLColour& ImageLayer::get_pixel(int x, int y) const
{
	return _layer->get_pixel(x, y);
}

void ImageLayer::buffer()
{
	_layer->buffer();
}

void ImageLayer::set_size(const vector2df& size)
{
	_layer->resize(static_cast<int>(size.x), static_cast<int>(size.y));

	if (get_mesh().get_index_buffer_size() > 0)
		get_mesh().set_quad_position(_mesh_start, vector2df(), vector2df(size.x, 0.f), size, vector2df(0.f, size.y));

	GUIElement::set_size(size);
}