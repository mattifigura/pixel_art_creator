#include "ColourPicker.h"
#include "FileManager.h"
#include "FontManager.h"
#include "Slider.h"
#include "Texture.h"
#include "TextEdit.h"
#include "Util.h"

const vector2df second_bar(260.f, 280.f);

ColourPicker::ColourPicker(const vector2df& pos, GUIElement* parent)
	: GUIElement(pos, vector2df(280.f, 380.f), parent, true)
	, _colours(new Texture)
	, _chosen_colour(255, 0, 0, 255)
	, _mouse_position()
	, _pixel_position()
	, _colour_position(254, 0)
	, _just_clicked(false)
	, _picking_colour(false)
	, _picking_hue(false)
	, _values_label(nullptr)
	, _red_edit(nullptr)
	, _green_edit(nullptr)
	, _blue_edit(nullptr)
	, _alpha_edit(nullptr)
	, _red_slider(nullptr)
	, _green_slider(nullptr)
	, _blue_slider(nullptr)
	, _alpha_slider(nullptr)
	, _colour_picked_callback(nullptr)
{
	_colours->create_gradient_image(255, 255, GLColour(), GLColour(255, 0, 0, 255), GLColour(0, 255), GLColour(0, 255));
	set_texture(_colours.get());

	const Font& font = FontManager::instance().get_resource(FileManager::_executable_directory + "media/OpenSans.ttf:{16");

	_values_label = new Text(vector2df(0.f, 288.f), vector2df(280.f, 72.f), "R: \nG: \nB: \nA: ", &font, this);
	_values_label->set_shadowed(true, false);

	auto colour_update_func = [this](GUIElement* element, const std::string& text) {
		_chosen_colour.r = atoi(_red_edit->get_text().c_str());
		_chosen_colour.g = atoi(_green_edit->get_text().c_str());
		_chosen_colour.b = atoi(_blue_edit->get_text().c_str());
		_chosen_colour.a = atoi(_alpha_edit->get_text().c_str());

		set_colour(_chosen_colour);
	};

	auto slider_update_func = [this](GUIElement* element, float value) {
		_chosen_colour.r = static_cast<GLubyte>(_red_slider->get_value() * 255.f);
		_chosen_colour.g = static_cast<GLubyte>(_green_slider->get_value() * 255.f);
		_chosen_colour.b = static_cast<GLubyte>(_blue_slider->get_value() * 255.f);
		_chosen_colour.a = static_cast<GLubyte>(_alpha_slider->get_value() * 255.f);

		set_colour(_chosen_colour);
	};

	// Red
	_red_edit = new TextEdit(vector2df(230.f, 286.f), vector2df(50.f, 24.f), &font, this, true);
	_red_edit->set_numbers_only(true, 0, 255, false);
	_red_edit->set_text("255");
	_red_edit->set_text_updated_function(colour_update_func);

	_red_slider = new Slider(vector2df(22.f, 286.f), vector2df(200.f, 24.f), this, true);
	_red_slider->set_value_update_function(slider_update_func);
	_red_slider->set_value(1.f);

	// Green
	_green_edit = new TextEdit(vector2df(230.f, 311.f), vector2df(50.f, 24.f), &font, this, true);
	_green_edit->set_numbers_only(true, 0, 255, false);
	_green_edit->set_text("0");
	_green_edit->set_text_updated_function(colour_update_func);

	_green_slider = new Slider(vector2df(22.f, 311.f), vector2df(200.f, 24.f), this, true);
	_green_slider->set_value_update_function(slider_update_func);

	// Blue
	_blue_edit = new TextEdit(vector2df(230.f, 336.f), vector2df(50.f, 24.f), &font, this, true);
	_blue_edit->set_numbers_only(true, 0, 255, false);
	_blue_edit->set_text("0");
	_blue_edit->set_text_updated_function(colour_update_func);

	_blue_slider = new Slider(vector2df(22.f, 336.f), vector2df(200.f, 24.f), this, true);
	_blue_slider->set_value_update_function(slider_update_func);

	// Alpha
	_alpha_edit = new TextEdit(vector2df(230.f, 361.f), vector2df(50.f, 24.f), &font, this, true);
	_alpha_edit->set_numbers_only(true, 0, 255, false);
	_alpha_edit->set_text("255");
	_alpha_edit->set_text_updated_function(colour_update_func);

	_alpha_slider = new Slider(vector2df(22.f, 361.f), vector2df(200.f, 24.f), this, true);
	_alpha_slider->set_value_update_function(slider_update_func);
	_alpha_slider->set_value(1.f);
}

ColourPicker::~ColourPicker()
{

}

void ColourPicker::build()
{
	Mesh& mesh = get_mesh();
	vector2df pos;
	vector2df size = get_size();
	vector2df border(4.f, 4.f);

	// Main colour
	_mesh_start = get_mesh().add_quad(GLVertex2D(pos, GLColour(), vector2df()),
		GLVertex2D(pos + vector2df(255.f, 0.f), GLColour(), vector2df(1.f, 0.f)),
		GLVertex2D(pos + vector2df(255.f, 255.f), GLColour(), vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, 255.f), GLColour(), vector2df(0.f, 1.f)));

	get_mesh().add_quad(GLVertex2D(pos + vector2df(0.f, 259.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(255.f, 259.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(255.f, 279.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(0.f, 279.f), GLColour(255, 0, 0, 255)));

	// Bar at the side
	float chunk_size = 42.5f;
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, 0.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, 0.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size), GLColour(255, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size), GLColour(255, 255, 0, 255)));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, chunk_size), GLColour(255, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size), GLColour(255, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 2.f), GLColour(0, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 2.f), GLColour(0, 255, 0, 255)));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 2.f), GLColour(0, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 2.f), GLColour(0, 255, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 3.f), GLColour(0, 255, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 3.f), GLColour(0, 255, 255, 255)));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 3.f), GLColour(0, 255, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 3.f), GLColour(0, 255, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 4.f), GLColour(0, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 4.f), GLColour(0, 0, 255, 255)));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 4.f), GLColour(0, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 4.f), GLColour(0, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 5.f), GLColour(255, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 5.f), GLColour(255, 0, 255, 255)));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 5.f), GLColour(255, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 5.f), GLColour(255, 0, 255, 255)),
		GLVertex2D(pos + vector2df(second_bar.y, chunk_size * 6.f), GLColour(255, 0, 0, 255)),
		GLVertex2D(pos + vector2df(second_bar.x, chunk_size * 6.f), GLColour(255, 0, 0, 255)));

	// The targeting lines
	GLColour inverse(255 - _chosen_colour.r, 255 - _chosen_colour.g, 255 - _chosen_colour.b, 255);
	get_mesh().add_quad(GLVertex2D(pos, inverse),
		GLVertex2D(pos + vector2df(255.f, 0.f), inverse),
		GLVertex2D(pos + vector2df(255.f, 1.f), inverse),
		GLVertex2D(pos + vector2df(0.f, 1.f), inverse));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(254.f, 0.f), inverse),
		GLVertex2D(pos + vector2df(255.f, 0.f), inverse),
		GLVertex2D(pos + vector2df(255.f, 255.f), inverse),
		GLVertex2D(pos + vector2df(254.f, 255.f), inverse));
	get_mesh().add_quad(GLVertex2D(pos + vector2df(second_bar.x, 0.f), inverse),
		GLVertex2D(pos + vector2df(second_bar.y, 0.f), inverse),
		GLVertex2D(pos + vector2df(second_bar.y, 1.f), inverse),
		GLVertex2D(pos + vector2df(second_bar.x, 1.f), inverse));
}

bool ColourPicker::update(int mouse_x, int mouse_y)
{
	bool dirty = false;

	_mouse_position.set(mouse_x, mouse_y);

	// Pick colour or hue
	vector2df absolute_position(get_absolute_position());
	vector2di previous_pixel(_pixel_position);
	//vector2df relative_mouse(static_cast<float>(mouse_x) - absolute_position.x, static_cast<float>(mouse_y) - absolute_position.y);
	_pixel_position.set(mouse_x - static_cast<int>(absolute_position.x), mouse_y - static_cast<int>(absolute_position.y));
	//vector2df pixel_position(static_cast<int>(relative_mouse.x), static_cast<int>(relative_mouse.y)); // Colour picker scale will always be (1, 1)
	_pixel_position.x = std::max(std::min(_pixel_position.x, 254), 0);
	_pixel_position.y = std::max(std::min(_pixel_position.y, 254), 0);

	if (_picking_hue)
	{
		// We only need the y value from _pixel_position
		if (_just_clicked || previous_pixel.y != _pixel_position.y)
		{
			// Set the "hue" based on the above value
			GLColour col = get_hue_rgb((static_cast<float>(_pixel_position.y) / 255.f) * 6.f);

			// Now change our texture
			_colours->create_gradient_image(255, 255, GLColour(), col, GLColour(0, 255), GLColour(0, 255));

			// Need to update our picking target
			Mesh& mesh = get_mesh();
			vector2df pos(0.f, static_cast<float>(_pixel_position.y));

			GLColour inverse(255 - col.r, 255 - col.g, 255 - col.b, 255);
			get_mesh().set_quad_position(10, vector2df(second_bar.x, pos.y),
				vector2df(second_bar.y, pos.y),
				vector2df(second_bar.y, pos.y + 1.f),
				vector2df(second_bar.x, pos.y + 1.f));

			get_mesh().set_quad_colour(10, inverse);

			dirty = true;
		}
	}
	
	if (dirty || _picking_colour)
	{
		if (_picking_colour)
			_colour_position = _pixel_position;
		
		// We need to keep the x value inside too
		if (_just_clicked || previous_pixel != _pixel_position)
		{
			// Pick the colour from our texture
			_chosen_colour = _colours->get_pixel(_colour_position.x, _colour_position.y);

			// Need to update our picking target
			Mesh& mesh = get_mesh();
			vector2df pos(static_cast<float>(_colour_position.x), static_cast<float>(_colour_position.y));

			GLColour inverse(255 - _chosen_colour.r, 255 - _chosen_colour.g, 255 - _chosen_colour.b, 255);
			get_mesh().set_quad_position(8, vector2df(0.f, pos.y),
				vector2df(255.f, pos.y),
				vector2df(255.f, pos.y + 1.f),
				vector2df(0.f, pos.y + 1.f));
			get_mesh().set_quad_position(9, vector2df(pos.x, 0.f),
				vector2df(pos.x + 1.f, 0.f),
				vector2df(pos.x + 1.f, 255.f),
				vector2df(pos.x, 255.f));

			get_mesh().set_quad_colour(1, _chosen_colour);
			get_mesh().set_quad_colour(8, inverse);
			get_mesh().set_quad_colour(9, inverse);

			// Update our text
			_red_edit->set_text(std::to_string(_chosen_colour.r));
			_green_edit->set_text(std::to_string(_chosen_colour.g));
			_blue_edit->set_text(std::to_string(_chosen_colour.b));
			_alpha_edit->set_text(std::to_string(_chosen_colour.a));

			_red_edit->buffer();
			_green_edit->buffer();
			_blue_edit->buffer();
			_alpha_edit->buffer();

			_red_slider->set_value(static_cast<float>(_chosen_colour.r) / 255.f);
			_green_slider->set_value(static_cast<float>(_chosen_colour.g) / 255.f);
			_blue_slider->set_value(static_cast<float>(_chosen_colour.b) / 255.f);
			_alpha_slider->set_value(static_cast<float>(_chosen_colour.a) / 255.f);

			if (_colour_picked_callback)
				_colour_picked_callback(_chosen_colour);

			dirty = true;
		}
	}
	
	_just_clicked = false;

	dirty |= GUIElement::update(mouse_x, mouse_y);
	return dirty;
}

bool ColourPicker::mouse_event(int button, int action, int mods)
{
	vector2df absolute_position(get_absolute_position());
	if (button == GLFW_MOUSE_BUTTON_1)
	{
		if (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK)
		{
			if (util::is_point_inside(static_cast<vector2df>(_mouse_position), absolute_position, absolute_position + vector2df(255.f, 255.f)))
			{
				_picking_colour = true;
				_just_clicked = true;
				return true;
			}
			else if (util::is_point_inside(static_cast<vector2df>(_mouse_position), absolute_position + vector2df(259.f, 0.f), absolute_position + vector2df(279.f, 255.f)))
			{
				_picking_hue = true;
				_just_clicked = true;
				return true;
			}
		}
		else if (action == GLFW_RELEASE)
		{
			_picking_colour = false;
			_picking_hue = false;
		}
	}
	
	return false;
}

const GLColour& ColourPicker::get_chosen_colour() const
{
	return _chosen_colour;
}

void ColourPicker::set_colour(const GLColour& col)
{
	// We need to convert to HSV to set our picker correctly
	HSV hsv = util::rgb_to_hsv(col);
	_chosen_colour = col;

	// Now update everything
	Mesh& mesh = get_mesh();
	vector2df pos(hsv.sat * 254.f, (1.f - hsv.val) * 254.f);
	_colour_position.set(static_cast<int>(pos.x), static_cast<int>(pos.y));
	pos.set(static_cast<float>(_colour_position.x), static_cast<float>(_colour_position.y));

	GLColour inverse(255 - _chosen_colour.r, 255 - _chosen_colour.g, 255 - _chosen_colour.b, 255);
	get_mesh().set_quad_position(8, vector2df(0.f, pos.y),
		vector2df(255.f, pos.y),
		vector2df(255.f, pos.y + 1.f),
		vector2df(0.f, pos.y + 1.f));
	get_mesh().set_quad_position(9, vector2df(pos.x, 0.f),
		vector2df(pos.x + 1.f, 0.f),
		vector2df(pos.x + 1.f, 255.f),
		vector2df(pos.x, 255.f));

	get_mesh().set_quad_colour(1, _chosen_colour);
	get_mesh().set_quad_colour(8, inverse);
	get_mesh().set_quad_colour(9, inverse);

	pos.set(0.f, (hsv.hue / 360.f) * 255.f);

	GLColour hue_col = get_hue_rgb((hsv.hue / 360.f) * 6.f);

	_colours->create_gradient_image(255, 255, GLColour(), hue_col, GLColour(0, 255), GLColour(0, 255));

	inverse.set(255 - hue_col.r, 255 - hue_col.g, 255 - hue_col.b, 255);
	get_mesh().set_quad_position(10, vector2df(second_bar.x, pos.y),
		vector2df(second_bar.y, pos.y),
		vector2df(second_bar.y, pos.y + 1.f),
		vector2df(second_bar.x, pos.y + 1.f));

	get_mesh().set_quad_colour(10, inverse);

	// Update our text
	_red_edit->set_text(std::to_string(_chosen_colour.r));
	_green_edit->set_text(std::to_string(_chosen_colour.g));
	_blue_edit->set_text(std::to_string(_chosen_colour.b));
	_alpha_edit->set_text(std::to_string(_chosen_colour.a));

	_red_edit->buffer();
	_green_edit->buffer();
	_blue_edit->buffer();
	_alpha_edit->buffer();

	_red_slider->set_value(static_cast<float>(_chosen_colour.r) / 255.f);
	_green_slider->set_value(static_cast<float>(_chosen_colour.g) / 255.f);
	_blue_slider->set_value(static_cast<float>(_chosen_colour.b) / 255.f);
	_alpha_slider->set_value(static_cast<float>(_chosen_colour.a) / 255.f);

	if (_colour_picked_callback)
		_colour_picked_callback(_chosen_colour);
}

void ColourPicker::set_colour_picked_function_callback(const std::function<void(const GLColour&)>& func)
{
	_colour_picked_callback = func;
}

GLColour ColourPicker::get_hue_rgb(float hue) const
{
	GLColour col(0, 0, 0, 255);
	if (hue < 1.f)
	{
		col.r = 255;
		col.g = static_cast<GLubyte>(hue * 255.f);
	}
	else if (hue >= 1.f && hue < 2.f)
	{
		col.r = static_cast<GLubyte>((1.f - (hue - 1.f)) * 255.f);
		col.g = 255;
	}
	else if (hue >= 2.f && hue < 3.f)
	{
		col.g = 255;
		col.b = static_cast<GLubyte>((hue - 2.f) * 255.f);
	}
	else if (hue >= 3.f && hue < 4.f)
	{
		col.g = static_cast<GLubyte>((1.f - (hue - 3.f)) * 255.f);
		col.b = 255;
	}
	else if (hue >= 4.f && hue < 5.f)
	{
		col.r = static_cast<GLubyte>((hue - 4.f) * 255.f);
		col.b = 255;
	}
	else if (hue >= 5.f)
	{
		col.r = 255;
		col.b = static_cast<GLubyte>((1.f - (hue - 5.f)) * 255.f);
	}

	return col;
}