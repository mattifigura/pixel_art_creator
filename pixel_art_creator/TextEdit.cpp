#include "TextEdit.h"
#include "ProgramManager.h"

#include <codecvt>
#include <locale>

TextEdit::TextEdit(const vector2df& pos, const vector2df& size, const Font* font, GUIElement* parent, bool force_mesh)
	: GUIElement(pos, size, parent, force_mesh)
	, _text(new Text(vector2df(4.f, (size.y - font->get_height()) * 0.5f), size, "", font, this))
	, _numbers_only(false)
	, _lower_number(0.f)
	, _upper_number(1.f)
	, _allow_decimals(true)
	, _erasing(false)
	, _deleting(false)
	, _mouse_position()
	, _cursor_position(0)
	, _cursor_offset(0.f)
	, _focus_func(nullptr)
	, _text_updated_func(nullptr)
{
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));

	_text->set_expand_past_width(true, false);
	_text->set_shadowed(true, false);
}

TextEdit::~TextEdit()
{

}

void TextEdit::build()
{
	// Add a background and a cursor
	vector2df pos = get_position();
	vector2df size = get_size();

	if (!get_parent() || get_mesh_ptr())
		pos = vector2df();

	GLColour col(70, 70, 70, 220);

	_mesh_start = get_mesh().add_quad(GLVertex2D(pos, col, vector2df()),
		GLVertex2D(pos + vector2df(size.x, 0.f), col, vector2df(1.f, 0.f)),
		GLVertex2D(pos + size, col, vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y), col, vector2df(0.f, 1.f)));

	col.set(255, 255, 255, 0);
	pos += _text->get_position() + vector2df(_text->get_text_width(), -2.f);
	size.y = _text->get_text_height();
	get_mesh().add_quad(GLVertex2D(pos, col, vector2df()),
		GLVertex2D(pos + vector2df(2.f, 0.f), col, vector2df(1.f, 0.f)),
		GLVertex2D(pos + vector2df(2.f, size.y), col, vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y), col, vector2df(0.f, 1.f)));
}

void TextEdit::focus()
{
	set_priority(true);
	
	get_mesh().set_quad_colour(_mesh_start + 1, GLColour(255, 180));
	_cursor_position = _text->get_text_length();

	update_mesh();

	if (_focus_func)
		_focus_func(this, true);
}

void TextEdit::set_text(const std::string& text)
{
	_text->set_text(text);
	_cursor_position = _text->get_text_length();

	update_mesh();
}

const std::string& TextEdit::get_text() const
{
	return _text->get_text();
}

void TextEdit::set_numbers_only(bool numbers, float lower, float upper, bool allow_decimals)
{
	_numbers_only = numbers;
	_lower_number = lower;
	_upper_number = upper;
	_allow_decimals = allow_decimals;
}

bool TextEdit::get_numbers_only() const
{
	return _numbers_only;
}

bool TextEdit::update(int mouse_x, int mouse_y)
{
	// Needed for text erasing
	if (has_priority())
	{
		if (_erasing)
		{
			auto current_time = std::chrono::system_clock::now();
			auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(current_time.time_since_epoch()) -
				std::chrono::duration_cast<std::chrono::milliseconds>(_erase_start.time_since_epoch());

			if (diff.count() > 40)
			{
				_erase_start = current_time;

				std::string text = _text->get_readable_text();
				text.erase(_deleting ? _cursor_position : --_cursor_position, 1);
				_text->set_text(text);

				if (_deleting && _cursor_position == _text->get_text_length())
				{
					_deleting = false;
					_erasing = false;
				}
				else if (!_deleting && _cursor_position == 0)
					_erasing = false;

				// We need to manually buffer the text here as it will have already been updated - the text will blink otherwise
				update_mesh();
				_text->buffer();

				if (_text_updated_func)
					_text_updated_func(this, _text->get_text());
			}
		}
	}

	_mouse_position.set(static_cast<float>(mouse_x), static_cast<float>(mouse_y));
	return GUIElement::update(mouse_x, mouse_y);
}

void TextEdit::set_position(const vector2df& pos)
{
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + get_size().x, abs_pos.y + get_size().y));

	GUIElement::set_position(pos);
}

void TextEdit::set_size(const vector2df& size)
{
	_text->set_position(vector2df(_text->get_position().x, (size.y - _text->get_font()->get_height()) * 0.5f));

	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));

	GUIElement::set_size(size);
}

void TextEdit::set_focus_function(const std::function<void(GUIElement*, bool)>& func)
{
	_focus_func = func;
}

void TextEdit::set_text_updated_function(const std::function<void(GUIElement*, const std::string&)>& func)
{
	_text_updated_func = func;
}

void TextEdit::buffer()
{
	_text->buffer();
}

bool TextEdit::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if ((action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK))
		{
			if (is_mouse_hovered())
			{
				if (has_priority())
				{
					size_t current_cursor_position = _cursor_position;
					_cursor_position = _text->get_text_position(_mouse_position);

					if (_cursor_position >= _text->get_text_length() && _mouse_position.x < _text->get_absolute_position().x)
						_cursor_position = 0;

					if (current_cursor_position != _cursor_position)
					{
						update_mesh();
					}

					return true;
				}
				else
				{
					focus();

					return true;
				}
			}
			else
			{
				unfocus();

				// We want to return false to allow other elements to process after clicking off of us
				return false;
			}
		}
		else if (action == GLFW_RELEASE && has_priority())
		{
			if (is_mouse_hovered())
			{
				return true;
			}
			else
			{
				unfocus();

				// We want to return false to allow other elements to process after clicking off of us
				return false;
			}
		}
	}

	return false;
}

bool TextEdit::key_event(int key, int scancode, int action, int mods)
{
	if (has_priority())
	{
		if (key == GLFW_KEY_ENTER)
			unfocus();
		else if (key == GLFW_KEY_BACKSPACE || key == GLFW_KEY_DELETE)
		{
			if (action == GLFW_PRESS && (_cursor_position > 0 || key == GLFW_KEY_DELETE))
			{
				_deleting = (key == GLFW_KEY_DELETE);
				
				//float width = _text->get_text_width();
				std::string text = _text->get_readable_text();
				text.erase(_deleting ? _cursor_position : --_cursor_position, 1);
				_text->set_text(text);

				//_cursor_offset = std::max(_cursor_offset + (_text->get_text_width() - width), 0.f);

				update_mesh();

				_erasing = true;
				_erase_start = std::chrono::system_clock::now() + std::chrono::milliseconds(250);

				if (_text_updated_func)
					_text_updated_func(this, _text->get_text());

			}
			else if (action == GLFW_RELEASE)
			{
				_erasing = false;
				_deleting = false;
			}
		}
		else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
		{
			if (_cursor_position > 0)
			{
				--_cursor_position;
				update_mesh();
			}
		}
		else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
		{
			if (_cursor_position < _text->get_text_length())
			{
				++_cursor_position;
				update_mesh();
			}
		}

		return true;
	}

	return false;
}

bool TextEdit::char_event(unsigned int codepoint)
{
	if (has_priority())
	{
		// Type away
		std::wstring temp;
		temp += codepoint;

		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		std::string input = converter.to_bytes(temp);
		bool allowed = true;

		if (_numbers_only)
		{
			for (auto c : input) {
				if (c == '.')
				{
					if (!_allow_decimals || _text->get_text().find('.') != std::string::npos)
						allowed = false;
				}
				else if (c == '-')
				{
					if (_lower_number > -1 || _cursor_position > 0 || _text->get_text().find('-') != std::string::npos)
						allowed = false;
				}
				else if (!isdigit(c))
					allowed = false;
			}
		}

		if (allowed)
		{
			float width = _text->get_text_width();
			_text->append_text(input, static_cast<int>(_cursor_position++), false);

			if (_numbers_only && (_text->get_text().length() > 1 || _text->get_text()[0] != '-'))
			{
				float val = static_cast<float>(atof(_text->get_text().c_str()));
				float prev_val = val;
				val = std::min(std::max(val, _lower_number), _upper_number);

				if (prev_val != val)
				{
					std::string val_string = std::to_string(val);

					val_string.erase(val_string.find_last_not_of('0') + 1, std::string::npos);
					if (!_allow_decimals)
						val_string.erase(val_string.find_last_not_of('.') + 1, std::string::npos);
					else if (val_string[val_string.length() - 1] == '.')
						val_string += "0";

					_text->set_text(val_string);

					_cursor_position = std::min(_cursor_position, _text->get_text().length());
				}
			}

			_cursor_offset = std::max(_cursor_offset + (_text->get_text_width() - width), 0.f);

			update_mesh();

			if (_text_updated_func)
				_text_updated_func(this, _text->get_text());
		}
	}

	return false;
}

bool TextEdit::set_mouse_cursor(size_t& cursor) const
{
	if (is_mouse_hovered())
	{
		cursor = cursor::ibeam;
		return true;
	}

	return false;
}

void TextEdit::update_mesh()
{
	_text->set_size(vector2df(_text->get_text_width() + 2.f, _text->get_size().y));

	float width_to_cursor = _text->get_text_width(0, _cursor_position);
	float width_from_cursor = _text->get_text_width(_cursor_position, _text->get_text_length());
	vector2df text_pos = _text->get_position();

	// Adjust the text position based on where the cursor is if our text is too long to fit in the box
	if (_cursor_position >= _text->get_text_length())
		_cursor_offset = 0.f;
	else if (text_pos.x + width_to_cursor < 4.f)
		_cursor_offset = width_from_cursor - get_size().x + 6.f;
	else if (text_pos.x + width_to_cursor >= get_size().x)
		_cursor_offset = std::max(_cursor_offset - (width_to_cursor - get_size().x + text_pos.x + 6.f), 0.f);

	float text_offset = get_size().x - 8.f - _text->get_size().x + _cursor_offset;
	text_offset = std::min(text_offset, 0.f);
	_text->set_position(vector2df(4.f + text_offset, text_pos.y));

	_text->build();
	
	if (get_mesh().get_index_buffer_size() > 0)
	{
		vector2df pos = get_position();
		float size = _text->get_text_height();

		if (!get_parent() || get_mesh_ptr())
			pos = vector2df();

		pos += _text->get_position() + vector2df(_text->get_text_width(0, _cursor_position), -2.f);
		get_mesh().set_quad_position(_mesh_start + 1, pos,
			pos + vector2df(2.f, 0.f),
			pos + vector2df(2.f, size),
			pos + vector2df(0.f, size));
	}
}

void TextEdit::update_transforms()
{
	vector2df abs_pos(get_absolute_position());
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + get_size().x, abs_pos.y + get_size().y));

	GUIElement::update_transforms();
}

void TextEdit::unfocus()
{
	set_priority(false);
	get_mesh().set_quad_colour(_mesh_start + 1, GLColour(255, 0));
	_erasing = false;
	_deleting = false;
	_cursor_position = _text->get_text_length();
	_cursor_offset = 0.f;

	if (_focus_func)
		_focus_func(this, false);
}