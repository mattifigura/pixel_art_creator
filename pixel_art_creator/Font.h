#ifndef FONT_H
#define FONT_H

#include "Texture.h"

#include <map>
#include <memory>
#include <string>
#include <string_view>

struct FontGlyph {
	GLubyte width;
	vector2ds pos;
	GLubyte advance;
	GLbyte top_offset;
};

typedef std::unordered_map<char, FontGlyph> FontMap;

class Font {
public:
	Font(std::string_view font);
	~Font();

	const FontGlyph get_character_info(char c) const;
	const Texture* get_texture() const;

	int get_height() const;

	int get_string_width(std::string_view in) const;

protected:

private:
	std::unique_ptr<Texture> _texture;
	FontMap _font;
	int _height;
	int _font_size;
	std::string _filename;

	void extract_font_info(std::string_view key);
};

#endif
