#include "Slider.h"

const vector2df grab_size(4.f, 4.f);

Slider::Slider(const vector2df& pos, const vector2df& size, GUIElement* parent, bool force_mesh)
	: GUIElement(pos, size, parent, force_mesh)
	, _value(0.f)
	, _changing(false)
	, _original_value(0.f)
	, _value_update_function(nullptr)
	, _finished_changing_function(nullptr)
{

}

Slider::~Slider()
{

}

void Slider::build()
{
	Mesh& mesh = get_mesh();
	vector2df pos = (!get_parent() || get_mesh_ptr()) ? vector2df() : get_position();
	const vector2df& size = get_size();
	float x_pos = _value * size.x;

	_mesh_start = mesh.add_quad(GLVertex2D(pos + vector2df(0.f, size.y * 0.5f - 2.f), GLColour(120, 120, 120, 180), vector2df()),
		GLVertex2D(pos + vector2df(size.x, size.y * 0.5f - 2.f), GLColour(120, 120, 120, 180), vector2df(1.f, 0.f)),
		GLVertex2D(pos + vector2df(size.x, size.y * 0.5f + 2.f), GLColour(120, 120, 120, 180), vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y * 0.5f + 2.f), GLColour(120, 120, 120, 180), vector2df(0.f, 1.f)));

	mesh.add_quad(GLVertex2D(pos + vector2df(0.f, size.y * 0.5f - 2.f), GLColour(220, 180), vector2df()),
		GLVertex2D(pos + vector2df(x_pos, size.y * 0.5f - 2.f), GLColour(220, 180), vector2df(1.f, 0.f)),
		GLVertex2D(pos + vector2df(x_pos, size.y * 0.5f + 2.f), GLColour(220, 180), vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y * 0.5f + 2.f), GLColour(220, 180), vector2df(0.f, 1.f)));

	mesh.add_quad(GLVertex2D(pos + vector2df(x_pos - grab_size.x, size.y * 0.5f - grab_size.y), GLColour(255, 220), vector2df()),
		GLVertex2D(pos + vector2df(x_pos + grab_size.x, size.y * 0.5f - grab_size.y), GLColour(255, 220), vector2df(1.f, 0.f)),
		GLVertex2D(pos + vector2df(x_pos + grab_size.x, size.y * 0.5f + grab_size.y), GLColour(255, 220), vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(x_pos - grab_size.x, size.y * 0.5f + grab_size.y), GLColour(255, 220), vector2df(0.f, 1.f)));
}

bool Slider::update(int mouse_x, int mouse_y)
{
	bool dirty = false;
	
	if (_changing)
	{
		vector2df abs_pos = get_absolute_position();
		const vector2df& size = get_size();
		float mouse_x_pos = std::min(std::max(static_cast<float>(mouse_x), abs_pos.x), abs_pos.x + size.x) - abs_pos.x;

		float new_value = mouse_x_pos / size.x;

		// Update our mesh
		if (new_value != _value)
		{
			_value = new_value;
			dirty = true;
			update_slider();

			if (_value_update_function)
				_value_update_function(this, _value);
		}
	}

	dirty |= GUIElement::update(mouse_x, mouse_y);
	return dirty;
}

void Slider::set_value(float value)
{
	_value = std::min(std::max(value, 0.f), 1.f);
	update_slider();
}

float Slider::get_value() const
{
	return _value;
}

void Slider::set_value_update_function(const std::function<void(GUIElement*, float)>& func)
{
	_value_update_function = func;
}

void Slider::set_finished_changing_function(const std::function<void(GUIElement*, float, float)>& func)
{
	_finished_changing_function = func;
}

bool Slider::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		bool was_changing = _changing;
		_changing = is_mouse_hovered() && (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK);

		if (_changing)
			_original_value = _value;
		else if (was_changing && _finished_changing_function)
			_finished_changing_function(this, _original_value, _value);

		return _changing;
	}

	return false;
}

void Slider::update_slider()
{
	Mesh& mesh = get_mesh();

	if (mesh.get_quad_count() > 0)
	{
		vector2df pos = (!get_parent() || get_mesh_ptr()) ? vector2df() : get_position();
		const vector2df& size = get_size();
		float x_pos = _value * size.x;

		mesh.set_quad_position(_mesh_start + 1, pos + vector2df(0.f, size.y * 0.5f - 2.f),
			pos + vector2df(x_pos, size.y * 0.5f - 2.f),
			pos + vector2df(x_pos, size.y * 0.5f + 2.f),
			pos + vector2df(0.f, size.y * 0.5f + 2.f));

		mesh.set_quad_position(_mesh_start + 2, pos + vector2df(x_pos - grab_size.x, size.y * 0.5f - grab_size.y),
			pos + vector2df(x_pos + grab_size.x, size.y * 0.5f - grab_size.y),
			pos + vector2df(x_pos + grab_size.x, size.y * 0.5f + grab_size.y),
			pos + vector2df(x_pos - grab_size.x, size.y * 0.5f + grab_size.y));
	}
}