#ifndef COLOUR_PICKER_H
#define COLOUR_PICKER_H

#include "GUIElement.h"

#include <functional>

class Slider;
class Text;
class TextEdit;

class ColourPicker : public GUIElement {
public:
	ColourPicker(const vector2df& pos, GUIElement* parent);
	~ColourPicker();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	virtual bool mouse_event(int button, int action, int mods);

	const GLColour& get_chosen_colour() const;
	void set_colour(const GLColour& col);

	void set_colour_picked_function_callback(const std::function<void(const GLColour&)>& func);

protected:

private:
	std::unique_ptr<Texture> _colours;
	GLColour _chosen_colour;

	vector2di _mouse_position;
	vector2di _pixel_position;
	vector2di _colour_position;
	bool _just_clicked;
	bool _picking_colour;
	bool _picking_hue;

	GLColour get_hue_rgb(float hue) const;

	Text* _values_label;
	TextEdit* _red_edit;
	TextEdit* _green_edit;
	TextEdit* _blue_edit;
	TextEdit* _alpha_edit;
	Slider* _red_slider;
	Slider* _green_slider;
	Slider* _blue_slider;
	Slider* _alpha_slider;

	std::function<void(const GLColour&)> _colour_picked_callback;
};

#endif