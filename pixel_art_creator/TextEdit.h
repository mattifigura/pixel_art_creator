#ifndef TEXT_EDIT_H
#define TEXT_EDIT_H

#include "Text.h"

#include <chrono>
#include <functional>

class TextEdit : public GUIElement {
public:
	TextEdit(const vector2df& pos, const vector2df& size, const Font* font, GUIElement* parent = nullptr, bool force_mesh = false);
	~TextEdit();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	virtual void set_position(const vector2df& pos);
	virtual void set_size(const vector2df& size);

	void focus();

	void set_text(const std::string& text);
	const std::string& get_text() const;

	void set_numbers_only(bool numbers, float lower, float upper, bool allow_decimals = true);
	bool get_numbers_only() const;

	void set_focus_function(const std::function<void(GUIElement*, bool)>& func);
	void set_text_updated_function(const std::function<void(GUIElement*, const std::string&)>& func);

	void buffer();

	virtual bool mouse_event(int button, int action, int mods);
	virtual bool key_event(int key, int scancode, int action, int mods);
	virtual bool char_event(unsigned int codepoint);
	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:
	virtual void update_mesh();
	virtual void update_transforms();

	void unfocus();

private:
	Text* _text;

	bool _numbers_only;
	float _lower_number;
	float _upper_number;
	bool _allow_decimals;

	bool _erasing;
	bool _deleting;
	std::chrono::time_point<std::chrono::system_clock> _erase_start;
	vector2df _mouse_position;
	size_t _cursor_position;
	float _cursor_offset;

	std::function<void(GUIElement*, bool)> _focus_func;
	std::function<void(GUIElement*, const std::string&)> _text_updated_func;
};

#endif