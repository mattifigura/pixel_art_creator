#ifndef TEXTURE_H
#define TEXTURE_H

#include "GLBase.h"

#include <vector>

class Texture {
public:
	Texture();
	~Texture();

	void create_colour_image(int width, int height, const GLColour& col, bool buffer_data = true);
	void create_gradient_image(int width, int height, const GLColour& start, const GLColour& end, bool horizontal = false, bool buffer_data = true);
	void create_gradient_image(int width, int height, const GLColour& top_left, const GLColour& top_right, const GLColour& bottom_left, const GLColour& bottom_right, bool buffer_data = true);
	void create_texture_from_buffer(const GLubyte* data_buffer, int width, int height, GLenum type, bool buffer_data = true);
	void create_texture_from_buffer(const std::vector<GLColour>& data_buffer, int width, int height, bool buffer_data = true);

	void resize(int width, int height);

	void set_pixel(int x, int y, const GLColour& col);
	const GLColour& get_pixel(int x, int y) const;
	const std::vector<GLColour>& get_data() const;

	bool buffer();

	GLuint get_index() const;
	const vector2di& get_size() const;

protected:

private:
	std::vector<GLColour> _data;
	vector2di _size;
	GLuint _index;
};

#endif