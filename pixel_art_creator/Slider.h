#ifndef SLIDER_H
#define SLIDER_H

#include "GUIElement.h"

#include <functional>

class Slider : public GUIElement {
public:
	Slider(const vector2df& pos, const vector2df& size, GUIElement* parent = nullptr, bool force_mesh = false);
	~Slider();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	void set_value(float value);
	float get_value() const;

	void set_value_update_function(const std::function<void(GUIElement*, float)>& func);
	void set_finished_changing_function(const std::function<void(GUIElement*, float, float)>& func);

	virtual bool mouse_event(int button, int action, int mods);

protected:

private:
	float _value;
	bool _changing;
	float _original_value;
	std::function<void(GUIElement*, float)> _value_update_function;
	std::function<void(GUIElement*, float, float)> _finished_changing_function;

	void update_slider();
};

#endif