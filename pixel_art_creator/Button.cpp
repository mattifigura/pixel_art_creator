#include "Button.h"

const vector2df border_size(2.f, 2.f);
const GLColour normal_col(0, 0, 0, 150);
const GLColour hovered_col(150, 150, 150, 200);
const GLColour pressed_col(30, 30, 30, 200);
const GLColour hovered_pressed_col(100, 100, 100, 200);
const vector4df normal_uv(0.f, 0.f, 0.5f, 0.5f);
const vector4df hovered_uv(0.5f, 0.f, 1.f, 0.5f);
const vector4df pressed_uv(0.f, 0.5f, 0.5f, 1.f);
const vector4df hovered_pressed_uv(0.5f, 0.5f, 1.f, 1.f);

Button::Button(const vector2df& pos, const vector2df& size, const std::string& text, const Font* font, GUIElement* parent, bool force_mesh)
	: GUIElement(pos, size, parent, force_mesh)
	, _function(nullptr)
	, _is_pressing(false)
	, _toggle_mode(false)
	, _image_mode(false)
	, _pressed(false)
	, _text_offset(12.f)
{
	// Add our label
	if (font)
		_text_offset = (size.y - font->get_height()) * 0.5f;

	Text* label = new Text(vector2df(border_size.x, _text_offset), size - border_size * 2.f, text, font, this);
	label->set_alignment(TextAlignment::centre, false);
	label->set_shadowed(true, false);
}

Button::~Button()
{
	
}

void Button::build()
{
	Mesh& mesh = get_mesh();
	vector2df pos = get_position();
	vector2df size = get_size();

	if (!get_parent() || get_mesh_ptr())
		pos = vector2df();

	if (!_image_mode)
	{
		_mesh_start = mesh.add_quad(GLVertex2D(pos, GLColour(), vector2df()),
			GLVertex2D(pos + vector2df(size.x, 0.f), GLColour(), vector2df(1.f, 0.f)),
			GLVertex2D(pos + size, GLColour(), vector2df(1.f, 1.f)),
			GLVertex2D(pos + vector2df(0.f, size.y), GLColour(), vector2df(0.f, 1.f)));

		mesh.add_quad(GLVertex2D(pos + border_size, normal_col, vector2df()),
			GLVertex2D(pos + vector2df(size.x - border_size.x, border_size.y), normal_col, vector2df(1.f, 0.f)),
			GLVertex2D(pos + size - border_size, normal_col, vector2df(1.f, 1.f)),
			GLVertex2D(pos + vector2df(border_size.x, size.y - border_size.y), normal_col, vector2df(0.f, 1.f)));
	}
	else
	{
		float press = _pressed ? 0.5f : 0.f;
		_mesh_start = mesh.add_quad(GLVertex2D(pos, GLColour(), vector2df(0.f, press)),
			GLVertex2D(pos + vector2df(size.x, 0.f), GLColour(), vector2df(0.5f, press)),
			GLVertex2D(pos + size, GLColour(), vector2df(0.5f, press + 0.5f)),
			GLVertex2D(pos + vector2df(0.f, size.y), GLColour(), vector2df(0.f, press + 0.5f)));
	}
}

void Button::set_size(const vector2df& size)
{
	// Update our text position
	const Font* font = dynamic_cast<Text*>(get_children()[0])->get_font();
	if (font)
	{
		_text_offset = (size.y - font->get_height()) * 0.5f;
		get_children()[0]->set_position(vector2df(border_size.x, _text_offset));
	}

	GUIElement::set_size(size);
}

void Button::set_text(const std::string& text)
{
	dynamic_cast<Text*>(get_children()[0])->set_text(text);
}

void Button::set_toggle_mode(bool toggle)
{
	_toggle_mode = toggle;
}

void Button::set_image_mode(bool image, const Texture* texture)
{
	_image_mode = image;

	if (image)
	{
		force_mesh();
		set_texture(texture);
		dynamic_cast<Text*>(get_children()[0])->set_text("");
	}
}

void Button::press()
{
	bool press = true;
	if (_toggle_mode)
	{
		_pressed = !_pressed;
		press = _pressed;
	}
	
	if (_function)
		_function(this, press);
}

void Button::set_pressed(bool pressed)
{
	_pressed = pressed;

	if (get_mesh().get_quad_count() > 0)
		mouse_hover(false);
	get_children()[0]->set_position(vector2df(border_size.x, _text_offset + (_pressed ? 1.f : 0.f)));
}

bool Button::is_pressed() const
{
	return _pressed;
}

void Button::set_function(const std::function<void(GUIElement*, bool)>& func)
{
	_function = func;
}

bool Button::mouse_hover(bool entering)
{
	// If the mouse is entering the button, light it up
	// If it is leaving, dim it (either put it back to original colour or set it to a darker blue if the user has pressed it in)
	if (!_image_mode)
	{
		GLColour col(normal_col);
		if (entering)
		{
			col = hovered_col;

			if (_is_pressing || _pressed)
				col = hovered_pressed_col;
		}
		else if (_is_pressing || _pressed)
			col = pressed_col;

		get_mesh().set_quad_colour(_mesh_start + 1, col);
	}
	else
	{
		vector4df uv(normal_uv);
		if (entering)
		{
			uv = hovered_uv;

			if (_is_pressing || _pressed)
				uv = hovered_pressed_uv;
		}
		else if (_is_pressing || _pressed)
			uv = pressed_uv;

		get_mesh().set_quad_uv(_mesh_start, uv);
	}

	return true;
}

bool Button::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_1)
	{
		// The user is pressing in the button, update the colour to show this
		if (is_mouse_hovered() && (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK))
		{
			if (!_image_mode)
				get_mesh().set_quad_colour(_mesh_start + 1, hovered_pressed_col);
			else
				get_mesh().set_quad_uv(_mesh_start, hovered_pressed_uv);
			_is_pressing = true;

			get_children()[0]->set_position(vector2df(border_size.x, _text_offset + 1.f));
			return true;
		}

		if (action == GLFW_RELEASE)
		{
			bool was_pressed = _is_pressing;
			_is_pressing = false;

			if (was_pressed)
			{
				// If the user is hovering the button, had starting pressing it in and releases the mouse, execute our function
				if (is_mouse_hovered())
				{
					bool press = true;
					if (_toggle_mode)
					{
						_pressed = !_pressed;
						press = _pressed;
					}
					
					get_children()[0]->set_position(vector2df(border_size.x, _text_offset + (_pressed ? 1.f : 0.f)));

					if (!_image_mode)
						get_mesh().set_quad_colour(_mesh_start + 1, _pressed ? hovered_pressed_col : hovered_col);
					else
						get_mesh().set_quad_uv(_mesh_start, _pressed ? hovered_pressed_uv : hovered_uv);

					if(_function)
						_function(this, press);

					return true;
				}

				// If the user pressed the button but releases their mouse outside of the button, set the colour back but don't execute our function
				get_children()[0]->set_position(vector2df(border_size.x, _text_offset + (_pressed ? 1.f : 0.f)));

				if (!_image_mode)
					get_mesh().set_quad_colour(_mesh_start + 1, _pressed ? pressed_col : normal_col);
				else
					get_mesh().set_quad_uv(_mesh_start, _pressed ? pressed_uv : normal_uv);
			}
		}
	}

	return false;
}

void Button::update_mesh()
{
	Mesh& mesh = get_mesh();
	if (mesh.get_index_buffer_size() > 0)
	{
		vector2df pos = get_position();
		vector2df size = get_size();

		if (!get_parent())
			pos = vector2df();

		mesh.set_quad_position(_mesh_start, pos, pos + vector2df(size.x, 0.f), pos + size, pos + vector2df(0.f, size.y));

		if (!_image_mode)
			mesh.set_quad_position(_mesh_start + 1, pos + border_size, pos + vector2df(size.x - border_size.x, border_size.y), pos + size - border_size, pos + vector2df(border_size.x, size.y - border_size.y));
	}
}