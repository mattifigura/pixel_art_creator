#include "Text.h"
#include "Util.h"

#include <sstream>

Text::Text(const vector2df& pos, const vector2df& size, const std::string& text, const Font* font, GUIElement* parent)
	: GUIElement(pos, size, parent, true)
	, _text(text)
	, _font(font)
	, _alignment(TextAlignment::left)
	, _changed(true)
	, _split_by_word(true)
	, _shadows(false)
	, _expand_past_width(false)
	, _line_spacing(1.25f)
	, _line_count(1)
	, _shadow_offset(0.f, 2.f)
	, _shadow_alpha(150)
{
	if (_font)
		set_texture(_font->get_texture());
}

Text::~Text()
{

}

void Text::build()
{
	// Only build us if something has changed to avoid unwanted rebuilds
	if (!_changed)
		return;
	_changed = false;
	
	// Clear our data
	get_mesh().clear_data();
	
	// Only do this if we have a font and some text
	if (_font && !_text.empty())
	{
		// Variables for building the buffers
		char c = _text[0];
		int n = 0, t = 0;
		float y = 0;
		GLColour col, next_col;
		int width = static_cast<int>(get_size().x);
		std::stringstream word;
		float height = static_cast<float>(_font->get_height());
		_line_count = 1;
		std::vector<int> line_lengths;
		get_line_lengths(line_lengths);

		// Function to add a quad
		auto add_quad = [&](const FontGlyph& glyph) {
			vector2di size(get_texture()->get_size());

			vector2df p1(static_cast<float>(n), static_cast<float>(floor(y * height) + glyph.top_offset));
			vector2df p2(static_cast<float>(n + glyph.advance), static_cast<float>(floor((y + 1.f) * height) + glyph.top_offset));
			vector2df uv1(static_cast<float>(glyph.pos.x) / static_cast<float>(size.x), static_cast<float>(glyph.pos.y) / static_cast<float>(size.y));
			vector2df uv2(static_cast<float>(glyph.pos.x + glyph.advance) / static_cast<float>(size.x), static_cast<float>(glyph.pos.y + height) / static_cast<float>(size.y));

			// Add a shadow if we need to
			if (_shadows)
				get_mesh().add_quad(GLVertex2D(vector2df(p1.x, p1.y) + _shadow_offset, GLColour(0, _shadow_alpha), vector2df(uv1.x, uv1.y)),
					GLVertex2D(vector2df(p2.x, p1.y) + _shadow_offset, GLColour(0, _shadow_alpha), vector2df(uv2.x, uv1.y)),
					GLVertex2D(vector2df(p2.x, p2.y) + _shadow_offset, GLColour(0, _shadow_alpha), vector2df(uv2.x, uv2.y)),
					GLVertex2D(vector2df(p1.x, p2.y) + _shadow_offset, GLColour(0, _shadow_alpha), vector2df(uv1.x, uv2.y)));

			get_mesh().add_quad(GLVertex2D(vector2df(p1.x, p1.y), col, vector2df(uv1.x, uv1.y)),
				GLVertex2D(vector2df(p2.x, p1.y), col, vector2df(uv2.x, uv1.y)),
				GLVertex2D(vector2df(p2.x, p2.y), col, vector2df(uv2.x, uv2.y)),
				GLVertex2D(vector2df(p1.x, p2.y), col, vector2df(uv1.x, uv2.y)));
		};

		// Function to set alignment
		auto set_alignment = [&]() {
			size_t _where = std::min(line_lengths.size() - 1, static_cast<size_t>(_line_count) - 1);

			if (_alignment == TextAlignment::centre)
			{
				n = (width / 2) - (line_lengths[_where] / 2);
			}
			else if (_alignment == TextAlignment::right)
			{
				n = width - line_lengths[_where];
			}
		};
		set_alignment();

		// Is this the end of the current word?
		bool end_of_word(false);

		for (size_t i = 0; c != '\0' && i < _text.length(); c = _text[++i]) {
			// Get the character details
			const FontGlyph& g = _font->get_character_info(c);

			// Sort out the colour
			bool colour_change(false);
			if (c == '^' && _text[i + 1] != '\0')
			{
				colour_change = get_colour(next_col, _text[i + 1]);
				if (colour_change)
				{
					++i;

					// If we are splitting by words, end the word here
					if (_split_by_word)
						end_of_word = true;
					else
						continue;
				}
			}

			// If there's no colour change set our colour
			if (!colour_change)
				col = next_col;

			// Positional corrections inside text
			if ((!_split_by_word || !end_of_word) && c == '%' && _text[i + 1] != '\0')
			{
				char a = _text[i + 1];
				if (isdigit(a))
				{
					std::stringstream s;
					size_t j = i + 1;

					while (_text[j] != '\0')
					{
						if (_text[j] == '\\')
						{
							++j;
							break;
						}

						if (!isdigit(_text[j]))
							break;

						s << _text[j];
						++j;
					}

					n = stoi(s.str());
					i += (j - i - 1);
					continue;
				}
			}

			// Add the character to our current word
			if (!colour_change && c != '\n')
				word << c;

			// If we are splitting by word then add the whole word now
			end_of_word |= (c == ' ' || _text[i + 1] == '\0');
			if (end_of_word && _split_by_word)
			{
				for (GLuint j = 0; j < word.str().length(); ++j) {
					// Get the glyph for the current character
					const FontGlyph& _g = _font->get_character_info(word.str()[j]);

					// Add a quad
					add_quad(_g);

					// Move on
					n += _g.advance;
				}

				t = n;
				word.str(std::string());
				word.clear();
				end_of_word = false;
			}

			// Newlines
			if (c == '\n' || (t + g.advance > width && !_expand_past_width))
			{
				// Move onto the next line
				y += _line_spacing;
				++_line_count;
				n = 0;
				t = 0;

				// Set alignment for the new line
				set_alignment();

				if (c == '\n' || (c == ' ' && !_split_by_word))
				{
					if (i + 1 >= _text.length())
						break;

					continue;
				}
			}

			// Add the quad
			if (!_split_by_word)
			{
				// Add a quad
				add_quad(g);

				// go to the next character
				n += g.advance;
			}

			t += g.advance;
		}
	}
}

void Text::set_font(const Font* font, bool rebuild)
{
	_font = font;
	_changed = true;

	if (_font)
		set_texture(_font->get_texture());

	if (rebuild)
		build();
}

void Text::set_text(const std::string& text, bool rebuild)
{
	if (text != _text)
	{
		_text = text;
		_changed = true;

		if (rebuild)
			build();
	}
}

void Text::append_text(const std::string& text, int pos, bool rebuild)
{
	if (pos < 0)
		_text.append(text);
	else
		_text.insert(static_cast<size_t>(pos), text);

	_changed = true;

	if (rebuild)
		build();
}

const std::string& Text::get_text() const
{
	return _text;
}

const std::string Text::get_readable_text() const
{
	// Get a readable version of the text
	size_t length = get_text_length();
	char c = _text[0];
	GLColour col;
	std::stringstream str;

	for (size_t i = 0; i < length; c = _text[++i]) {
		// Sort out the colour
		if (c == '^' && _text[i + 1] != '\0')
		{
			if (get_colour(col, _text[i + 1]))
			{
				++i;
				continue;
			}
		}

		// Positional corrections inside text
		if (c == '%' && _text[i + 1] != '\0')
		{
			size_t j = i + 1;
			while (_text[j] != '\0')
			{
				if (_text[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(_text[j]))
					break;

				++j;
			}

			i += (j - i - 1);
			continue;
		}

		// Newlines
		if (c == '\n')
		{
			continue;
		}

		// Append the character onto our string
		str << c;
	}

	// Return the string
	return str.str();
}

const Font* Text::get_font() const
{
	return _font;
}

size_t Text::get_text_position(const vector2df& pos) const
{
	const Mesh* mesh = get_mesh_ptr();
	vector2df abs_pos = get_absolute_position();
	size_t letter_count = mesh->get_quad_count();
	vector2df pos1, pos2, pos3, pos4;

	for (size_t i = 0; i < letter_count; i += (_shadows ? 2 : 1)) {
		mesh->get_quad_position(i + (_shadows ? 1 : 0), pos1, pos2, pos3, pos4);
		float half = pos1.x + ((pos2.x - pos1.x) * 0.5f);

		if (util::is_point_inside(pos, abs_pos + vector2df(pos1.x - 4.f, pos1.y - 8.f), abs_pos + vector2df(half, pos4.y + 2.f)))
			return _shadows ? i / 2 : i;
		else if (util::is_point_inside(pos, abs_pos + vector2df(half, pos1.y - 8.f), abs_pos + vector2df(pos3.x + 4.f, pos4.y + 2.f)))
			return _shadows ? (i / 2) + 1 : i + 1;
	}

	return _shadows ? letter_count / 2 : letter_count;
}

size_t Text::get_text_length() const
{
	return _text.length();
}

float Text::get_text_width() const
{
	if (!_font)
		return 0.f;

	return static_cast<float>(_font->get_string_width(get_readable_text()));
}

float Text::get_text_width(size_t offset, size_t count) const
{
	if (!_font)
		return 0.f;

	return static_cast<float>(_font->get_string_width(get_readable_text().substr(offset, count)));
}

float Text::get_text_height() const
{
	if (!_font)
		return 0.f;

	std::vector<int> temp;
	get_line_lengths(temp);

	return static_cast<float>(temp.size() * _font->get_height() * 1.25f);
}

void Text::set_line_spacing(float spacing, bool rebuild)
{
	if (spacing != _line_spacing)
	{
		_line_spacing = spacing;
		_changed = true;

		if (rebuild)
			build();
	}
}

float Text::get_line_spacing() const
{
	return _line_spacing;
}

void Text::set_alignment(TextAlignment align, bool rebuild)
{
	if (align != _alignment)
	{
		_alignment = align;
		_changed = true;

		if (rebuild)
			build();
	}
}

TextAlignment Text::get_alignment() const
{
	return _alignment;
}

void Text::set_split_by_word(bool split, bool rebuild)
{
	if (split != _split_by_word)
	{
		_split_by_word = split;
		_changed = true;

		if (rebuild)
			build();
	}
}

bool Text::get_split_by_word() const
{
	return _split_by_word;
}

void Text::set_shadowed(bool shadow, bool rebuild)
{
	if (shadow != _shadows)
	{
		_shadows = shadow;
		_changed = true;

		if (rebuild)
			build();
	}
}

bool Text::get_shadowed() const
{
	return _shadows;
}

void Text::set_expand_past_width(bool expand, bool rebuild)
{
	if (expand != _expand_past_width)
	{
		_expand_past_width = expand;
		_changed = true;

		if (rebuild)
			build();
	}
}

bool Text::get_expand_past_width() const
{
	return _expand_past_width;
}

void Text::set_size(const vector2df& size)
{
	if (size != get_size())
	{
		GUIElement::set_size(size);
		_changed = true;
	}
}

void Text::buffer()
{
	get_mesh().buffer();
}

bool Text::set_mouse_cursor(size_t& cursor) const
{
	return false;
}

bool Text::get_colour(GLColour& col, char c) const
{
	// Check for a valid colour flag
	switch (c)
	{
	case '0':
		col.r = 0; col.g = 0; col.b = 0;
		break;
	case '1':
		col.r = 255; col.g = 0; col.b = 0;
		break;
	case '2':
		col.r = 0; col.g = 255; col.b = 0;
		break;
	case '3':
		col.r = 0; col.g = 0; col.b = 255;
		break;
	case '4':
		col.r = 255; col.g = 0; col.b = 255;
		break;
	case '5':
		col.r = 0; col.g = 255; col.b = 255;
		break;
	case '6':
		col.r = 255; col.g = 255; col.b = 0;
		break;
	case '7':
		col.r = 255; col.g = 255; col.b = 255;
		break;
	case '8':
		col.r = 125; col.g = 125; col.b = 125;
		break;
	case '9':
		col.r = 75; col.g = 75; col.b = 75;
		break;
	default:
		return false;
		break;
	}

	// Return true if we have a new colour
	return true;
}

void Text::get_line_lengths(std::vector<int>& out) const
{
	// Get the length of each line in this text object
	// Used to centre text that has multiple lines
	size_t length = get_text_length();
	char c = _text[0];
	GLColour col;
	int width = static_cast<int>(get_size().x);
	int line = 0;

	for (size_t i = 0; i < length; c = _text[++i]) {
		// Sort out the colour
		if (c == '^' && _text[i + 1] != '\0')
		{
			if (get_colour(col, _text[i + 1]))
			{
				++i;
				continue;
			}
		}

		// Positional corrections inside text
		if (c == '%' && _text[i + 1] != '\0')
		{
			size_t j = i + 1;
			while (_text[j] != '\0')
			{
				if (_text[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(_text[j]))
					break;

				++j;
			}

			i += (j - i - 1);
			continue;
		}

		// Newlines
		if (c == '\n')
		{
			out.push_back(line);
			line = 0;

			continue;
		}

		// Get the width of this character
		int advance = _font->get_character_info(c).advance;
		if (line + advance > width && !_expand_past_width)
		{
			out.push_back(line);
			line = 0;

			continue;
		}
		line += advance;
	}

	// Add the final line length
	if (_text.empty() || _text[_text.length() - 1] != '\n')
		out.push_back(line);
}