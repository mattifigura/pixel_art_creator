#ifndef BUTTON_H
#define BUTTON_H

#include "Text.h"

#include <functional>

class Button : public GUIElement {
public:
	Button(const vector2df& pos, const vector2df& size, const std::string& text, const Font* font, GUIElement* parent = nullptr, bool force_mesh = false);
	~Button();

	virtual void build();

	virtual void set_size(const vector2df& size);

	void set_text(const std::string& text);

	void set_toggle_mode(bool toggle);

	// Button images should be a square texture split into quarters:
	// Normal | Hovered
	// =========================
	// Pressed | Pressed Hovered
	void set_image_mode(bool image, const Texture* texture);

	void press();
	void set_pressed(bool pressed);
	bool is_pressed() const;

	void set_function(const std::function<void(GUIElement*, bool)>& func);

	virtual bool mouse_hover(bool entering);
	virtual bool mouse_event(int button, int action, int mods);

protected:
	virtual void update_mesh();

private:
	std::function<void(GUIElement*, bool)> _function;
	bool _is_pressing;
	bool _toggle_mode;
	bool _image_mode;
	bool _pressed;
	float _text_offset;
};

#endif