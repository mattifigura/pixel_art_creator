#ifndef SCROLL_VIEW_H
#define SCROLL_VIEW_H

#include "GUIElement.h"

#include <functional>
#include <map>

class ScrollView : public GUIElement {
public:
	ScrollView(const vector2df& pos, const vector2df& size, GUIElement* parent = nullptr);
	~ScrollView();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	virtual void set_position(const vector2df& pos);

	void add_element(GUIElement* element);
	void remove_element(GUIElement* element);

	void scroll_to_top();

	void set_scroll_amount(float amount);

	void set_vertical_scrollbar_toggle_function(const std::function<void(bool)>& func);

	virtual bool mouse_event(int button, int action, int mods);
	virtual bool scroll_event(double x, double y);
	virtual void screen_resized(int width, int height);
	virtual bool set_mouse_cursor(size_t& cursor) const;

	void recalculate_size();

protected:

private:
	std::unordered_map<GUIElement*, vector2df> _places;
	vector2df _content_size;
	vector2df _mouse_position;
	vector2df _scroll_start_position;
	float _scroll_amount;

	float _vertical_position;
	float _vertical_scrollbar_size;
	bool _scrolling_vertical;
	bool _vertical_hovered;
	std::function<void(bool)> _vertical_scrollbar_toggled_function;

	float _horizontal_position;
	float _horizontal_scrollbar_size;
	bool _scrolling_horizontal;
	bool _horizontal_hovered;

	void update_positions();
};

#endif