#include "EditStateManager.h"

EditStateManager::EditStateManager()
	: _undo_stack()
	, _redo_stack()
{

}

void EditStateManager::undo()
{
	// Pop off the undo deque, execute it and move it to the redo deque
	if (!_undo_stack.empty())
	{
		std::shared_ptr<EditState> state = _undo_stack.back();
		_undo_stack.pop_back();

		state->undo();
		_redo_stack.push_back(state);
	}
}

void EditStateManager::redo()
{
	// Pop off the redo deque, execute it and move it to the undo deque
	if (!_redo_stack.empty())
	{
		std::shared_ptr<EditState> state = _redo_stack.back();
		_redo_stack.pop_back();

		state->redo();
		_undo_stack.push_back(state);
	}
}

void EditStateManager::add_state(EditState* state)
{
	// Check the state
	if (!state->validate_state())
	{
		delete state;
		return;
	}

	// Check for duplicate
	if (_undo_stack.size() > 0)
	{
		if (state->compare_to(_undo_stack[_undo_stack.size() - 1].get()))
		{
			delete state;
			return;
		}
	}

	// Check for compression opportunity
	if (_undo_stack.size() > 1)
	{
		if (state->compare_to(_undo_stack[_undo_stack.size() - 2].get()))
		{
			delete state;
			return;
		}
	}
	
	// Add a new state and clear the redo deque as it is now invalidated
	_undo_stack.push_back(std::shared_ptr<EditState>(state));
	_redo_stack.clear();

	// Limit the amount of undos/redos to a given number
	if (_undo_stack.size() > 60)
		_undo_stack.pop_front();
}

void EditStateManager::clear_states()
{
	_undo_stack.clear();
	_redo_stack.clear();
}