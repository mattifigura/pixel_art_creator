#include "ColourRect.h"
#include "Texture.h"

std::unique_ptr<Texture> ColourRect::_texture(nullptr);

ColourRect::ColourRect(const vector2df& pos, const vector2df& size, const GLColour& col, GUIElement* parent, bool force_mesh)
	: GUIElement(pos, size, parent, force_mesh)
	, _colour(col)
{
	// Ensure that we have a white texture
	if (!_texture)
	{
		_texture.reset(new Texture());
		_texture->create_colour_image(16, 16, GLColour());
	}

	set_texture(_texture.get());
}

ColourRect::~ColourRect()
{

}

void ColourRect::build()
{
	vector2df pos = (!get_parent() || get_mesh_ptr()) ? vector2df() : get_position();
	vector2df size = get_size();

	_mesh_start = get_mesh().add_quad(GLVertex2D(pos, _colour, vector2df()),
		GLVertex2D(pos + vector2df(size.x, 0.f), _colour, vector2df(1.f, 0.f)),
		GLVertex2D(pos + size, _colour, vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y), _colour, vector2df(0.f, 1.f)));
}

void ColourRect::set_colour(const GLColour& col)
{
	_colour = col;
	get_mesh().set_quad_colour(_mesh_start, _colour);
}

const GLColour& ColourRect::get_colour() const
{
	return _colour;
}

bool ColourRect::set_mouse_cursor(size_t& cursor) const
{
	return false;
}

void ColourRect::update_mesh()
{
	if (get_mesh().get_quad_count() > 0)
	{
		vector2df pos = (!get_parent() || get_mesh_ptr()) ? vector2df() : get_position();
		vector2df size = get_size();

		get_mesh().set_quad_position(_mesh_start, pos, pos + vector2df(size.x, 0.f), pos + size, pos + vector2df(0.f, size.y));
	}
}