#ifndef MATRIX_H_
#define MATRIX_H_

// custom implementation of matrices done by Matti Figura :)
#include "Vector.h"

#include <iostream>

// 4x4 matrix class
template<typename T>
class Matrix4 {
public:
	Matrix4() { identity(); }
	Matrix4(const Matrix4<T>& other) { set(other); }
	Matrix4(T vals[]) { set(vals); }
	Matrix4(T val) { set(val); }
	//Matrix4(T left, T right, T bottom, T top, T near, T far) { Orthographic(left, right, bottom, top, near, far); }

	void reset()
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] = 0;
		}
	}
	void identity()
	{
		reset();
		matrix[0] = 1;
		matrix[5] = 1;
		matrix[10] = 1;
		matrix[15] = 1;
	}
	void orthographic(T left, T right, T bottom, T top, T near, T far)
	{
		identity();

		T tx = -(right + left) / (right - left);
		T ty = -(top + bottom) / (top - bottom);
		T tz = -(far + near) / (far - near);

		matrix[0] = 2.f / (right - left);
		matrix[5] = 2.f / (top - bottom);
		matrix[10] = -2.f / (far - near);
		matrix[3] = tx;
		matrix[7] = ty;
		matrix[11] = tz;
	}
	void perspective(T angle, T ratio, T near, T far)
	{
		reset();

		T tan_half = (T)tan((angle * DEG_TO_RAD) / 2);

		matrix[0] = 1 / (ratio * tan_half);
		matrix[5] = 1 / tan_half;
		matrix[10] = -(far + near) / (far - near);
		matrix[11] = -(2 * far * near) / (far - near);
		matrix[14] = -1;
	}
	void flip()
	{
		T temp;
		for (int i = 0; i < SIZE / 2; ++i) {
			temp = matrix[i];
			matrix[i] = matrix[SIZE - 1 - i];
			matrix[SIZE - 1 - i] = temp;
		}
	}
	void print() const
	{
		for (int i = 0; i < SIZE; ++i) {
			std::cout << matrix[i] << " ";
			if ((i + 1) % 4 == 0)
				std::cout << std::endl;
		}
	}
	void transpose()
	{
		T temp[SIZE];

		for (int i = 0; i < 4; ++i) {
			temp[i] = matrix[i * 4];
			temp[i + 4] = matrix[i * 4 + 1];
			temp[i + 8] = matrix[i * 4 + 2];
			temp[i + 12] = matrix[i * 4 + 3];
		}

		set(temp);
	}
	bool inverse()
	{
		double inv[SIZE], det;
		int i;

		inv[0] = matrix[5] * matrix[10] * matrix[15] -
			matrix[5] * matrix[11] * matrix[14] -
			matrix[9] * matrix[6] * matrix[15] +
			matrix[9] * matrix[7] * matrix[14] +
			matrix[13] * matrix[6] * matrix[11] -
			matrix[13] * matrix[7] * matrix[10];

		inv[4] = -matrix[4] * matrix[10] * matrix[15] +
			matrix[4] * matrix[11] * matrix[14] +
			matrix[8] * matrix[6] * matrix[15] -
			matrix[8] * matrix[7] * matrix[14] -
			matrix[12] * matrix[6] * matrix[11] +
			matrix[12] * matrix[7] * matrix[10];

		inv[8] = matrix[4] * matrix[9] * matrix[15] -
			matrix[4] * matrix[11] * matrix[13] -
			matrix[8] * matrix[5] * matrix[15] +
			matrix[8] * matrix[7] * matrix[13] +
			matrix[12] * matrix[5] * matrix[11] -
			matrix[12] * matrix[7] * matrix[9];

		inv[12] = -matrix[4] * matrix[9] * matrix[14] +
			matrix[4] * matrix[10] * matrix[13] +
			matrix[8] * matrix[5] * matrix[14] -
			matrix[8] * matrix[6] * matrix[13] -
			matrix[12] * matrix[5] * matrix[10] +
			matrix[12] * matrix[6] * matrix[9];

		inv[1] = -matrix[1] * matrix[10] * matrix[15] +
			matrix[1] * matrix[11] * matrix[14] +
			matrix[9] * matrix[2] * matrix[15] -
			matrix[9] * matrix[3] * matrix[14] -
			matrix[13] * matrix[2] * matrix[11] +
			matrix[13] * matrix[3] * matrix[10];

		inv[5] = matrix[0] * matrix[10] * matrix[15] -
			matrix[0] * matrix[11] * matrix[14] -
			matrix[8] * matrix[2] * matrix[15] +
			matrix[8] * matrix[3] * matrix[14] +
			matrix[12] * matrix[2] * matrix[11] -
			matrix[12] * matrix[3] * matrix[10];

		inv[9] = -matrix[0] * matrix[9] * matrix[15] +
			matrix[0] * matrix[11] * matrix[13] +
			matrix[8] * matrix[1] * matrix[15] -
			matrix[8] * matrix[3] * matrix[13] -
			matrix[12] * matrix[1] * matrix[11] +
			matrix[12] * matrix[3] * matrix[9];

		inv[13] = matrix[0] * matrix[9] * matrix[14] -
			matrix[0] * matrix[10] * matrix[13] -
			matrix[8] * matrix[1] * matrix[14] +
			matrix[8] * matrix[2] * matrix[13] +
			matrix[12] * matrix[1] * matrix[10] -
			matrix[12] * matrix[2] * matrix[9];

		inv[2] = matrix[1] * matrix[6] * matrix[15] -
			matrix[1] * matrix[7] * matrix[14] -
			matrix[5] * matrix[2] * matrix[15] +
			matrix[5] * matrix[3] * matrix[14] +
			matrix[13] * matrix[2] * matrix[7] -
			matrix[13] * matrix[3] * matrix[6];

		inv[6] = -matrix[0] * matrix[6] * matrix[15] +
			matrix[0] * matrix[7] * matrix[14] +
			matrix[4] * matrix[2] * matrix[15] -
			matrix[4] * matrix[3] * matrix[14] -
			matrix[12] * matrix[2] * matrix[7] +
			matrix[12] * matrix[3] * matrix[6];

		inv[10] = matrix[0] * matrix[5] * matrix[15] -
			matrix[0] * matrix[7] * matrix[13] -
			matrix[4] * matrix[1] * matrix[15] +
			matrix[4] * matrix[3] * matrix[13] +
			matrix[12] * matrix[1] * matrix[7] -
			matrix[12] * matrix[3] * matrix[5];

		inv[14] = -matrix[0] * matrix[5] * matrix[14] +
			matrix[0] * matrix[6] * matrix[13] +
			matrix[4] * matrix[1] * matrix[14] -
			matrix[4] * matrix[2] * matrix[13] -
			matrix[12] * matrix[1] * matrix[6] +
			matrix[12] * matrix[2] * matrix[5];

		inv[3] = -matrix[1] * matrix[6] * matrix[11] +
			matrix[1] * matrix[7] * matrix[10] +
			matrix[5] * matrix[2] * matrix[11] -
			matrix[5] * matrix[3] * matrix[10] -
			matrix[9] * matrix[2] * matrix[7] +
			matrix[9] * matrix[3] * matrix[6];

		inv[7] = matrix[0] * matrix[6] * matrix[11] -
			matrix[0] * matrix[7] * matrix[10] -
			matrix[4] * matrix[2] * matrix[11] +
			matrix[4] * matrix[3] * matrix[10] +
			matrix[8] * matrix[2] * matrix[7] -
			matrix[8] * matrix[3] * matrix[6];

		inv[11] = -matrix[0] * matrix[5] * matrix[11] +
			matrix[0] * matrix[7] * matrix[9] +
			matrix[4] * matrix[1] * matrix[11] -
			matrix[4] * matrix[3] * matrix[9] -
			matrix[8] * matrix[1] * matrix[7] +
			matrix[8] * matrix[3] * matrix[5];

		inv[15] = matrix[0] * matrix[5] * matrix[10] -
			matrix[0] * matrix[6] * matrix[9] -
			matrix[4] * matrix[1] * matrix[10] +
			matrix[4] * matrix[2] * matrix[9] +
			matrix[8] * matrix[1] * matrix[6] -
			matrix[8] * matrix[2] * matrix[5];

		det = matrix[0] * inv[0] + matrix[1] * inv[4] + matrix[2] * inv[8] + matrix[3] * inv[12];

		if (det == 0)
			return false;

		det = 1.0 / det;

		for (i = 0; i < 16; ++i) {
			matrix[i] = static_cast<T>(inv[i] * det);
		}

		return true;
	}
	void look_at(const Vector3<T>& start, const Vector3<T>& end, const Vector3<T>& _xaxis = Vector3<T>(0, 1, 0))
	{
		identity();

		Vector3<T> zaxis = (start - end);
		zaxis.normal();

		Vector3<T> xaxis(_xaxis);
		xaxis.cross(zaxis);
		xaxis.normal();

		Vector3<T> yaxis = zaxis;
		yaxis.cross(xaxis);

		set(0, xaxis.x);
		set(1, xaxis.y);
		set(2, xaxis.z);
		set(3, -xaxis.dot(start));
		set(4, yaxis.x);
		set(5, yaxis.y);
		set(6, yaxis.z);
		set(7, -yaxis.dot(start));
		set(8, zaxis.x);
		set(9, zaxis.y);
		set(10, zaxis.z);
		set(11, -zaxis.dot(start));
	}
	void set_from_quaternion(const quaternion& q)
	{
		set(0, (T)(1 - 2 * q.y * q.y - 2 * q.z * q.z));
		set(1, (T)(2 * q.x * q.y - 2 * q.w * q.z));
		set(2, (T)(2 * q.x * q.z + 2 * q.w * q.y));
		set(3, (T)(0));
		set(4, (T)(2 * q.x * q.y + 2 * q.w * q.z));
		set(5, (T)(1 - 2 * q.x * q.x - 2 * q.z * q.z));
		set(6, (T)(2 * q.y * q.z - 2 * q.w * q.x));
		set(7, (T)(0));
		set(8, (T)(2 * q.x * q.z - 2 * q.w * q.y));
		set(9, (T)(2 * q.y * q.z + 2 * q.w * q.x));
		set(10, (T)(1 - 2 * q.x * q.x - 2 * q.y * q.y));
		set(11, (T)(0));
		set(12, (T)(0));
		set(13, (T)(0));
		set(14, (T)(0));
		set(15, (T)(1));
	}

	void set(const Matrix4<T>& other)
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] = other.matrix[i];
		}
	}
	void set(T vals[])
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] = vals[i];
		}
	}
	void set(T val)
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] = val;
		}
	}
	void set(int pos, T val)
	{
		matrix[pos] = val;
	}
	T get(int pos) const
	{
		return matrix[pos];
	}
	void add(const Matrix4<T>& other)
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] += other.matrix[i];
		}
	}
	void subtract(const Matrix4<T>& other)
	{
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] += other.matrix[i];
		}
	}
	Matrix4<T> multiply(const Matrix4<T>& other) const
	{
		Matrix4<T> m;
		for (int i = 0; i < SIZE; ++i) {
			m.matrix[i] = 0;
			for (int j = 0; j < 4; ++j) {
				m.matrix[i] += matrix[(i / 4) * 4 + j] * other.matrix[(i % 4) + (j * 4)];
			}
		}

		return m;
	}
	Vector4<T> multiply(const Vector4<T>& other) const
	{
		T vals[4];
		for (int i = 0; i < 4; ++i) {
			vals[i] = 0;
			vals[i] += other.x * matrix[i * 4] + other.y * matrix[i * 4 + 1] + other.z * matrix[i * 4 + 2] + other.w * matrix[i * 4 + 3];
		}

		//cout << vals[0] << ", " << vals[1] << ", " << vals[2] << ", " << vals[3] << endl;
		return Vector4<T>(vals);
	}
	void multiplication_by_product(const Matrix4<T>& m1, const Matrix4<T>& m2)
	{
		// quicker that calling matrix = matrix1 * matrix2
		for (int i = 0; i < SIZE; ++i) {
			matrix[i] = 0;
			for (int j = 0; j < 4; ++j) {
				matrix[i] += m1.matrix[(i / 4) * 4 + j] * m2.matrix[(i % 4) + (j * 4)];
			}
		}
	}

	void translate(const vector3df& pos)
	{
		matrix[3] += pos.x;
		matrix[7] += pos.y;
		matrix[11] += pos.z;
	}
	void set_translation(const vector3df& pos)
	{
		matrix[3] = pos.x;
		matrix[7] = pos.y;
		matrix[11] = pos.z;
	}

	void scale(const vector3df& scale)
	{
		matrix[0] *= scale.x;
		matrix[5] *= scale.y;
		matrix[10] *= scale.z;
	}

	void set_rotation(const Vector3<T>& vec, const Vector3<T>& off = Vector3<T>())
	{
		//Identity();

		// z rotation matrix
		if (vec.z != 0)
		{
			Matrix4<T> m;
			m.set(0, cos(vec.z));
			m.set(1, -sin(vec.z));
			m.set(4, sin(vec.z));
			m.set(5, cos(vec.z));
			m.set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
			m.set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
			m.set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
			set(multiply(m));
		}

		// x rotation matrix
		if (vec.x != 0)
		{
			Matrix4<T> m;
			m.set(5, cos(vec.x));
			m.set(6, -sin(vec.x));
			m.set(9, sin(vec.x));
			m.set(10, cos(vec.x));
			m.set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
			m.set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
			m.set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
			set(multiply(m));
		}

		// y rotation matrix
		if (vec.y != 0)
		{
			Matrix4<T> m;
			m.set(0, cos(vec.y));
			m.set(2, sin(vec.y));
			m.set(8, -sin(vec.y));
			m.set(10, cos(vec.y));
			m.set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
			m.set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
			m.set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
			set(multiply(m));
		}
	}

	const T* get_data_pointer() const { return &matrix[0]; }

	void operator=(const Matrix4<T>& other) { set(other); }
	//Matrix4<T> operator+(const Matrix4<T> &other) { return Matrix4<T>(x + other.x, y + other.y); }
	//Matrix4<T> operator-(const Matrix4<T> &other) { return Matrix4<T>(x - other.x, y - other.y); }
	void operator+=(const Matrix4<T>& other) { add(other); }
	void operator-=(const Matrix4<T>& other) { subtract(other); }
	void operator*=(const Matrix4<T>& other) { set(multiply(other)); }
	Matrix4<T> operator*(const Matrix4<T>& other) const { return multiply(other); }
	bool operator==(const Matrix4<T>& other) const
	{
		for (int i = 0; i < SIZE; ++i) {
			if (matrix[i] != other.matrix[i])
				return false;
		}

		return true;
	}
	bool operator!=(const Matrix4<T>& other) const { return !operator==(other); }

protected:

private:
	static const int SIZE = 16;
	T matrix[SIZE];
};

typedef Matrix4<int>	matrix4i;
typedef Matrix4<short>	matrix4s;
typedef Matrix4<float>	matrix4f;

#endif
