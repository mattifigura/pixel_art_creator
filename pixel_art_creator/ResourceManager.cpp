#include "ResourceManager.h"

template<typename T>
ResourceManager<T>::ResourceManager(T* (*func)(const std::string& key))
	: _resources()
	, _creation_function(func)
{

}

template<typename T>
ResourceManager<T>::~ResourceManager()
{
	clear();
}

template<typename T>
void ResourceManager<T>::clear()
{
	_resources.clear();
}

template<typename T>
const T& ResourceManager<T>::get_resource(const std::string& key)
{
	// Check if this resource already exists
	auto resource = _resources.find(key);
	if (resource != _resources.end())
		return *resource->second;

	// The resource doesn't exist, create it and then return it
	_resources.insert(std::make_pair(key, std::unique_ptr<T>(_creation_function(key))));
	return *(_resources.find(key)->second);
}