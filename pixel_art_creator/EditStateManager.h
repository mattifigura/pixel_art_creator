#ifndef EDIT_STATE_MANAGER_H
#define EDIT_STATE_MANAGER_H

#include "EditState.h"

#include <memory>
#include <deque>

class EditStateManager {
public:
	EditStateManager(EditStateManager const&) = delete;
	void operator=(EditStateManager const&) = delete;

	static EditStateManager& instance()
	{
		static EditStateManager singleton;
		return singleton;
	}

	void undo();
	void redo();

	void add_state(EditState* state);
	void clear_states();

protected:

private:
	std::deque<std::shared_ptr<EditState>> _undo_stack;
	std::deque<std::shared_ptr<EditState>> _redo_stack;

	EditStateManager();
};

#endif