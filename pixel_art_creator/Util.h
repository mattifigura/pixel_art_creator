#ifndef UTIL_H
#define UTIL_H

#include "GLBase.h"

#include <string>
#include <vector>

// Helper functions for our program
namespace util {
	// Fills in a line between two points
	// We needed this to paint properly without breaks in the line
	void get_pixel_line(const vector2di& start, const vector2di& end, std::vector<vector2di>& points);

	// Mix two colours together at a ratio
	GLColour mix_colours(const GLColour& one, const GLColour& two, float ratio);

	// Blend two colours together
	GLColour blend_colours(const GLColour& one, const GLColour& two);

	// Is the point in the given square
	template<typename T>
	bool is_point_inside(const T& point, const T& one, const T& two);

	// Convert RGB to HSV
	HSV rgb_to_hsv(const GLColour& col);

	// Convert HSV to RGB
	GLColour hsv_to_rgb(const HSV& col);

	// Split word by regex
	std::vector<std::string> split_by_regex(const std::string& in, const std::string& regex_string);

	// Read the given file into a string
	bool file_to_string(const std::string& filename, std::string& out);

	// Remove trailing 0's
	void remove_trailing_zeroes(std::string& in);

	// Float to string
	std::string float_to_string(float in);
}

#endif
