#include "Texture.h"
#include "Util.h"

Texture::Texture()
	: _data()
	, _size()
	, _index(0)
{

}

Texture::~Texture()
{
	if (VALID_BUFFER(_index))
		glDeleteTextures(1, &_index);
}

void Texture::create_colour_image(int width, int height, const GLColour& col, bool buffer_data)
{
	_data.clear();
	_data.resize(static_cast<size_t>(width) * static_cast<size_t>(height), col);
	_data.shrink_to_fit();
	_size.set(width, height);

	if (buffer_data)
		buffer();
}

void Texture::create_gradient_image(int width, int height, const GLColour& start, const GLColour& end, bool horizontal, bool buffer_data)
{
	_data.clear();
	_data.resize(static_cast<size_t>(width) * static_cast<size_t>(height));
	_data.shrink_to_fit();
	_size.set(width, height);

	vector4df diff(static_cast<float>(end.r) - static_cast<float>(start.r),
		static_cast<float>(end.g) - static_cast<float>(start.g),
		static_cast<float>(end.b) - static_cast<float>(start.b),
		static_cast<float>(end.a) - static_cast<float>(start.a));
	float progress = 0.f;

	for (int x = 0; x < width; ++x) {
		if (horizontal)
			progress = static_cast<float>(x) / static_cast<float>(width);

		for (int y = 0; y < height; ++y) {
			if (!horizontal)
				progress = static_cast<float>(y) / static_cast<float>(height);
			
			_data[static_cast<size_t>(y) * static_cast<size_t>(width) + static_cast<size_t>(x)].set(
				start.r + static_cast<GLubyte>(diff.x * progress),
				start.g + static_cast<GLubyte>(diff.y * progress),
				start.b + static_cast<GLubyte>(diff.z * progress),
				start.a + static_cast<GLubyte>(diff.w * progress));
		}
	}

	if (buffer_data)
		buffer();
}

void Texture::create_gradient_image(int width, int height, const GLColour& top_left, const GLColour& top_right, const GLColour& bottom_left, const GLColour& bottom_right, bool buffer_data)
{
	_data.clear();
	_data.resize(static_cast<size_t>(width) * static_cast<size_t>(height));
	_data.shrink_to_fit();
	_size.set(width, height);

	for (int x = 0; x < width; ++x) {
		GLColour top_blend = util::mix_colours(top_left, top_right, static_cast<float>(x) / static_cast<float>(width));
		GLColour bottom_blend = util::mix_colours(bottom_left, bottom_right, static_cast<float>(x) / static_cast<float>(width));
		for (int y = 0; y < height; ++y) {
			set_pixel(x, y, util::mix_colours(top_blend, bottom_blend, static_cast<float>(y) / static_cast<float>(height)));
		}
	}

	if (buffer_data)
		buffer();
}

void Texture::create_texture_from_buffer(const GLubyte* data_buffer, int width, int height, GLenum type, bool buffer_data)
{
	// Create a custom texture from the given buffer
	_data.clear();
	bool luminance_alpha = type == GL_LUMINANCE_ALPHA;

	// Set up our internal buffer
	for (int x = 0; x < width; ++x) {
		for (int y = 0; y < height; ++y) {
			GLColour pixel;
			if (luminance_alpha)
			{
				// 2 bytes per pixel aka greyscale
				pixel.r = data_buffer[x * (height * 2) + y];
				pixel.g = data_buffer[x * (height * 2) + y];
				pixel.b = data_buffer[x * (height * 2) + y];
				pixel.a = data_buffer[x * (height * 2) + y + 1];
			}
			else
			{
				// RGBA
				pixel.r = data_buffer[(x * height + y) * 4];
				pixel.g = data_buffer[(x * height + y) * 4 + 1];
				pixel.b = data_buffer[(x * height + y) * 4 + 2];
				pixel.a = data_buffer[(x * height + y) * 4 + 3];
			}

			_data.push_back(pixel);
		}
	}
	_data.shrink_to_fit();
	_size.set(width, height);

	if (buffer_data)
		buffer();
}

void Texture::create_texture_from_buffer(const std::vector<GLColour>& data_buffer, int width, int height, bool buffer_data)
{
	_data.clear();
	_data = data_buffer;
	_data.shrink_to_fit();
	_size.set(width, height);

	if (buffer_data)
		buffer();
}

void Texture::resize(int width, int height)
{
	if (_size.x != width || _size.y != height)
	{
		auto temp(_data);
		_data.clear();
		_data.shrink_to_fit();
		_data.resize(static_cast<size_t>(width) * static_cast<size_t>(height), GLColour(0, 0));

		// Copy the old data over our new data
		int width_to_copy = std::min(width, _size.x);
		int height_to_copy = std::min(height, _size.y);
		int old_width = _size.x;
		_size.set(width, height);

		for (int x = 0; x < width_to_copy; ++x) {
			for (int y = 0; y < height_to_copy; ++y) {
				set_pixel(x, y, temp[static_cast<size_t>(y) * static_cast<size_t>(old_width) + static_cast<size_t>(x)]);
			}
		}

		buffer();
	}
}

void Texture::set_pixel(int x, int y, const GLColour& col)
{
	_data[static_cast<size_t>(y) * static_cast<size_t>(_size.x) + static_cast<size_t>(x)] = col;
}

const GLColour& Texture::get_pixel(int x, int y) const
{
	return _data[static_cast<size_t>(y) * static_cast<size_t>(_size.x) + static_cast<size_t>(x)];
}

const std::vector<GLColour>& Texture::get_data() const
{
	return _data;
}

bool Texture::buffer()
{
	if (INVALID_BUFFER(_index))
		glGenTextures(1, &_index);

	if (VALID_BUFFER(_index))
	{
		// We should be able to reinterpret the data vector as unsigned bytes as that is all a GLColour is
		glBindTexture(GL_TEXTURE_2D, _index);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _size.x, _size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, reinterpret_cast<GLubyte*>(&_data[0]));
		glBindTexture(GL_TEXTURE_2D, 0);

		return true;
	}

	return false;
}

GLuint Texture::get_index() const
{
	return _index;
}

const vector2di& Texture::get_size() const
{
	return _size;
}