#ifndef MENU_BAR_H
#define MENU_BAR_H

#include "Text.h"

#include <map>
#include <functional>

class MenuBar : public GUIElement {
public:
	MenuBar(const vector2df& pos, const vector2df& size, const Font* font, GUIElement* parent = nullptr);
	~MenuBar();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	void add_menu(const std::string& name);
	void add_menu_item(const std::string& name, const std::string& item, int key = -1, int modifier = -1, const std::function<void()>& func = nullptr);

	virtual bool mouse_event(int button, int action, int mods);
	virtual bool key_event(int key, int scancode, int action, int mods);

protected:

private:
	struct MenuItem {
		Text* items;
		std::vector<std::pair<int, int>> accelerators;
		std::vector<std::function<void()>> funcs;

		MenuItem() : items(nullptr), accelerators(), funcs() {}
	};

	Text* _menu_text;
	std::vector<std::string> _menu_names;
	std::unordered_map<std::string, MenuItem> _menus;
	std::vector<vector4df> _menu_boxes;

	bool _menu_open;
	bool _menu_was_open;
	size_t _menu_selected;
	size_t _menu_item_selected;

	void update_visuals();
};

#endif