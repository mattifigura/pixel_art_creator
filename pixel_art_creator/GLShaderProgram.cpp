#include "GLShaderProgram.h"
#include <iostream>
#include <vector>

GLShaderProgram::GLShaderProgram()
	: _vertex_shader(0)
	, _fragment_shader(0)
	, _geometry_shader(0)
	, _shader_program(0)
	, _angle(85.f)
	, _ratio(16.f / 9.f)
	, _near(0.1f)
	, _far(800.f)
	, _current_angle(_angle)
	, _vertex("")
	, _fragment("")
	, _geometry("")
{
	// Unusuable until vertex & fragment shader strings are provided and compiled
}

GLShaderProgram::GLShaderProgram(const std::string& vertex, const std::string& fragment, const std::string& geometry)
	: _vertex_shader(0)
	, _fragment_shader(0)
	, _geometry_shader(0)
	, _shader_program(0)
	, _angle(85.f)
	, _ratio(16.f / 9.f)
	, _near(0.1f)
	, _far(800.f)
	, _current_angle(_angle)
	, _vertex(vertex)
	, _fragment(fragment)
	, _geometry(geometry)
{
	// Only compile shaders if we've been given any to compile
	if (!vertex.empty() && !fragment.empty())
		compile_shaders();
}

GLShaderProgram::~GLShaderProgram()
{
	clear();
}

void GLShaderProgram::clear()
{
	glDeleteShader(_vertex_shader);
	glDeleteShader(_fragment_shader);

	if (_geometry_shader > 0)
		glDeleteShader(_geometry_shader);

	glDeleteProgram(_shader_program);

	_vertex_shader = 0;
	_fragment_shader = 0;
	_geometry_shader = 0;
}

bool GLShaderProgram::compile_shaders()
{
	// Load the given shaders (supplied as strings) and compile a shader program
	const GLchar* vertex = _vertex.c_str();
	_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(_vertex_shader, 1, &vertex, NULL);
	glCompileShader(_vertex_shader);

	const GLchar* fragment = _fragment.c_str();
	_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(_fragment_shader, 1, &fragment, NULL);
	glCompileShader(_fragment_shader);

	// Geometry shaders are optional so check if we have a valid one before trying
	bool geometry_shader = !_geometry.empty();
	if (geometry_shader)
	{
		const GLchar* geometry = _geometry.c_str();
		_geometry_shader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(_geometry_shader, 1, &geometry, NULL);
		glCompileShader(_geometry_shader);
	}

	// Check that the shaders have compiled correctly
	GLint shader_status(GL_FALSE);
	glGetShaderiv(_vertex_shader, GL_COMPILE_STATUS, &shader_status);
	if (shader_status != GL_TRUE)
	{
		//cout << "Vertex shader failed to compile!" << endl;
		GLint max_length = 0;
		glGetShaderiv(_vertex_shader, GL_INFO_LOG_LENGTH, &max_length);

		std::vector<GLchar> error_log(max_length);
		glGetShaderInfoLog(_vertex_shader, max_length, &max_length, &error_log[0]);

		std::cout << "Vertex shader failed to compile! Details: " << std::endl;
		for (GLint i = 0; i < max_length; ++i) {
			std::cout << error_log[i];
		}
		std::cout << std::endl;
		return false;
	}

	glGetShaderiv(_fragment_shader, GL_COMPILE_STATUS, &shader_status);
	if (shader_status != GL_TRUE)
	{
		//cout << "Fragment shader failed to compile!" << endl;
		GLint max_length = 0;
		glGetShaderiv(_fragment_shader, GL_INFO_LOG_LENGTH, &max_length);

		std::vector<GLchar> error_log(max_length);
		glGetShaderInfoLog(_fragment_shader, max_length, &max_length, &error_log[0]);

		std::cout << "Fragment shader failed to compile! Details: " << std::endl;
		for (GLint i = 0; i < max_length; ++i) {
			std::cout << error_log[i];
		}
		std::cout << std::endl;
		return false;
	}

	if (geometry_shader)
	{
		glGetShaderiv(_geometry_shader, GL_COMPILE_STATUS, &shader_status);
		if (shader_status != GL_TRUE)
		{
			//cout << "Fragment shader failed to compile!" << endl;
			GLint max_length = 0;
			glGetShaderiv(_geometry_shader, GL_INFO_LOG_LENGTH, &max_length);

			std::vector<GLchar> error_log(max_length);
			glGetShaderInfoLog(_geometry_shader, max_length, &max_length, &error_log[0]);

			std::cout << "Geometry shader failed to compile! Details: " << std::endl;
			for (GLint i = 0; i < max_length; ++i) {
				std::cout << error_log[i];
			}
			std::cout << std::endl;
			return false;
		}
	}

	// Create the shader program
	_shader_program = glCreateProgram();
	glAttachShader(_shader_program, _vertex_shader);
	glAttachShader(_shader_program, _fragment_shader);

	if (geometry_shader)
		glAttachShader(_shader_program, _geometry_shader);

	glLinkProgram(_shader_program);

	// Check that the program was created successfully and use it
	glGetProgramiv(_shader_program, GL_LINK_STATUS, &shader_status);
	if (shader_status != GL_TRUE)
	{
		std::cout << "Shader program failed to link!" << std::endl;
		return false;
	}

	return true;
}

bool GLShaderProgram::recompile_shaders()
{
	clear();
	return compile_shaders();
}

void GLShaderProgram::set_shaders(const std::string& vertex, const std::string& fragment, const std::string& geometry)
{
	_vertex = vertex;
	_fragment = fragment;
	_geometry = geometry;
}

void GLShaderProgram::bind()
{
	glUseProgram(_shader_program);
}

void GLShaderProgram::unbind()
{
	glUseProgram(0);
}

GLint GLShaderProgram::get_attrib_location(const GLchar* attrib)
{
	// Remember to call Bind() first!
	return glGetAttribLocation(_shader_program, attrib);
}

GLint GLShaderProgram::get_uniform_location(const GLchar* attrib)
{
	// Remember to call Bind() first!
	return glGetUniformLocation(_shader_program, attrib);
}

void GLShaderProgram::set_ortho_projection(int width, int height)
{
	bind();
	_proj_matrix.orthographic(0.f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.f, -100.f, 100.f);
	glUniformMatrix4fv(get_uniform_location("projection"), 1, GL_TRUE, _proj_matrix.get_data_pointer());
	unbind();
}

void GLShaderProgram::set_perspective_projection(float angle, float ratio, float near, float far, bool defaults)
{
	bind();
	_proj_matrix.perspective(angle, ratio, near, far);
	glUniformMatrix4fv(get_uniform_location("projection"), 1, GL_TRUE, _proj_matrix.get_data_pointer());
	unbind();

	// If we are setting the defaults, do so now
	if (defaults)
	{
		_angle = angle;
		_ratio = ratio;
		_near = near;
		_far = far;
	}
}

const matrix4f& GLShaderProgram::get_projection_matrix() const
{
	return _proj_matrix;
}
