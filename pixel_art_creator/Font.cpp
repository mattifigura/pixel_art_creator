#include "Font.h"

#ifdef __linux__
#include <ft2build.h>
#else
#include "ft2build.h"
#endif

#include FT_GLYPH_H
#include FT_FREETYPE_H

#include <algorithm>
#include <iostream>

Font::Font(std::string_view font)
	: _texture(nullptr)
	, _font()
	, _height(0)
	, _font_size(0)
	, _filename("")
{
	// Start up FreeType and load the font with the correct size
	extract_font_info(font);

	std::cout << "Loading font " << _filename << ", size " << _height << std::endl;

	FT_Library ftl;
	if (FT_Init_FreeType(&ftl))
	{
		std::cout << "ERROR: Cannot initialise FreeType!" << std::endl;
		return;
	}

	FT_Face face;
	if (FT_New_Face(ftl, _filename.c_str(), 0, &face))
	{
		std::cout << "ERROR: Cannot open font " << _filename << std::endl;
		FT_Done_FreeType(ftl);
		return;
	}

	FT_Set_Pixel_Sizes(face, 0, static_cast<unsigned int>(_height));

	// Read each character to  get the max height of a character
	FT_GlyphSlot glyph = face->glyph;
	_font_size = _height;
	for (int i = -128; i < 128; ++i) {
		FT_Load_Char(face, static_cast<char>(i), FT_LOAD_RENDER);

		_height = std::max(_height, _font_size + (static_cast<int>(glyph->bitmap.rows) - static_cast<int>(glyph->bitmap_top)));
	}

	int image_size = 256;
	int needed = static_cast<int>(sqrt(_height * _height * 256));
	while (needed >= image_size)
	{
		image_size <<= 1;
	}

#ifndef NDEBUG
	std::cout << _filename << " at size " << _font_size << " will be " << image_size << "x" << image_size << " in dimensions" << std::endl;
#endif

	// Read in each glyph and construct our texture
	_texture.reset(new Texture);
	_texture->create_colour_image(image_size, image_size, GLColour(0, 0, 0, 0), false);

	int x = 0;
	int y = 0;

	for (int i = -128; i < 128; ++i) {
		// Get the details of the character
		char c = static_cast<char>(i);

		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "INFO: Loading font " << _filename << ": cannot load character " << c << std::endl;
			continue;
		}

		FontGlyph character_glyph;
		character_glyph.advance = static_cast<GLubyte>(ceil(glyph->advance.x * 0.015625f)); // Magic number 1/64 - why???
		character_glyph.width = character_glyph.advance;
		character_glyph.top_offset = _font_size - glyph->bitmap_top;

		if (x + character_glyph.width >= image_size)
		{
			x = 0;
			y += _height;
		}
		character_glyph.pos.set(x, y);
		_font.emplace(c, character_glyph);

		// Write the pixel data to our texture
		int start_x = std::max(x + glyph->bitmap_left, 0);
		int start_y = std::max(y, 0); //  + _font_size - glyph->bitmap_top - We will leave this out here and add it on when building text meshes
									  // This has been added to the FontGlyp struct - see top_offset
		for (int j = 0; j < static_cast<int>(glyph->bitmap.width); ++j) {
			for (int k = 0; k < static_cast<int>(glyph->bitmap.rows); ++k) {
				_texture->set_pixel(start_x + j, start_y + k, GLColour(255, 255, 255, glyph->bitmap.buffer[j + k * glyph->bitmap.width]));
			}
		}
		x += _height;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ftl);

	_texture->buffer();
}

Font::~Font()
{

}

const FontGlyph Font::get_character_info(char c) const
{
	// Look for the character in the map
	auto character = _font.find(c);
	if (character != _font.end())
		return character->second;

	// We have not found the character, return an empty one
	static FontGlyph empty_character;
	return empty_character;
}

const Texture* Font::get_texture() const
{
	return _texture.get();
}

int Font::get_height() const
{
	return _height;
}

int Font::get_string_width(std::string_view in) const
{
	int width = 0;

	size_t i = 0;
	while (i < in.length())
	{
		const char c = in[i++];

		if (c == '\0')
			break;

		width += get_character_info(c).advance;
	}

	return width;
}

void Font::extract_font_info(std::string_view key)
{
	// Find the colon in the key
	size_t size = key.find(":{");

	// Get the filename and size
	_filename = key.substr(0, size);
	size = atoi(std::string(key.substr(size + 1, key.length() - size - 1)).c_str());

	// If we weren't provided with a size use a default of 16
	if (size == 0)
		size = 16;

	_height = static_cast<int>(size);
}
