#include "Pane.h"

Pane::Pane(const vector2df& pos, const vector2df& size, GUIElement* parent, bool force_mesh)
	: GUIElement(pos, size, parent, force_mesh)
{

}

Pane::~Pane()
{

}

void Pane::build()
{
	vector2df pos = get_position();
	vector2df size = get_size();

	if (!get_parent() || get_mesh_ptr())
		pos = vector2df();

	GLColour col(120, 120, 120, 220);

	_mesh_start = get_mesh().add_quad(GLVertex2D(pos, col, vector2df()),
		GLVertex2D(pos + vector2df(size.x, 0.f), col, vector2df(1.f, 0.f)),
		GLVertex2D(pos + size, col, vector2df(1.f, 1.f)),
		GLVertex2D(pos + vector2df(0.f, size.y), col, vector2df(0.f, 1.f)));
}

void Pane::update_mesh()
{
	vector2df pos = get_position();
	vector2df size = get_size();
	
	if (!get_parent() || get_mesh_ptr())
		pos = vector2df();

	get_mesh().set_quad_position(_mesh_start, pos, pos + vector2df(size.x, 0.f), pos + size, pos + vector2df(0.f, size.y));
}