#ifndef WINDOW_PANE_H
#define WINDOW_PANE_H

#include "Text.h"

#include <functional>

class Button;

class WindowPane : public GUIElement {
public:
	WindowPane(const vector2df& pos, const vector2df& size, const Font* font, const std::string& title);
	~WindowPane();

	virtual void build();

	virtual bool update(int mouse_x, int mouse_y);

	void set_move_limits(const vector4df& limits);

	void set_title(const std::string& title);
	const std::string& get_title() const;

	float get_titlebar_height() const;
	vector2df get_content_size() const;

	void set_keep_focus(bool focus);
	void set_can_resize(bool resize);

	void set_close_button_function(const std::function<void()>& func);

	virtual bool mouse_hover(bool entering);
	virtual bool mouse_event(int button, int action, int mods);
	virtual bool scroll_event(double x, double y);
	virtual bool key_event(int key, int scancode, int action, int mods);
	virtual bool char_event(unsigned int codepoint);
	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:
	virtual void update_mesh();

private:
	Text* _title;
	Button* _close_button;

	bool _keep_focus;

	bool _moving;
	vector2df _mouse_move_position;
	vector2df _mouse_resize_offset;
	vector2df _mouse_position;

	vector4df _move_limits;
	vector2df _original_size;

	bool _can_resize;
	bool _resizing;
	bool _resizing_left;
	bool _resizing_right;
	bool _resizing_top;
	bool _resizing_bottom;

	std::function<void()> _close_function;

	static std::unique_ptr<Texture> _close_texture;
};

#endif