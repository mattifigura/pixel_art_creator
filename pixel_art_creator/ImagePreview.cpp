#include "ImagePreview.h"
#include "ColourRect.h"
#include "ImageLayer.h"
#include "ProgramManager.h"
#include "Util.h"
#include "WindowPane.h"

ImagePreview::ImagePreview(WindowPane* window)
	: GUIElement(vector2df(2.f, window->get_titlebar_height()), window->get_content_size() + vector2df(0.f, 2.f), window)
	, _window(window)
	, _layers()
	, _zoom(1.f)
	, _zoom_index(0)
	, _previous_zoom(0.f)
	, _layer_size(64.f, 64.f)
	, _layer_position()
	, _layer_adjust()
	, _previous_mouse_position()
	, _moving_perspective(false)
{
	vector2df abs_pos = get_absolute_position();
	const vector2df& size = get_size();
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));
}

ImagePreview::~ImagePreview()
{

}

void ImagePreview::build()
{

}

bool ImagePreview::update(int mouse_x, int mouse_y)
{
	vector2df size = _window->get_content_size() + vector2df(0.f, 2.f);

	if (size != get_size())
		set_size(size);

	// Panning our perspective
	if (_moving_perspective)
		_layer_position += vector2df(static_cast<float>(mouse_x), static_cast<float>(mouse_y)) - _previous_mouse_position;

	// Keep our layers positioned correctly
	vector2df abs_pos = get_absolute_position();
	vector2df half_screen = get_size() * 0.5f;
	_layer_position.x = floor(std::max(std::min(_layer_position.x, _layer_size.x * _zoom * 0.5f), -_layer_size.x * _zoom * 0.5f));
	_layer_position.y = floor(std::max(std::min(_layer_position.y, _layer_size.y * _zoom * 0.5f), -_layer_size.y * _zoom * 0.5f));

	_layer_adjust.set(half_screen - (_layer_size * 0.5f * _zoom) + _layer_position);
	_layer_adjust.x = floor(_layer_adjust.x);
	_layer_adjust.y = floor(_layer_adjust.y);

	if (_layer_adjust != _layers[0]->get_position() || _previous_zoom != _zoom)
	{
		// We need to adjust
		const vector2df& size = get_size();
		set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));

		for (auto& layer : _layers) {
			layer->set_position(_layer_adjust);

			if (_zoom != _previous_zoom)
				layer->update(mouse_x, mouse_y);
		}
	}

	_previous_zoom = _zoom;
	_previous_mouse_position.set(static_cast<float>(mouse_x), static_cast<float>(mouse_y));
	return GUIElement::update(mouse_x, mouse_y);
}

void ImagePreview::add_layer(const Texture* texture)
{
	ColourRect* layer = new ColourRect(_layer_adjust, _layer_size, GLColour(), this, true);
	layer->set_texture(texture);
	layer->set_depth(10 + static_cast<int>(_layers.size()));
	layer->set_scale(vector2df(_zoom, _zoom));

	_layers.push_back(layer);
	ProgramManager::instance().add_gui_element_queued(layer);
}

void ImagePreview::remove_layer(size_t index)
{
	if (_layers.size() > 1 && index < _layers.size())
	{
		// Remove the given layer
		remove_child(_layers[index]);
		_layers[index]->kill();
		_layers.erase(std::find(_layers.begin(), _layers.end(), _layers[index]));

		// Need to adjust layer depth
		int depth = get_depth() + 1;
		for (size_t i = index; i < _layers.size(); ++i) {
			_layers[i]->set_depth(_layers[i]->get_depth() - depth - 1);
		}

		_layers.shrink_to_fit();
	}
}

ColourRect* ImagePreview::get_layer(size_t index) const
{
	if (index < _layers.size())
		return _layers[index];

	return nullptr;
}

void ImagePreview::move_layer(size_t original, size_t dest)
{
	// Move the item from layer to dest
	auto layer = _layers[original];
	int diff = static_cast<int>(dest) - static_cast<int>(original);
	int depth = get_depth() + 1;
	if (dest > original)
	{
		for (size_t i = original; i < dest; ++i) {
			_layers[i] = _layers[i + 1];
			_layers[i]->set_depth(_layers[i]->get_depth() - depth - 1);
		}
	}
	else
	{
		for (size_t i = original; i > dest; --i) {
			_layers[i] = _layers[i - 1];
			_layers[i]->set_depth(_layers[i]->get_depth() - depth + 1);
		}
	}

	_layers[dest] = layer;
	_layers[dest]->set_depth(_layers[dest]->get_depth() - depth + diff);
	ProgramManager::instance().sort_gui_elements();
}

void ImagePreview::set_layer_size(const vector2di& size)
{
	_layer_size = size;

	for (auto& layer : _layers) {
		layer->set_size(_layer_size);
	}
}

void ImagePreview::update_visibility(const std::vector<ImageLayer*>& layers)
{
	for (size_t i = 0; i < _layers.size(); ++i) {
		_layers[i]->set_visible(layers[i]->is_visible());
	}
}

void ImagePreview::set_position(const vector2df& pos)
{
	GUIElement::set_position(pos);

	vector2df abs_pos = get_absolute_position();
	const vector2df& size = get_size();
	set_cull_box(vector4df(abs_pos.x, abs_pos.y, abs_pos.x + size.x, abs_pos.y + size.y));
}

bool ImagePreview::mouse_event(int button, int action, int mods)
{
	if (is_mouse_hovered())
	{
		const float grab_size(7.f);
		const vector2df abs_pos = get_absolute_position();
		const vector2df& size = get_size();
		if (button == GLFW_MOUSE_BUTTON_MIDDLE)
		{
			if (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK)
			{
				_moving_perspective = true;
				return true;
			}
			else if (action == GLFW_RELEASE && _moving_perspective)
			{
				_moving_perspective = false;
				return true;
			}
		}
		else if (util::is_point_inside(_previous_mouse_position, abs_pos + vector2df(grab_size, 0.f), abs_pos + size - vector2df(grab_size, grab_size)) &&
			button == GLFW_MOUSE_BUTTON_LEFT && (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK))
		{
			return true;
		}
	}

	if (button == GLFW_MOUSE_BUTTON_MIDDLE)
		_moving_perspective = false;
	return false;
}

bool ImagePreview::scroll_event(double x, double y)
{
	if (!is_mouse_hovered())
		return false;
	
	// Zoom in and out
	int amount = static_cast<int>(y);

	static float zooms[] = { 1.f, 2.f, 4.f, 8.f, 16.f, 32.f, 64.f };
	_zoom_index = std::max(std::min(_zoom_index + amount, 6), 0);

	_zoom = zooms[_zoom_index];

	if (_zoom != _previous_zoom)
	{
		// Scale our image layers
		for (auto& layer : _layers) {
			layer->set_scale(vector2df(_zoom, _zoom));
		}

		// Adjust the layer position so that we're zooming based on the mouse position
		if (_layers[0]->is_mouse_hovered())
		{
			vector2df previous_mouse_relative = (static_cast<vector2df>(_previous_mouse_position - get_absolute_position()) - (_layer_adjust + (_layer_size * 0.5f * _previous_zoom))) / (_layer_size * (_previous_zoom * 0.5f));
			_layer_position -= previous_mouse_relative * (_layer_size * 0.5f) * (_zoom - _previous_zoom);
		}
	}

	return true;
}

bool ImagePreview::set_mouse_cursor(size_t& cursor) const
{
	return false;
}