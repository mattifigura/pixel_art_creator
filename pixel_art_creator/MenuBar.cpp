#include "MenuBar.h"
#include "ProgramManager.h"
#include "Util.h"

MenuBar::MenuBar(const vector2df& pos, const vector2df& size, const Font* font, GUIElement* parent)
	: GUIElement(pos, size, parent, true)
	, _menu_text(new Text(vector2df(), size, "", font, this))
	, _menu_names()
	, _menus()
	, _menu_boxes()
	, _menu_open(false)
	, _menu_was_open(false)
	, _menu_selected(0)
	, _menu_item_selected(0)
{
	_menu_text->set_expand_past_width(true, false);
	_menu_text->set_shadowed(true, false);

	if (font)
	{
		float text_offset = (size.y - font->get_height()) * 0.5f;
		_menu_text->set_position(vector2df(0.f, text_offset));
	}

	set_depth(5000000);
}

MenuBar::~MenuBar()
{

}

void MenuBar::build()
{
	Mesh& mesh = get_mesh();

	// Quads: 0 - top selection background, 1 - menu drop-down shadow, 2 - menu drop-down, 3 - menu item selection background
	mesh.add_quad(GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(0.f, 1.f))),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(1.f, 1.f))),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(1.f, 0.f))),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df())));
	mesh.add_quad(GLVertex2D(vector2df(), GLColour(0, 0)),
		GLVertex2D(vector2df(), GLColour(0, 0)),
		GLVertex2D(vector2df(), GLColour(0, 0)),
		GLVertex2D(vector2df(), GLColour(0, 0)));
	mesh.add_quad(GLVertex2D(vector2df(120, 0), GLColour(), GLTexCoord(vector2df())),
		GLVertex2D(vector2df(), GLColour(120, 0), GLTexCoord(vector2df(1.f, 0.f))),
		GLVertex2D(vector2df(), GLColour(120, 0), GLTexCoord(vector2df(1.f, 1.f))),
		GLVertex2D(vector2df(), GLColour(120, 0), GLTexCoord(vector2df(0.f, 1.f))));
	mesh.add_quad(GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df())),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(1.f, 0.f))),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(1.f, 1.f))),
		GLVertex2D(vector2df(), GLColour(255, 0), GLTexCoord(vector2df(0.f, 1.f))));
}

bool MenuBar::update(int mouse_x, int mouse_y)
{
	size_t previous_menu_selected(_menu_selected);
	size_t previous_menu_item_selected(_menu_item_selected);
	vector2df mouse_pos(static_cast<float>(mouse_x), static_cast<float>(mouse_y));

	if (is_mouse_hovered())
	{
		for (size_t i = 0; i < _menu_boxes.size(); ++i) {
			const vector4df& box = _menu_boxes[i];
			if (util::is_point_inside(mouse_pos, vector2df(box.x, box.y), vector2df(box.z, get_size().y)))
			{
				_menu_selected = i;
				break;
			}
		}

		if (previous_menu_selected != _menu_selected)
			_menu_item_selected = 9001;
	}
	else if (_menu_open && _menu_selected < _menus.size())
	{
		MenuItem& item = _menus[_menu_names[_menu_selected]];

		if (item.items->is_mouse_hovered())
			_menu_item_selected = std::min(static_cast<size_t>((mouse_pos.y - _menu_boxes[_menu_selected].w) / _menu_text->get_text_height()), item.funcs.size() - 1);
		else
			_menu_item_selected = 9001;
	}
	else
	{
		// It's over 9000!
		_menu_selected = 9001;
		_menu_item_selected = 9001;
	}

	if (previous_menu_selected != _menu_selected || previous_menu_item_selected != _menu_item_selected || _menu_was_open != _menu_open)
		update_visuals();

	_menu_was_open = _menu_open;
	return GUIElement::update(mouse_x, mouse_y);
}

void MenuBar::add_menu(const std::string& name)
{
	float x_pos = _menu_text->get_text_width() + 4.f;
	_menu_text->append_text("  " + name + "  ");
	_menu_names.push_back(name);

	MenuItem item;
	item.items = new Text(vector2df(x_pos + 4.f, 32.f), vector2df(200.f, 32.f), "", _menu_text->get_font(), this);

	_menus.emplace(name, item);
	_menus[name].items->set_expand_past_width(true, false);
	_menus[name].items->set_shadowed(true, false);
	_menus[name].items->set_visible(false);

	ProgramManager::instance().add_gui_element_queued(_menus[name].items);
	
	_menu_text->set_size(vector2df(_menu_text->get_text_width(), get_size().y));
	set_size(_menu_text->get_size());

	_menu_boxes.push_back(vector4df(x_pos - 4.f, _menu_text->get_absolute_position().y - 2.f,
		_menu_text->get_text_width(), _menu_text->get_absolute_position().y + _menu_text->get_text_height() - 2.f));
}

void MenuBar::add_menu_item(const std::string& name, const std::string& item, int key, int modifier, const std::function<void()>& func)
{
	if (_menus.find(name) != _menus.end())
	{
		std::string accelerator_text(" ");

		if (key > -1)
		{
			if (modifier > -1)
			{
				if ((modifier & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)
					accelerator_text += "Ctrl + ";
				if ((modifier & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT)
					accelerator_text += "Shift + ";
				if ((modifier & GLFW_MOD_ALT) == GLFW_MOD_ALT)
					accelerator_text += "Alt + ";
			}

			std::string key_name;

			if (key >= GLFW_KEY_F1 && key <= GLFW_KEY_F12)
				key_name = "F" + std::to_string(key - GLFW_KEY_F1 + 1);
			else if (key == GLFW_KEY_DELETE)
				accelerator_text += "Del";
			else
				key_name = std::string(glfwGetKeyName(key, glfwGetKeyScancode(key)));

			for (auto const c : key_name) {
				accelerator_text += toupper(c);
			}

			int width = _menu_text->get_font()->get_string_width(accelerator_text);
			accelerator_text = " %" + std::to_string(static_cast<int>(_menus[name].items->get_size().x) - 4 - width) + accelerator_text;
		}

		_menus[name].items->append_text(item + accelerator_text + " \n");
		_menus[name].items->set_size(vector2df(_menus[name].items->get_size().x, _menus[name].items->get_text_height()));
		_menus[name].funcs.push_back(func);

		_menus[name].accelerators.push_back(std::make_pair(modifier, key));
	}
}

bool MenuBar::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK)
		{
			if (is_mouse_hovered())
			{
				_menu_open = !_menu_open;

				if (_menu_open)
					ProgramManager::instance().lock(this);
				else
					ProgramManager::instance().unlock(this);

				return true;
			}
			else if (_menu_open && _menu_selected < _menus.size() && !_menus[_menu_names[_menu_selected]].items->is_mouse_hovered())
			{
				_menu_open = false;
				ProgramManager::instance().unlock(this);
				return true;
			}
		}
		else if (action == GLFW_RELEASE && _menu_open)
		{
			if (_menu_selected >= _menus.size())
				return false;
			
			MenuItem& item = _menus[_menu_names[_menu_selected]];
			if (_menu_item_selected < item.funcs.size())
			{
				// Unlock the GUI first incase the function needs to lock it to a different element
				_menu_open = false;
				ProgramManager::instance().unlock(this);
				
				if (item.funcs[_menu_item_selected])
					item.funcs[_menu_item_selected]();

				return true;
			}
		}
	}

	return false;
}

bool MenuBar::key_event(int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		for (auto menu_items : _menus) {
			size_t index = 0;
			for (auto keys : menu_items.second.accelerators) {
				if (keys.second > -1)
				{
					int modifier = std::max(keys.first, 0);
					if (key == keys.second && mods == modifier)
					{
						if (menu_items.second.funcs[index])
							menu_items.second.funcs[index]();

						return true;
					}
				}
				++index;
			}
		}
	}

	return false;
}

void MenuBar::update_visuals()
{
	bool menu_is_selected = _menu_selected < _menus.size();
	GLColour selected_colour(_menu_open ? 80 : 220, menu_is_selected || _menu_open ? 200 : 0);
	Mesh& mesh = get_mesh();

	mesh.set_quad_colour(0, selected_colour);
	mesh.set_quad_colour(2, GLColour(80, _menu_open ? 200 : 0));
	mesh.set_quad_colour(1, GLColour(0, _menu_open ? 50 : 0));
	mesh.set_quad_colour(3, GLColour(220, _menu_open && menu_is_selected && _menus[_menu_names[_menu_selected]].items->is_mouse_hovered() ? 200 : 0));

	for (auto& menu : _menus) {
		menu.second.items->set_visible(false);
	}

	if (menu_is_selected)
	{
		const vector4df& box = _menu_boxes[_menu_selected];
		mesh.set_quad_position(0, vector2df(box.x, box.y),
			vector2df(box.z, box.y),
			vector2df(box.z, box.w),
			vector2df(box.x, box.w));

		if (_menu_open)
		{
			MenuItem& item = _menus[_menu_names[_menu_selected]];
			const vector2df size = item.items->get_size() + vector2df(12.f, 4.f);
			mesh.set_quad_position(2, vector2df(box.x, box.w),
				vector2df(box.x + size.x, box.w),
				vector2df(box.x + size.x, box.w + size.y),
				vector2df(box.x, box.w + size.y));

			const vector2df shadow_offset(3.f, 3.f);
			mesh.set_quad_position(1, vector2df(box.x, box.w) + shadow_offset,
				vector2df(box.x + size.x, box.w) + shadow_offset,
				vector2df(box.x + size.x, box.w + size.y) + shadow_offset,
				vector2df(box.x, box.w + size.y) + shadow_offset);

			item.items->set_visible();

			float height = _menu_text->get_text_height();
			float y_pos = static_cast<float>(_menu_item_selected) * height;
			mesh.set_quad_position(3, vector2df(box.x, box.w + y_pos + 2.f),
				vector2df(box.x + size.x, box.w + y_pos + 2.f),
				vector2df(box.x + size.x, box.w + height + y_pos + 2.f),
				vector2df(box.x, box.w + height + y_pos + 2.f));
		}
	}
}