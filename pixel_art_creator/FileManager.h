#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "GLBase.h"
#include <string>
#include <vector>

struct LayerDetails {
	std::string name;
	float alpha;
	bool visible;
	const GLColour* data;

	LayerDetails() { name = ""; alpha = 1.f; visible = true; data = nullptr; }
};

class FileManager {
public:
	static bool read_png(const std::string& filename, std::vector<GLColour>& data, int& width, int& height);
	static bool write_png(const std::string& filename, std::vector<GLColour>& data, int width, int height);

	static bool read_pxla(const std::string& filename, std::vector<LayerDetails>& data, int& width, int& height);
	static bool write_pxla(const std::string& filename, std::vector<LayerDetails>& data, int width, int height);

	static std::string _executable_directory;

protected:

private:

};

#endif
