#ifndef TEXT_H
#define TEXT_H

#include "Font.h"
#include "GUIElement.h"

#include <string>

enum class TextAlignment {
	left,
	centre,
	right
};

class Text : public GUIElement {
public:
	Text(const vector2df& pos, const vector2df& size, const std::string& text, const Font* font, GUIElement* parent = nullptr);
	~Text();

	virtual void build();

	void set_font(const Font* font, bool rebuild = true);

	void set_text(const std::string& text, bool rebuild = true);
	void append_text(const std::string& text, int pos = -1, bool rebuild = true);
	const std::string& get_text() const;
	const std::string get_readable_text() const;

	const Font* get_font() const;

	size_t get_text_position(const vector2df& pos) const;

	size_t get_text_length() const;
	float get_text_width() const;
	float get_text_width(size_t offset, size_t count) const;
	float get_text_height() const;

	void set_line_spacing(float spacing, bool rebuild = true);
	float get_line_spacing() const;

	void set_alignment(TextAlignment align, bool rebuild = true);
	TextAlignment get_alignment() const;

	void set_split_by_word(bool split = true, bool rebuild = true);
	bool get_split_by_word() const;

	void set_shadowed(bool shadow = true, bool rebuild = true);
	bool get_shadowed() const;

	void set_expand_past_width(bool expand, bool rebuild = true);
	bool get_expand_past_width() const;

	virtual void set_size(const vector2df& size);

	void buffer();

	virtual bool set_mouse_cursor(size_t& cursor) const;

protected:

private:
	std::string _text;
	const Font* _font;
	TextAlignment _alignment;
	bool _changed;

	bool _split_by_word;
	bool _shadows;
	bool _expand_past_width;

	float _line_spacing;
	int _line_count;

	vector2df _shadow_offset;
	unsigned char _shadow_alpha;

	bool get_colour(GLColour& col, char c) const;
	void get_line_lengths(std::vector<int>& out) const;
};

#endif