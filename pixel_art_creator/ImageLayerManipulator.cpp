#include "ImageLayerManipulator.h"
#include "EditStateManager.h"
#include "ProgramManager.h"
#include "Util.h"

#include <thread>
#include <queue>

ImageLayerManipulator::ImageLayerManipulator(const vector2df& screen_size)
	: GUIElement(vector2df(), screen_size)
	, _layer_size(64.f, 64.f)
	, _layer_position()
	, _layer_adjust()
	, _texture(new Texture)
	, _mode(ManipulationMode::paint)
	, _pixel_position()
	, _pixel_edit_position()
	, _previous_mouse_position()
	, _paint_colour(GLColour(255, 0, 0, 255))
	, _layers()
	, _active_layer(0)
	, _painted(static_cast<size_t>(_layer_size.x) * static_cast<size_t>(_layer_size.y), false)
	, _hsv_pixels()
	, _zoom(8.f)
	, _zoom_index(3)
	, _previous_zoom(0.f)
	, _moving_perspective(false)
	, _mouse_down(false)
	, _just_clicked(false)
	, _cursor_visible(true)
	, _working_on_selection(false)
	, _select_start()
	, _select_end()
	, _copied_pixels()
	, _copied_size()
	, _pasted_pixels(new Texture)
	, _pasted_image(new ColourRect(vector2df(), vector2df(), GLColour()))
	, _paste_move_offset()
	, _rotate_pixels()
	, _rotate_texture(new Texture)
	, _rotate_image(new ColourRect(vector2df(), vector2df(), GLColour()))
	, _selection_expand(0.f)
	, _zoom_callback(nullptr)
	, _colour_picked_callback(nullptr)
	, _layer_size_changed_callback(nullptr)
	, _mode_changed_callback(nullptr)
	, _mouse_position_callback(nullptr)
	, _working_state(nullptr)
{
	// Keep us below the rest of the GUI but above the layers
	set_depth(-100);

	// We always need a background layer
	add_layer();

	// Set our texture to a blank texture
	_texture->create_colour_image(16, 16, GLColour());
	set_texture(_texture.get());

	// Ensure the pasted image is handled by the program
	_pasted_image->set_depth(get_depth() - 1);
	_pasted_image->set_visible(false);
	_pasted_image->set_texture(_pasted_pixels.get());
	_pasted_image->set_scale(vector2df(_zoom, _zoom));
	ProgramManager::instance().add_gui_element(_pasted_image);

	// Ensure the rotate image is handled by the program
	_rotate_image->set_depth(get_depth() - 1);
	_rotate_image->set_visible(false);
	_rotate_image->set_texture(_rotate_texture.get());
	_rotate_image->set_scale(vector2df(_zoom, _zoom));
	ProgramManager::instance().add_gui_element(_rotate_image);
}

ImageLayerManipulator::~ImageLayerManipulator()
{
	// If for some reason we have been killed during execution, kill our layers
	for (auto& layer : _layers) {
		layer->kill();
	}
	_pasted_image->kill();
}

void ImageLayerManipulator::build()
{
	// Build our "mouse cursor" (the cursor is separate, but we have a mesh highlighting the current pixel depending on mode)
	// The first quad is the pixel preview
	Mesh& mesh = get_mesh();

	mesh.add_quad(GLVertex2D(), GLVertex2D(), GLVertex2D(), GLVertex2D());

	// This is the selection box
	mesh.add_quad(GLVertex2D(vector2df(), GLColour(0, 0)), GLVertex2D(vector2df(), GLColour(0, 0)), GLVertex2D(vector2df(), GLColour(0, 0)), GLVertex2D(vector2df(), GLColour(0, 0)));

	// The next 8 are the border
	GLColour black(0, 255);
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));

	// These 4 are the image border
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));
	mesh.add_quad(GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black), GLVertex2D(vector2df(), black));

	// Call the update_mesh function to "build" us properly
	update_mesh(true, true);
}

bool ImageLayerManipulator::update(int mouse_x, int mouse_y)
{
	bool dirty = false;

	// Panning our perspective
	if (_moving_perspective)
		_layer_position += vector2df(static_cast<float>(mouse_x), static_cast<float>(mouse_y)) - _previous_mouse_position;

	// Keep our layers positioned correctly
	vector2df half_screen = get_size() * 0.5f;
	_layer_position.x = floor(std::max(std::min(_layer_position.x, _layer_size.x * _zoom * 0.5f), -_layer_size.x * _zoom * 0.5f));
	_layer_position.y = floor(std::max(std::min(_layer_position.y, _layer_size.y * _zoom * 0.5f), -_layer_size.y * _zoom * 0.5f));

	_layer_adjust.set(half_screen - (_layer_size * 0.5f * _zoom) + _layer_position);
	_layer_adjust.x = floor(_layer_adjust.x);
	_layer_adjust.y = floor(_layer_adjust.y);

	if (_layer_adjust != _layers[0]->get_position() || _previous_zoom != _zoom)
	{
		// We need to adjust
		for (auto& layer : _layers) {
			layer->set_position(_layer_adjust);
		}

		_pasted_image->set_position(active_layer()->get_position() + static_cast<vector2df>(_select_start) * _zoom);
		update_mesh(mouse_x > -1 && mouse_y > -1, true);
	}

	// If negative mouse position, this is likely a call from screen_resized and so we don't need to do any processing
	if (mouse_x > -1 && mouse_y > -1)
	{
		// Update our pixel position - the point on the current layer we're working on
		vector2df absolute_position(active_layer()->get_position());
		vector2df relative_mouse(static_cast<float>(mouse_x) - absolute_position.x, static_cast<float>(mouse_y) - absolute_position.y);
		vector2di previous_pixel(_pixel_position);
		_pixel_position.set(static_cast<int>(relative_mouse.x / _zoom), static_cast<int>(relative_mouse.y / _zoom));

		if (mouse_x < active_layer()->get_position().x)
			--_pixel_position.x;
		if (mouse_y < active_layer()->get_position().y)
			--_pixel_position.y;

		// Our pixel edit position is confined to the image space
		vector2di previous_edit_pixel(_pixel_edit_position);
		_pixel_edit_position.x = std::max(std::min(_pixel_position.x, static_cast<int>(_layer_size.x) - 1), 0);
		_pixel_edit_position.y = std::max(std::min(_pixel_position.y, static_cast<int>(_layer_size.y) - 1), 0);

		// Handle specific manipulation modes
		if (active_layer()->is_visible())
		{
			switch (_mode)
			{
			case ManipulationMode::paint:
			case ManipulationMode::erase:
				// We are free-drawing onto the layer
				if (_mouse_down && (_pixel_edit_position == _pixel_position || util::is_point_inside(previous_pixel, vector2di(0, 0), static_cast<vector2di>(_layer_size))))
				{
					// We can paint, check if we need to
					if (previous_edit_pixel != _pixel_edit_position || _just_clicked)
					{
						// Paint onto the layer
						std::vector<vector2di> points;
						util::get_pixel_line(previous_edit_pixel, _pixel_edit_position, points);
						GLColour erase_colour(0, 0);
						PaintState* paint_state = dynamic_cast<PaintState*>(_working_state);

						for (auto& pos : points) {
							if (_painted[static_cast<size_t>(pos.y) * static_cast<size_t>(_layer_size.x) + static_cast<size_t>(pos.x)])
								continue;

							// For undo state
							const GLColour col = active_layer()->get_pixel(pos.x, pos.y);
							if (paint_state)
							{
								paint_state->_positions.push_back(pos);
								paint_state->_original_colours.push_back(col);
							}

							if (_mode == ManipulationMode::erase)
								active_layer()->set_pixel(pos.x, pos.y, erase_colour);
							else
								active_layer()->set_pixel(pos.x, pos.y, util::blend_colours(col, _paint_colour));

							_painted[static_cast<size_t>(pos.y) * static_cast<size_t>(_layer_size.x) + static_cast<size_t>(pos.x)] = true;
						}

						active_layer()->buffer();

						dirty = true;
					}
				}
				break;
			case ManipulationMode::fill:
				if (_just_clicked && _pixel_edit_position == _pixel_position)
				{
					if (active_layer()->get_pixel(_pixel_edit_position.x, _pixel_edit_position.y) != _paint_colour)
					{
						fill();
						dirty = true;
					}
				}
				break;
			case ManipulationMode::select:
				if (_mouse_down)
				{
					if (_pasted_image->is_visible())
					{
						if (_paste_move_offset.x > -1 && _pixel_position != _select_start + _paste_move_offset)
						{
							_select_start = _pixel_position - _paste_move_offset;
							_select_end = _select_start;

							_pasted_image->set_position(active_layer()->get_position() + static_cast<vector2df>(_select_start) * _zoom);
						}
					}
					else
					{
						vector2di prev_select = _select_end;
						_select_end = _pixel_edit_position;

						if (prev_select != _select_end)
							dirty = true;
					}
				}
				break;
			default:
				// Do nothing
				break;
			}
		}

		// Update our mesh if we need to
		if (previous_pixel != _pixel_position)
		{
			update_mesh(_pixel_edit_position == _pixel_position || _mode == ManipulationMode::select, false);

			if (_mouse_position_callback)
				_mouse_position_callback(_pixel_edit_position);
		}

		_just_clicked = false;
	}

	// Set previous variables and call base class update
	_previous_zoom = _zoom;
	_previous_mouse_position.set(mouse_x, mouse_y);
	dirty |= GUIElement::update(mouse_x, mouse_y);
	return dirty;
}

void ImageLayerManipulator::add_layer()
{
	// The image layer is not our child as our position will change
	ImageLayer* layer = new ImageLayer(_layer_adjust, _layer_size);
	layer->set_depth(-1000000 + static_cast<int>(_layers.size()));
	layer->set_scale(vector2df(_zoom, _zoom));

	if (!_layers.empty())
		layer->clear_to_colour(GLColour(0, 0));

	_layers.push_back(layer);

	// If this isn't the call from the constructor then add a new element to the program manager
	ProgramManager::instance().add_gui_element_queued(layer);
}

void ImageLayerManipulator::remove_layer(size_t index)
{
	if (index == 0 && _layers.size() == 1)
	{
		// We always need a layer, just clear the bottom layer
		_layers[0]->clear_to_colour(GLColour(0, 0));
		update_transforms();
	}
	else if (index < _layers.size())
	{
		// Remove the given layer
		_layers[index]->kill();
		_layers.erase(std::find(_layers.begin(), _layers.end(), _layers[index]));

		// Need to adjust layer depth
		for (size_t i = index; i < _layers.size(); ++i) {
			_layers[i]->set_depth(_layers[i]->get_depth() - 1);
		}

		if (_active_layer > index || _active_layer >= _layers.size())
			--_active_layer;

		_layers.shrink_to_fit();
	}
}

void ImageLayerManipulator::set_active_layer(size_t index)
{
	_active_layer = index;
}

size_t ImageLayerManipulator::get_active_layer() const
{
	return _active_layer;
}

const ImageLayer* ImageLayerManipulator::get_layer(size_t index) const
{
	if (index < _layers.size())
		return _layers[index];

	return nullptr;
}

const std::vector<ImageLayer*>& ImageLayerManipulator::get_layers() const
{
	return _layers;
}

void ImageLayerManipulator::swap_layers(size_t one, size_t two)
{
	int diff = static_cast<int>(two) - static_cast<int>(one);
	std::swap(_layers[one], _layers[two]);
	_layers[one]->set_depth(_layers[one]->get_depth() - diff);
	_layers[two]->set_depth(_layers[one]->get_depth() + diff);
	ProgramManager::instance().sort_gui_elements();
}

void ImageLayerManipulator::move_layer(size_t original, size_t dest)
{
	// Move the item from layer to dest
	auto layer = _layers[original];
	int diff = static_cast<int>(dest) - static_cast<int>(original);
	if (dest > original)
	{
		for (size_t i = original; i < dest; ++i) {
			_layers[i] = _layers[i + 1];
			_layers[i]->set_depth(_layers[i]->get_depth() - 1);
		}
	}
	else
	{
		for (size_t i = original; i > dest; --i) {
			_layers[i] = _layers[i - 1];
			_layers[i]->set_depth(_layers[i]->get_depth() + 1);
		}
	}

	_layers[dest] = layer;
	_layers[dest]->set_depth(_layers[dest]->get_depth() + diff);
	ProgramManager::instance().sort_gui_elements();
}

void ImageLayerManipulator::set_layer_size(const vector2di& size, bool resize_layers, bool from_state_change)
{
	// If we're not resizing from an undo/redo, create a new state
	if (!from_state_change)
	{
		SizeState* size_state = new SizeState(this);
		size_state->_original = _layer_size;
		size_state->_new = size;

		// If we're shrinking, save the layer data
		if (size_state->_new.x < size_state->_original.x || size_state->_new.y < size_state->_original.y)
		{
			for (auto& layer : _layers) {
				size_state->_original_data.push_back(layer->get_texture()->get_data());
			}
		}

		EditStateManager::instance().add_state(size_state);
	}
	
	_layer_size = size;
	_painted.clear();
	_painted.resize(static_cast<size_t>(_layer_size.x) * static_cast<size_t>(_layer_size.y), false);
	_painted.shrink_to_fit();

	if (resize_layers)
	{
		for (auto& layer : _layers) {
			layer->set_size(_layer_size);
		}
	}

	if (_layer_size_changed_callback)
		_layer_size_changed_callback(size);
}

const vector2df& ImageLayerManipulator::get_layer_size() const
{
	return _layer_size;
}

void ImageLayerManipulator::set_layer_alpha(size_t index, float alpha)
{
	_layers[index]->set_alpha(alpha);
}

void ImageLayerManipulator::set_layer_visibility(size_t index, bool visible)
{
	_layers[index]->set_visible(visible);
}

void ImageLayerManipulator::load_from_buffer(const std::vector<GLColour>& data, int width, int height)
{
	_layers[_layers.size() - 1]->load_from_buffer(data, width, height);
	update_transforms();
}

void ImageLayerManipulator::load_from_buffer(const GLColour* data, int width, int height)
{
	_layers[_layers.size() - 1]->load_from_buffer(data, width, height);
	update_transforms();
}

float ImageLayerManipulator::get_zoom() const
{
	return _zoom;
}

void ImageLayerManipulator::set_paint_colour(const GLColour& col)
{
	_paint_colour = col;

	update_mesh(_pixel_edit_position == _pixel_position, false);
}

const GLColour& ImageLayerManipulator::get_paint_colour() const
{
	return _paint_colour;
}

void ImageLayerManipulator::get_complete_image_buffer(std::vector<GLColour>& data) const
{
	data.clear();
	data.shrink_to_fit();
	data.resize(static_cast<size_t>(_layer_size.x) * static_cast<size_t>(_layer_size.y), GLColour(0, 0));

	// Compile the image
	for (auto& layer : _layers) {
		if (!layer->is_visible())
			continue;

		const std::vector<GLColour>& layer_data = layer->get_texture()->get_data();
		for (int y = 0; y < static_cast<int>(_layer_size.y); ++y) {
			for (int x = 0; x < static_cast<int>(_layer_size.x); ++x) {
				GLColour pixel = layer_data[static_cast<size_t>(y) * static_cast<size_t>(_layer_size.x) + static_cast<size_t>(x)];
				pixel.a = static_cast<GLubyte>(pixel.a * layer->get_alpha());
				if (pixel.a > 0)
				{
					const GLColour current = data[static_cast<size_t>(y) * static_cast<size_t>(_layer_size.x) + static_cast<size_t>(x)];
					data[static_cast<size_t>(y) * static_cast<size_t>(_layer_size.x) + static_cast<size_t>(x)] = util::blend_colours(current, pixel);
				}
			}
		}
	}
}

void ImageLayerManipulator::get_working_hsv()
{
	vector2di start;
	vector2di size(_layer_size);
	_working_on_selection = (_select_end != _select_start);
	
	if (_working_on_selection)
	{
		start = _select_start;
		size = _select_end - _select_start + vector2di(1, 1);
		update_mesh(true, false);
	}

	// Start the EditState for undoing this change
	AreaState* area_state = new AreaState(this);
	area_state->_start_position = start;
	area_state->_size = size;

	// Get HSV of our pixels for colourising
	_hsv_pixels.clear();
	_hsv_pixels.shrink_to_fit();
	_hsv_pixels.resize(static_cast<size_t>(size.x) * static_cast<size_t>(size.y));

	for (int y = 0; y < size.y; ++y) {
		for (int x = 0; x < size.x; ++x) {
			const GLColour& col = active_layer()->get_pixel(start.x + x, start.y + y);
			_hsv_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)] = util::rgb_to_hsv(col);

			if (_working_on_selection)
				area_state->_original.push_back(col);
		}
	}


	if (!_working_on_selection)
		area_state->_original = active_layer()->get_texture()->get_data();

	_working_state = area_state;
}

void ImageLayerManipulator::hsv_shift(float hue_shift, float sat_shift, float val_shift)
{
	// Use multiple threads to HSV shift the image
	vector2di start;
	vector2df size(_layer_size);

	if (_working_on_selection)
	{
		start = _select_start;
		size = _select_end - _select_start + vector2di(1, 1);
	}

	float pixel_count = size.x * size.y;
	const float max_pixels_per_thread = std::max(256.f * 256.f, ceil(pixel_count / 10.f));
	size_t num_threads = static_cast<size_t>(ceil(pixel_count / max_pixels_per_thread));

	auto thread_func = [&, this](size_t begin, size_t count) {
		auto* layer = active_layer();
		size_t upper = std::min(begin + count, static_cast<size_t>(pixel_count));
		for (size_t i = begin; i < upper; ++i) {
			HSV hsv = _hsv_pixels[i];
			hsv.hue += hue_shift;
			hsv.sat += sat_shift;
			hsv.val += val_shift;

			if (hsv.hue < 0.f)
				hsv.hue += 360.f;
			else if (hsv.hue > 360.f)
				hsv.hue -= 360.f;

			hsv.sat = std::max(std::min(hsv.sat, 1.f), 0.f);
			hsv.val = std::max(std::min(hsv.val, 1.f), 0.f);

			int y = static_cast<int>(i) / static_cast<int>(size.x);
			int x = static_cast<int>(i) - (y * static_cast<int>(size.x));

			GLColour col(util::hsv_to_rgb(hsv));
			col.a = layer->get_pixel(start.x + x, start.y + y).a;
			layer->set_pixel(start.x + x, start.y + y, col);
		}
	};

	std::vector<std::thread> threads;
	for (size_t i = 0; i < num_threads; ++i) {
		threads.push_back(std::thread(thread_func, i * static_cast<size_t>(max_pixels_per_thread), static_cast<size_t>(max_pixels_per_thread)));
	}

	for (size_t i = 0; i < num_threads; ++i) {
		threads[i].join();
	}

	active_layer()->buffer();
}

void ImageLayerManipulator::reset_adjustments(bool finished)
{
	vector2di start;
	vector2di size(_layer_size);

	if (_working_on_selection)
	{
		start = _select_start;
		size = _select_end - _select_start + vector2di(1, 1);
	}
	
	auto* layer = active_layer();
	for (int y = 0; y < size.y; ++y) {
		for (int x = 0; x < size.x; ++x) {
			GLColour col(util::hsv_to_rgb(_hsv_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)]));
			col.a = layer->get_pixel(start.x + x, start.y + y).a;
			layer->set_pixel(start.x + x, start.y + y, col);
		}
	}
	layer->buffer();

	if (finished)
	{
		// Don't need the undo state as we never did do
		delete _working_state;
		_working_state = nullptr;

		_working_on_selection = false;
		update_mesh(true, false);
	}
}

void ImageLayerManipulator::finished_adjustments()
{
	AreaState* area_state = dynamic_cast<AreaState*>(_working_state);
	if (area_state)
	{
		if (_working_on_selection)
		{
			vector2di start = _select_start;
			vector2di size = _select_end - _select_start + vector2di(1, 1);

			for (int y = 0; y < size.y; ++y) {
				for (int x = 0; x < size.x; ++x) {
					area_state->_new.push_back(active_layer()->get_pixel(start.x + x, start.y + y));
				}
			}

			_working_on_selection = false;
			update_mesh(true, false);
		}
		else
			area_state->_new = active_layer()->get_texture()->get_data();

		EditStateManager::instance().add_state(area_state);
		_working_state = nullptr;
	}
}

void ImageLayerManipulator::copy_selection(bool cut)
{
	if (_mode == ManipulationMode::select && !_mouse_down && !_pasted_image->is_visible() && _select_end != _select_start)
	{
		// Copy the selection into our copied pixels vector
		_copied_pixels.clear();

		AreaState* area_state = nullptr;

		if (cut)
		{
			area_state = new AreaState(this);
			area_state->_size = _select_end - _select_start + vector2di(1, 1);
		}

		for (int y = _select_start.y; y <= _select_end.y; ++y) {
			for (int x = _select_start.x; x <= _select_end.x; ++x) {
				const GLColour& col = active_layer()->get_pixel(x, y);
				_copied_pixels.push_back(col);

				if (cut)
				{
					area_state->_original.push_back(col);
					area_state->_new.push_back(GLColour(0, 0));
					active_layer()->set_pixel(x, y, GLColour(0, 0));
				}
			}
		}
		_copied_pixels.shrink_to_fit();
		_copied_size = _select_end - _select_start + vector2di(1, 1);

		_select_end = _select_start;

		if (cut)
		{
			active_layer()->buffer();
			EditStateManager::instance().add_state(area_state);
		}
		update_mesh(true, false);
	}
}

void ImageLayerManipulator::paste_selection()
{
	if (!_mouse_down && !_copied_pixels.empty())
	{
		// Change us to selection mode
		if (_mode != ManipulationMode::select)
			set_mode(ManipulationMode::select);
		
		// Paste our current selection if we have one
		if (_pasted_image->is_visible())
			write_pasted();

		// Paste the selection onto a temporary image (so it can be moved and what not)
		_pasted_pixels->create_texture_from_buffer(_copied_pixels, _copied_size.x, _copied_size.y);
		_pasted_image->set_position(active_layer()->get_position() + static_cast<vector2df>(_select_start) * _zoom);
		_pasted_image->set_size(_copied_size);
		_pasted_image->set_visible();
	}
}

void ImageLayerManipulator::delete_selection()
{
	if (_mode == ManipulationMode::select && !_mouse_down)
	{
		// Either stop the current paste or delete the selection
		if (_pasted_image->is_visible())
		{
			_pasted_image->set_visible(false);
			update_mesh(true, false);
		}
		else if (_select_end != _select_start)
		{
			AreaState* area_state = new AreaState(this);
			area_state->_size = _select_end - _select_start + vector2di(1, 1);
			for (int y = _select_start.y; y <= _select_end.y; ++y) {
				for (int x = _select_start.x; x <= _select_end.x; ++x) {
					area_state->_original.push_back(active_layer()->get_pixel(x, y));
					area_state->_new.push_back(GLColour(0, 0));
					active_layer()->set_pixel(x, y, GLColour(0, 0));
				}
			}
			active_layer()->buffer();
			update_mesh(true, false);

			EditStateManager::instance().add_state(area_state);
		}
	}
}

void ImageLayerManipulator::flip(bool vertical)
{
	vector2di start;
	vector2di size(_layer_size);
	ImageLayer* layer = active_layer();
	bool working_on_pasted = _pasted_image->is_visible();

	if (working_on_pasted)
		size = _copied_size;
	else if (_select_end != _select_start)
	{
		start = _select_start;
		size = _select_end - _select_start + vector2di(1, 1);
	}

	auto copied_pixel = [&size, this](int x, int y) -> GLColour& {
		return _copied_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)];
	};

	if (vertical)
	{
		int count = static_cast<int>(floor(size.y / 2.f));
		for (int y = 0; y < count; ++y) {
			for (int x = 0; x < size.x; ++x) {
				if (working_on_pasted)
				{
					const GLColour col = copied_pixel(x, y);
					copied_pixel(x, y) = copied_pixel(x, size.y - 1 - y);
					copied_pixel(x, size.y - 1 - y) = col;
				}
				else
				{
					const GLColour col = layer->get_pixel(start.x + x, start.y + y);
					layer->set_pixel(start.x + x, start.y + y, layer->get_pixel(start.x + x, start.y + size.y - 1 - y));
					layer->set_pixel(start.x + x, start.y + size.y - 1 - y, col);
				}
			}
		}
	}
	else
	{
		int count = static_cast<int>(floor(size.x / 2.f));
		for (int y = 0; y < size.y; ++y) {
			for (int x = 0; x < count; ++x) {
				if (working_on_pasted)
				{
					const GLColour col = copied_pixel(x, y);
					copied_pixel(x, y) = copied_pixel(size.x - 1 - x, y);
					copied_pixel(size.x - 1 - x, y) = col;
				}
				else
				{
					const GLColour col = layer->get_pixel(start.x + x, start.y + y);
					layer->set_pixel(start.x + x, start.y + y, layer->get_pixel(start.x + size.x - 1 - x, start.y + y));
					layer->set_pixel(start.x + size.x - 1 - x, start.y + y, col);
				}
			}
		}
	}

	// If we were allowing undoing of flipping the pasted image (which we aren't), we'd need to force an update on the pasted image here
	// As we're not, the update of the menu closing will repaint the new pasted image
	if (working_on_pasted)
		_pasted_pixels->create_texture_from_buffer(_copied_pixels, _copied_size.x, _copied_size.y);
	else
	{
		layer->buffer();
		update_mesh(true, false);

		EditStateManager::instance().add_state(new FlipState(this, vertical));
	}
}

void ImageLayerManipulator::start_rotation()
{
	vector2df start;
	vector2df size(_layer_size);

	if (_select_end != _select_start)
	{
		size = _select_end - _select_start + vector2df(1.f, 1.f);
		_selection_expand = ceil(size.distance(size * 0.5f) - std::min(size.x * 0.5f, size.y * 0.5f));
		size += vector2df(_selection_expand, _selection_expand) * 2.f;
		start = _select_start - vector2df(_selection_expand, _selection_expand);
		_working_on_selection = true;

		_rotate_pixels.clear();
		_rotate_pixels.resize(static_cast<size_t>(size.x) * static_cast<size_t>(size.y), GLColour(0, 0));
		_rotate_pixels.shrink_to_fit();

		for (int y = static_cast<int>(_selection_expand); y < static_cast<int>(size.y - _selection_expand); ++y) {
			for (int x = static_cast<int>(_selection_expand); x < static_cast<int>(size.x - _selection_expand); ++x) {
				_rotate_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)] = active_layer()->get_pixel(static_cast<int>(start.x) + x, static_cast<int>(start.y) + y);
			}
		}
	}
	else
		_rotate_pixels = active_layer()->get_texture()->get_data();
	
	_rotate_image->set_size(size);
	_rotate_image->set_scale(vector2df(_zoom, _zoom));
	_rotate_image->set_position(active_layer()->get_position() + start * _zoom);
	_rotate_image->set_visible(true);
	_rotate_texture->create_texture_from_buffer(_rotate_pixels, static_cast<int>(size.x), static_cast<int>(size.y));
}

void ImageLayerManipulator::rotate(float degrees)
{
	float radians = -degrees * DEG_TO_RAD;
	float r_sin = sin(radians);
	float r_cos = cos(radians);

	vector2df start;
	vector2df size(_layer_size);

	if (_select_end != _select_start)
	{
		size = _select_end - _select_start + vector2df(1.f, 1.f);
		size += vector2df(_selection_expand, _selection_expand) * 2.f;
		start = _select_start - vector2df(_selection_expand, _selection_expand);
	}

	// If the size is odd, adjust the offset to stop weird rotation artifacts
	vector2df offset(0.5f, 0.5f);
	if (static_cast<int>(size.x + size.y) % 2 == 1)
	{
		if (size.x > size.y)
			offset.x = 1.f;
		else
			offset.y = 1.f;
	}

	vector2df midpoint(size.x * 0.5f - offset.x, size.y * 0.5f - offset.y);
	ImageLayer* layer = active_layer();
	std::fill(_rotate_pixels.begin(), _rotate_pixels.end(), GLColour(0, 0));

	float pixel_count = size.x * size.y;
	const float max_pixels_per_thread = std::max(256.f * 256.f, ceil(pixel_count / 10.f));
	size_t num_threads = static_cast<size_t>(ceil(pixel_count / max_pixels_per_thread));

	auto thread_func = [&, this](size_t begin, size_t count) {
		auto* layer = active_layer();
		size_t upper = std::min(begin + count, static_cast<size_t>(pixel_count));
		for (size_t i = begin; i < upper; ++i) {
			int y = static_cast<int>(i) / static_cast<int>(size.x);
			int x = static_cast<int>(i) - (y * static_cast<int>(size.x));

			float y_diff_cos = (y - midpoint.y) * r_cos;
			float y_diff_sin = (y - midpoint.y) * r_sin;

			float x_diff_cos = (x - midpoint.x) * r_cos;
			float x_diff_sin = (x - midpoint.x) * r_sin;

			vector2di pos(static_cast<int>(start.x + round(x_diff_cos - y_diff_sin + midpoint.x)), static_cast<int>(start.y + round(x_diff_sin + y_diff_cos + midpoint.y)));
			if ((_select_end != _select_start && pos.x >= _select_start.x && pos.x <= _select_end.x && pos.y >= _select_start.y && pos.y <= _select_end.y) ||
				(_select_end == _select_start && pos.x >= 0 && pos.x < static_cast<int>(size.x) && pos.y >= 0 && pos.y < static_cast<int>(size.y)))
				_rotate_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)] = layer->get_pixel(pos.x, pos.y);
		}
	};

	std::vector<std::thread> threads;
	for (size_t i = 0; i < num_threads; ++i) {
		threads.push_back(std::thread(thread_func, i * static_cast<size_t>(max_pixels_per_thread), static_cast<size_t>(max_pixels_per_thread)));
	}

	for (size_t i = 0; i < num_threads; ++i) {
		threads[i].join();
	}

	_rotate_texture->create_texture_from_buffer(_rotate_pixels, static_cast<int>(size.x), static_cast<int>(size.y));
	update_transforms();
}

void ImageLayerManipulator::finish_rotation(bool save)
{
	if (save)
	{
		vector2df start;
		vector2df size(_layer_size);
		AreaState* area_state = new AreaState(this);
		ImageLayer* layer = active_layer();

		if (_select_end != _select_start)
		{
			size = _select_end - _select_start + vector2df(1.f, 1.f);
			size += vector2df(_selection_expand, _selection_expand) * 2.f;
			start = _select_start - vector2df(_selection_expand, _selection_expand);

			for (int y = 0; y < size.y; ++y) {
				for (int x = 0; x < size.x; ++x) {
					vector2di pos(static_cast<int>(start.x) + x, static_cast<int>(start.y) + y);
					if (pos.x >= 0 && pos.x < static_cast<int>(_layer_size.x) && pos.y >= 0 && pos.y < static_cast<int>(_layer_size.y))
						area_state->_original.push_back(layer->get_pixel(pos.x, pos.y));
					else
						area_state->_original.push_back(GLColour(0, 0));
				}
			}
		}
		else
			area_state->_original = layer->get_texture()->get_data();
		
		area_state->_start_position = start;
		area_state->_size = size;
		
		if (_select_end != _select_start)
		{
			for (int y = 0; y < size.y; ++y) {
				for (int x = 0; x < size.x; ++x) {
					vector2di pos(static_cast<int>(start.x) + x, static_cast<int>(start.y) + y);

					if (pos.x < 0 || pos.x >= _layer_size.x || pos.y < 0 || pos.y >= _layer_size.y)
					{
						area_state->_new.push_back(GLColour(0, 0));
						continue;
					}

					bool in_selection = (pos.x >= _select_start.x && pos.x <= _select_end.x && pos.y >= _select_start.y && pos.y <= _select_end.y);
					if (in_selection)
						layer->set_pixel(pos.x, pos.y, GLColour(0, 0));

					GLColour col = util::blend_colours(layer->get_pixel(pos.x, pos.y), _rotate_pixels[static_cast<size_t>(y) * static_cast<size_t>(size.x) + static_cast<size_t>(x)]);

					layer->set_pixel(pos.x, pos.y, col);
					area_state->_new.push_back(col);
				}
			}
			layer->buffer();
		}
		else
		{
			layer->load_from_buffer(_rotate_pixels, static_cast<int>(_layer_size.x), static_cast<int>(_layer_size.y));
			area_state->_new = _rotate_pixels;
		}
		update_transforms();

		EditStateManager::instance().add_state(area_state);
	}

	_rotate_image->set_visible(false);
	_working_on_selection = false;
	update_mesh(true, false);
}

void ImageLayerManipulator::set_mode(ManipulationMode mode)
{
	if (mode != _mode)
	{
		if (_pasted_image->is_visible())
			write_pasted();
		
		_select_end = vector2di();
		_select_start = vector2di();
	}

	_mode = mode;

	update_mesh(true, false);

	if (_mode_changed_callback)
		_mode_changed_callback(_mode);
}

void ImageLayerManipulator::set_zoom_function_callback(const std::function<void(float)>& func)
{
	_zoom_callback = func;
}

void ImageLayerManipulator::set_colour_picked_function_callback(const std::function<void(const GLColour&)>& func)
{
	_colour_picked_callback = func;
}

void ImageLayerManipulator::set_layer_size_changed_function_callback(const std::function<void(const vector2di&)>& func)
{
	_layer_size_changed_callback = func;
}

void ImageLayerManipulator::set_mode_changed_function_callback(const std::function<void(ManipulationMode)>& func)
{
	_mode_changed_callback = func;
}

void ImageLayerManipulator::set_mouse_position_function_callback(const std::function<void(const vector2di&)>& func)
{
	_mouse_position_callback = func;
}

bool ImageLayerManipulator::mouse_event(int button, int action, int mods)
{
	switch (button)
	{
	case GLFW_MOUSE_BUTTON_LEFT:
		// We don't need to bother with whether we've hovered or not
		// We're below the rest of the GUI so mouse events will always be processed last
		_mouse_down = (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK);
		_just_clicked = _mouse_down;

		if (!_mouse_down)
		{
			std::fill(_painted.begin(), _painted.end(), false);

			if (_mode == ManipulationMode::select)
			{
				if (_pasted_image->is_visible() && !_pasted_image->is_mouse_hovered())
					write_pasted();
				
				if (_select_end == _select_start)
					update_mesh(true, false);
				else
				{
					vector2di temp = _select_start;
					_select_start.set(std::min(_select_start.x, _select_end.x), std::min(_select_start.y, _select_end.y));
					_select_end.set(std::max(temp.x, _select_end.x), std::max(temp.y, _select_end.y));
				}
			}
			else if ((_mode == ManipulationMode::paint || _mode == ManipulationMode::erase) && _working_state)
			{
				EditStateManager::instance().add_state(_working_state);
				_working_state = nullptr;
			}
		}
		else if (is_mouse_hovered())
		{
			if (_pasted_image->is_visible())
			{
				if (_pasted_image->is_mouse_hovered())
					_paste_move_offset = _pixel_position - _select_start;
				else
					_paste_move_offset = vector2di(-1, -1);
			}
			else
			{
				_select_start = _pixel_edit_position;

				if (_mode == ManipulationMode::paint || _mode == ManipulationMode::erase)
				{
					_working_state = new PaintState(this);
				}
			}
		}

		return true;
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		if ((action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK) && mods == GLFW_MOD_SHIFT)
		{
			if (_pixel_position == _pixel_edit_position)
			{
				// Pick the colour under the pixel, combining layers
				GLColour picked_colour(0, 0);
				for (auto& layer : _layers) {
					if (layer->is_visible())
					{
						GLColour layer_colour = layer->get_pixel(_pixel_edit_position.x, _pixel_edit_position.y);
						layer_colour.a = static_cast<GLubyte>(layer_colour.a * layer->get_alpha());
						picked_colour = util::blend_colours(picked_colour, layer_colour);
					}
				}

				if (_colour_picked_callback)
					_colour_picked_callback(picked_colour);
			}
		}
		else if ((action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK) && mods == GLFW_MOD_CONTROL)
		{
			if (_pixel_position == _pixel_edit_position)
			{
				// Pick the colour under the pixel, from only the active layer
				if (_colour_picked_callback)
					_colour_picked_callback(active_layer()->get_pixel(_pixel_edit_position.x, _pixel_edit_position.y));
			}
		}
		else
			_moving_perspective = (action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK);
		return true;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:

		break;
	default:
		break;
	}
	
	return false;
}

bool ImageLayerManipulator::scroll_event(double x, double y)
{
	// Zoom in and out
	int amount = static_cast<int>(y);

	static float zooms[] = { 1.f, 2.f, 4.f, 8.f, 16.f, 32.f, 64.f };
	_zoom_index = std::max(std::min(_zoom_index + amount, 6), 0);

	_zoom = zooms[_zoom_index];

	if (_zoom != _previous_zoom)
	{
		// Scale our image layers
		for (auto& layer : _layers) {
			layer->set_scale(vector2df(_zoom, _zoom));
		}
		_pasted_image->set_scale(vector2df(_zoom, _zoom));

		// Adjust the layer position so that we're zooming based on the mouse position
		if (_layers[0]->is_mouse_hovered())
		{
			vector2df previous_mouse_relative = (static_cast<vector2df>(_previous_mouse_position) - (_layer_adjust + (_layer_size * 0.5f * _previous_zoom))) / (_layer_size * (_previous_zoom * 0.5f));
			_layer_position -= previous_mouse_relative * (_layer_size * 0.5f) * (_zoom - _previous_zoom);
		}

		if (_zoom_callback)
			_zoom_callback(_zoom);
	}

	return true;
}

void ImageLayerManipulator::screen_resized(int width, int height)
{
	set_size(vector2df(static_cast<float>(width), static_cast<float>(height)));
}

bool ImageLayerManipulator::key_event(int key, int scancode, int action, int mods)
{
	if (action == GLFW_RELEASE && !_mouse_down)
	{
		if (key >= GLFW_KEY_1 && key <= GLFW_KEY_4)
		{
			set_mode(static_cast<ManipulationMode>(key - GLFW_KEY_1));
			return true;
		}
	}

	return false;
}

bool ImageLayerManipulator::set_mouse_cursor(size_t& cursor) const
{
	cursor = cursor::arrow;

	if (_mode == ManipulationMode::erase)
		cursor = cursor::erase;
	else if (_mode == ManipulationMode::fill)
		cursor = cursor::fill;
	else if (_mode == ManipulationMode::select)
		cursor = cursor::select;

	return true;
}

void ImageLayerManipulator::update_mesh(bool visible, bool update_border)
{
	// Update the mesh!
	Mesh& mesh = get_mesh();
	vector2df position = active_layer()->get_position() + static_cast<vector2df>(_pixel_position) * _zoom;
	float size = 0.3f;
	float inv_size = 1.f - size;

	if (visible)
	{
		// Pixel
		mesh.set_quad_position(0, position, position + vector2df(_zoom, 0.f), position + vector2df(_zoom, _zoom), position + vector2df(0.f, _zoom));
		mesh.set_quad_colour(0, (_mode == ManipulationMode::erase || _mode == ManipulationMode::select) ? GLColour(255, 0) : _paint_colour);
		mesh.set_quad_colour(1, _mode == ManipulationMode::select && _select_start != _select_end && !_working_on_selection ? GLColour(220, 140) : GLColour(0, 0));

		GLColour border_col = _working_on_selection ? GLColour(220, 200) : GLColour(0, 255);
		for (size_t i = 2; i < 10; ++i) {
			mesh.set_quad_colour(i, border_col);
		}

		// Pixel border
		if (_pasted_image->is_visible() || _working_on_selection)
		{
			position = _pasted_image->get_position();
			vector2df pasted_size = _pasted_image->get_size();

			if (_working_on_selection)
			{
				if (_rotate_image->is_visible())
				{
					pasted_size = _select_end - _select_start + vector2df(1.f, 1.f);
					pasted_size += vector2df(_selection_expand, _selection_expand) * 2.f;
					position = _rotate_image->get_position();
				}
				else
				{
					position = active_layer()->get_position() + static_cast<vector2df>(_select_start) * _zoom;
					pasted_size = static_cast<vector2df>(_select_end - _select_start) + vector2df(1.f, 1.f);
				}
			}

			float x_size = pasted_size.x * 0.3f;
			float inv_x_size = pasted_size.x - x_size;
			float y_size = pasted_size.y * 0.3f;
			float inv_y_size = pasted_size.y - y_size;

			mesh.set_quad_position(2, position + vector2df(-2.f, -2.f),
				position + vector2df(_zoom * x_size, -2.f),
				position + vector2df(_zoom * x_size, 0.f),
				position + vector2df(-2.f, 0.f));
			mesh.set_quad_position(3, position + vector2df(_zoom * inv_x_size, -2.f),
				position + vector2df(pasted_size.x * _zoom + 2.f, -2.f),
				position + vector2df(pasted_size.x * _zoom + 2.f, 0.f),
				position + vector2df(_zoom * inv_x_size, 0.f));
			mesh.set_quad_position(4, position + vector2df(-2.f, pasted_size.y * _zoom),
				position + vector2df(_zoom * x_size, pasted_size.y * _zoom),
				position + vector2df(_zoom * x_size, pasted_size.y * _zoom + 2.f),
				position + vector2df(-2.f, pasted_size.y * _zoom + 2.f));
			mesh.set_quad_position(5, position + vector2df(_zoom * inv_x_size, pasted_size.y * _zoom),
				position + vector2df(pasted_size.x * _zoom + 2.f, pasted_size.y * _zoom),
				position + vector2df(pasted_size.x * _zoom + 2.f, pasted_size.y * _zoom + 2.f),
				position + vector2df(_zoom * inv_x_size, pasted_size.y * _zoom + 2.f));

			mesh.set_quad_position(6, position + vector2df(-2.f, 0.f),
				position,
				position + vector2df(0.f, _zoom * y_size),
				position + vector2df(-2.f, _zoom * y_size));
			mesh.set_quad_position(7, position + vector2df(-2.f, _zoom * inv_y_size),
				position + vector2df(0.f, _zoom * inv_y_size),
				position + vector2df(0.f, pasted_size.y * _zoom),
				position + vector2df(-2.f, pasted_size.y * _zoom));
			mesh.set_quad_position(8, position + vector2df(pasted_size.x * _zoom, 0.f),
				position + vector2df(pasted_size.x * _zoom + 2.f, 0.f),
				position + vector2df(pasted_size.x * _zoom + 2.f, _zoom * y_size),
				position + vector2df(pasted_size.x * _zoom, _zoom * y_size));
			mesh.set_quad_position(9, position + vector2df(pasted_size.x * _zoom, _zoom * inv_y_size),
				position + vector2df(pasted_size.x * _zoom + 2.f, _zoom * inv_y_size),
				position + vector2df(pasted_size.x * _zoom + 2.f, pasted_size.y * _zoom),
				position + vector2df(pasted_size.x * _zoom, pasted_size.y * _zoom));
		}
		else
		{
			mesh.set_quad_position(2, position + vector2df(-2.f, -2.f), position + vector2df(_zoom * size, -2.f), position + vector2df(_zoom * size, 0.f), position + vector2df(-2.f, 0.f));
			mesh.set_quad_position(3, position + vector2df(_zoom * inv_size, -2.f), position + vector2df(_zoom + 2.f, -2.f), position + vector2df(_zoom + 2.f, 0.f), position + vector2df(_zoom * inv_size, 0.f));
			mesh.set_quad_position(4, position + vector2df(-2.f, _zoom), position + vector2df(_zoom * size, _zoom), position + vector2df(_zoom * size, _zoom + 2.f), position + vector2df(-2.f, _zoom + 2.f));
			mesh.set_quad_position(5, position + vector2df(_zoom * inv_size, _zoom), position + vector2df(_zoom + 2.f, _zoom), position + vector2df(_zoom + 2.f, _zoom + 2.f), position + vector2df(_zoom * inv_size, _zoom + 2.f));

			mesh.set_quad_position(6, position + vector2df(-2.f, 0.f), position, position + vector2df(0.f, _zoom * size), position + vector2df(-2.f, _zoom * size));
			mesh.set_quad_position(7, position + vector2df(-2.f, _zoom * inv_size), position + vector2df(0.f, _zoom * inv_size), position + vector2df(0.f, _zoom), position + vector2df(-2.f, _zoom));
			mesh.set_quad_position(8, position + vector2df(_zoom, 0.f), position + vector2df(_zoom + 2.f, 0.f), position + vector2df(_zoom + 2.f, _zoom * size), position + vector2df(_zoom, _zoom * size));
			mesh.set_quad_position(9, position + vector2df(_zoom, _zoom * inv_size), position + vector2df(_zoom + 2.f, _zoom * inv_size), position + vector2df(_zoom + 2.f, _zoom), position + vector2df(_zoom, _zoom));
		}

		if (_mode == ManipulationMode::select)
		{
			vector2df select_start(static_cast<float>(std::min(_select_start.x, _select_end.x)), static_cast<float>(std::min(_select_start.y, _select_end.y)));
			vector2df select_end(static_cast<float>(std::max(_select_start.x, _select_end.x)), static_cast<float>(std::max(_select_start.y, _select_end.y)));
			position = active_layer()->get_position();

			mesh.set_quad_position(1, position + select_start * _zoom,
				position + vector2df(select_end.x + 1.f, select_start.y) * _zoom,
				position + (select_end + vector2df(1.f, 1.f)) * _zoom,
				position + vector2df(select_start.x, select_end.y + 1.f) * _zoom);
		}
	}

	if (!visible && visible != _cursor_visible)
	{
		for (size_t i = 0; i < 10; ++i) {
			if (i == 1)
				continue;

			mesh.set_quad_colour(i, GLColour(255, 0));
		}
	}
	_cursor_visible = visible;

	if (update_border)
	{
		// Image border
		position = active_layer()->get_position();
		mesh.set_quad_position(10, position + vector2df(-1.f, -1.f), position + vector2df(_layer_size.x * _zoom + 1.f, -1.f), position + vector2df(_layer_size.x * _zoom + 2.f, 0.f), position + vector2df(-1.f, 0.f));
		mesh.set_quad_position(11, position + vector2df(-1.f, _layer_size.y * _zoom), position + vector2df(_layer_size.x * _zoom + 1.f, _layer_size.y * _zoom),
			position + vector2df(_layer_size.x * _zoom + 2.f, _layer_size.y * _zoom + 1.f), position + vector2df(-1.f, _layer_size.y * _zoom + 1.f));
		mesh.set_quad_position(12, position + vector2df(-1.f, 0.f), position, position + vector2df(0.f, _layer_size.y * _zoom), position + vector2df(-1.f, _layer_size.y * _zoom));
		mesh.set_quad_position(13, position + vector2df(_layer_size.x * _zoom, 0.f), position + vector2df(_layer_size.x * _zoom + 1.f, 0.f),
			position + vector2df(_layer_size.x * _zoom + 1.f, _layer_size.y * _zoom), position + vector2df(_layer_size.x * _zoom, _layer_size.y * _zoom));
	}
}

void ImageLayerManipulator::fill()
{
	GLColour colour_to_match(active_layer()->get_pixel(_pixel_edit_position.x, _pixel_edit_position.y));
	std::queue<vector2di> check_points;
	ImageLayer* current_layer = active_layer();
	std::vector<bool> done(static_cast<size_t>(_layer_size.x) * static_cast<size_t>(_layer_size.y), false);
	FillState* fill_state = new FillState(this);
	fill_state->_original_colour = colour_to_match;

	// Check the position and if the colour matches add it to the list
	auto check_position = [this, &check_points, &colour_to_match, &current_layer, &done](const vector2di& pos) {
		if (pos.x < 0 || pos.x >= static_cast<int>(_layer_size.x) || pos.y < 0 || pos.y >= static_cast<int>(_layer_size.y))
			return;

		//if (current_layer->get_pixel(pos.x, pos.y) == colour_to_match && std::find(check_points.begin(), check_points.end(), pos) == check_points.end())
		if (!done[pos.x * static_cast<size_t>(_layer_size.y) + pos.y] && current_layer->get_pixel(pos.x, pos.y) == colour_to_match)
		{
			check_points.push(pos);
			done[pos.x * static_cast<size_t>(_layer_size.y) + pos.y] = true;
		}
	};

	// For each point in the list, change the colour and check the four surrounding pixels
	check_points.push(_pixel_edit_position);
	while (!check_points.empty())
	{
		vector2di pos = check_points.front();
		check_points.pop();

		const GLColour col = current_layer->get_pixel(pos.x, pos.y);
		current_layer->set_pixel(pos.x, pos.y, util::blend_colours(col, _paint_colour));
		fill_state->_positions.push_back(pos);

		check_position(pos + vector2di(-1, 0));
		check_position(pos + vector2di(1, 0));
		check_position(pos + vector2di(0, -1));
		check_position(pos + vector2di(0, 1));
	}

	current_layer->buffer();
	EditStateManager::instance().add_state(fill_state);
}

void ImageLayerManipulator::write_pasted()
{
	// Paste our selection onto the active layer
	ImageLayer* current_layer = active_layer();
	AreaState* area_state = new AreaState(this);
	area_state->_size = _copied_size;

	for (int y = 0; y < _copied_size.y; ++y) {
		for (int x = 0; x < _copied_size.x; ++x) {
			vector2di pos = _select_start + vector2di(x, y);

			if (pos.x >= 0 && pos.x < _layer_size.x && pos.y >= 0 && pos.y < _layer_size.y)
			{
				GLColour original = current_layer->get_pixel(pos.x, pos.y);
				GLColour new_col = util::blend_colours(original, _copied_pixels[static_cast<size_t>(y) * static_cast<size_t>(_copied_size.x) + static_cast<size_t>(x)]);
				current_layer->set_pixel(pos.x, pos.y, new_col);

				area_state->_original.push_back(original);
				area_state->_new.push_back(new_col);
			}
			else
			{
				// Just need to push back into our state so the array doesn't go out of bounds when undoing/redoing
				area_state->_original.push_back(GLColour(0, 0));
				area_state->_new.push_back(GLColour(0, 0));
			}
		}
	}
	current_layer->buffer();
	EditStateManager::instance().add_state(area_state);

	// We don't need our preview anymore
	_pasted_image->set_visible(false);
}

// EditStates related to image data
PaintState::PaintState(ImageLayerManipulator* image)
	: EditState(image->_active_layer)
	, _image(image)
	, _positions()
	, _painted_colour(image->_paint_colour)
	, _original_colours()
	, _erasing(image->_mode == ManipulationMode::erase)
{

}

void PaintState::undo()
{
	for (size_t i = 0; i < _positions.size(); ++i) {
		_image->_layers[_layer_index]->set_pixel(_positions[i].x, _positions[i].y, _original_colours[i]);
	}
	_image->_layers[_layer_index]->buffer();
	_image->update_mesh(true, false);
}

void PaintState::redo()
{
	GLColour erase_colour(0, 0);
	for (size_t i = 0; i < _positions.size(); ++i) {
		if (_erasing)
			_image->_layers[_layer_index]->set_pixel(_positions[i].x, _positions[i].y, erase_colour);
		else
			_image->_layers[_layer_index]->set_pixel(_positions[i].x, _positions[i].y, util::blend_colours(_original_colours[i], _painted_colour));
	}
	_image->_layers[_layer_index]->buffer();
	_image->update_mesh(true, false);
}

bool PaintState::validate_state()
{
	GLColour erase_colour(0, 0);
	for (size_t i = 0; i < _positions.size(); ++i) {
		const GLColour& col = _original_colours[i];
		const GLColour blend = util::blend_colours(col, _painted_colour);
		if (_erasing && col != erase_colour)
			return true;
		else if (!_erasing && col != blend)
			return true;
	}

	return false;
}

FillState::FillState(ImageLayerManipulator* image)
	: EditState(image->_active_layer)
	, _image(image)
	, _positions()
	, _filled_colour(image->_paint_colour)
	, _original_colour()
{

}

void FillState::undo()
{
	fill(_original_colour);
}

void FillState::redo()
{
	fill(util::blend_colours(_original_colour, _filled_colour));
}

void FillState::fill(const GLColour& col)
{
	for (size_t i = 0; i < _positions.size(); ++i) {
		_image->_layers[_layer_index]->set_pixel(_positions[i].x, _positions[i].y, col);
	}
	_image->_layers[_layer_index]->buffer();
	_image->update_mesh(true, false);
}

bool FillState::validate_state()
{
	return _original_colour != util::blend_colours(_original_colour, _filled_colour);
}

AreaState::AreaState(ImageLayerManipulator* image)
	: EditState(image->_active_layer)
	, _image(image)
	, _start_position(image->_select_start)
	, _size()
	, _original()
	, _new()
{

}

void AreaState::undo()
{
	update_area(_original);
}

void AreaState::redo()
{
	update_area(_new);
}

void AreaState::update_area(const std::vector<GLColour>& data)
{
	for (int y = 0; y < _size.y; ++y) {
		for (int x = 0; x < _size.x; ++x) {
			vector2di pos = _start_position + vector2di(x, y);

			if (pos.x >= 0 && pos.x < _image->_layer_size.x && pos.y >= 0 && pos.y < _image->_layer_size.y)
				_image->_layers[_layer_index]->set_pixel(pos.x, pos.y, data[static_cast<size_t>(y) * static_cast<size_t>(_size.x) + static_cast<size_t>(x)]);
		}
	}
	_image->_layers[_layer_index]->buffer();
	_image->update_mesh(true, false);
}

bool AreaState::validate_state()
{
	return _original != _new;
}

SizeState::SizeState(ImageLayerManipulator* image)
	: EditState(image->_active_layer)
	, _image(image)
	, _original()
	, _new()
	, _original_data()
{

}

void SizeState::undo()
{
	_image->set_layer_size(_original, true, true);

	// If our new size is smaller than the original, we need to write the lost data back
	if (!_original_data.empty())
	{
		for (size_t i = 0; i < _original_data.size(); ++i) {
			_image->_layers[i]->load_from_buffer(_original_data[i], static_cast<int>(_image->_layer_size.x), static_cast<int>(_image->_layer_size.y));
		}
	}
}

void SizeState::redo()
{
	_image->set_layer_size(_new, true, true);
}

bool SizeState::validate_state()
{
	return _original != _new;
}

FlipState::FlipState(ImageLayerManipulator* image, bool vertical)
	: EditState(image->_active_layer)
	, _image(image)
	, _vertical(vertical)
	, _start()
	, _size(image->_layer_size)
{
	if (_image->_select_end != _image->_select_start)
	{
		_start = _image->_select_start;
		_size = _image->_select_end - _image->_select_start + vector2di(1, 1);
	}
}

void FlipState::undo()
{
	flip();
}

void FlipState::redo()
{
	flip();
}

bool FlipState::validate_state()
{
	return _size.x > 1 || _size.y > 1;
}

void FlipState::flip()
{
	ImageLayer* layer = _image->_layers[_layer_index];
	if (_vertical)
	{
		int count = static_cast<int>(floor(_size.y / 2.f));
		for (int y = 0; y < count; ++y) {
			for (int x = 0; x < _size.x; ++x) {
				const GLColour col = layer->get_pixel(_start.x + x, _start.y + y);
				layer->set_pixel(_start.x + x, _start.y + y, layer->get_pixel(_start.x + x, _start.y + _size.y - 1 - y));
				layer->set_pixel(_start.x + x, _start.y + _size.y - 1 - y, col);
			}
		}
	}
	else
	{
		int count = static_cast<int>(floor(_size.x / 2.f));
		for (int y = 0; y < _size.y; ++y) {
			for (int x = 0; x < count; ++x) {
				const GLColour col = layer->get_pixel(_start.x + x, _start.y + y);
				layer->set_pixel(_start.x + x, _start.y + y, layer->get_pixel(_start.x + _size.x - 1 - x, _start.y + y));
				layer->set_pixel(_start.x + _size.x - 1 - x, _start.y + y, col);
			}
		}
	}

	layer->buffer();
	_image->update_mesh(true, false);
}