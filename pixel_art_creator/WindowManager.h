#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

// We won't include GLBase.h when we have more structure in place but for now I just want to get the window showing and stuff
#include "ProgramManager.h"

class WindowManager {
public:
	WindowManager();
	~WindowManager();
	
	bool init();
	void run(const std::string& filename);

	void screen_resized();

	void mouse_event(int button, int action, int mods);
	void scroll_event(double x, double y);
	void key_event(int key, int scancode, int action, int mods);
	void char_event(unsigned int codepoint);

protected:

private:
	GLFWwindow* _window;
};

#endif