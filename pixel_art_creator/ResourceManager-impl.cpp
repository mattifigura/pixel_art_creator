#include "ResourceManager.cpp"
#include "Font.h"

template ResourceManager<Font>::ResourceManager(Font* (*func)(const std::string& key));
template ResourceManager<Font>::~ResourceManager();
template void ResourceManager<Font>::clear();
template const Font& ResourceManager<Font>::get_resource(const std::string& key);