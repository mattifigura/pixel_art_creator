#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <map>
#include <memory>
#include <string>

template<typename T>
class ResourceManager {
public:
	ResourceManager(T* (*func)(const std::string& key));
	~ResourceManager();

	void clear();

	const T& get_resource(const std::string& key);
	
protected:

private:
	struct StringCompare {
		bool operator()(const std::string& a, const std::string& b) const
		{
			const size_t al = a.length();
			const size_t bl = b.length();
			if (al != bl)
				return al < bl;

			return a < b;
		}
	};

	std::map<std::string, std::unique_ptr<T>, StringCompare> _resources;
	T* (*_creation_function)(const std::string& key);
};

#endif