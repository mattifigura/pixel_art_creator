#include "WindowPane.h"
#include "Button.h"
#include "FileManager.h"
#include "Util.h"

const vector2df border_size(2.f, 2.f);
const vector2df shadow_offset(5.f, 5.f);

std::unique_ptr<Texture> WindowPane::_close_texture(nullptr);

WindowPane::WindowPane(const vector2df& pos, const vector2df& size, const Font* font, const std::string& title)
	: GUIElement(pos, size, nullptr, true)
	, _title(new Text(vector2df(4.f, 4.f), size, title, font, this))
	, _close_button(new Button(vector2df(size.x - 23.f, 7.f), vector2df(16.f, 16.f), "X", font, this, true))
	, _keep_focus(false)
	, _moving(false)
	, _mouse_move_position()
	, _mouse_resize_offset()
	, _mouse_position()
	, _move_limits(0.f, 0.f, 1000000.f, 1000000.f)
	, _original_size(size)
	, _can_resize(true)
	, _resizing(false)
	, _resizing_left(false)
	, _resizing_right(false)
	, _resizing_top(false)
	, _resizing_bottom(false)
	, _close_function(nullptr)
{
	set_depth(1000);
	_title->set_shadowed(true, false);

	if (!_close_texture)
	{
		int width, height;
		std::vector<GLColour> data;
		if (FileManager::read_png(FileManager::_executable_directory + "media/close_button.png", data, width, height))
		{
			_close_texture.reset(new Texture());
			_close_texture->create_texture_from_buffer(data, width, height);
		}
	}
	
	if (_close_texture)
		_close_button->set_image_mode(true, _close_texture.get());

	_close_button->set_function([this](GUIElement* button, bool ignored) {
		set_visible(false);

		if (_close_function)
			_close_function();
	});
}

WindowPane::~WindowPane()
{

}

void WindowPane::build()
{
	Mesh& mesh = get_mesh();
	const vector2df& size = get_size();

	GLColour shadow_col(0, 50);
	mesh.add_quad(GLVertex2D(shadow_offset, shadow_col, vector2df()),
		GLVertex2D(shadow_offset + vector2df(size.x, 0.f), shadow_col, vector2df(1.f, 0.f)),
		GLVertex2D(shadow_offset + size, shadow_col, vector2df(1.f, 1.f)),
		GLVertex2D(shadow_offset + vector2df(0.f, size.y), shadow_col, vector2df(0.f, 1.f)));
	mesh.add_quad(GLVertex2D(shadow_offset * 1.5f, shadow_col, vector2df()),
		GLVertex2D(shadow_offset * 1.5f + vector2df(size.x, 0.f), shadow_col, vector2df(1.f, 0.f)),
		GLVertex2D(shadow_offset * 1.5f + size, shadow_col, vector2df(1.f, 1.f)),
		GLVertex2D(shadow_offset * 1.5f + vector2df(0.f, size.y), shadow_col, vector2df(0.f, 1.f)));

	mesh.add_quad(GLVertex2D(vector2df(), GLColour(200, 255), vector2df()),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(200, 255), vector2df(1.f, 0.f)),
		GLVertex2D(size, GLColour(200, 255), vector2df(1.f, 1.f)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(200, 255), vector2df(0.f, 1.f)));

	GLColour col(80, 200);
	const float title_height = _title->get_text_height() + 2.f;
	mesh.add_quad(GLVertex2D(border_size + vector2df(0.f, title_height), col, vector2df()),
		GLVertex2D(vector2df(size.x - border_size.x, border_size.y + title_height), col, vector2df(1.f, 0.f)),
		GLVertex2D(size - border_size, col, vector2df(1.f, 1.f)),
		GLVertex2D(vector2df(border_size.x, size.y - border_size.y), col, vector2df(0.f, 1.f)));
}

bool WindowPane::update(int mouse_x, int mouse_y)
{
	_mouse_position.set(static_cast<float>(mouse_x), static_cast<float>(mouse_y));

	bool updated = false;

	if (_resizing)
	{
		vector2df pos(get_position());
		vector2df bottom_right(pos + get_size());

		if (_resizing_left)
			pos.x = std::max(std::min(bottom_right.x - _original_size.x, _mouse_position.x + _mouse_resize_offset.x), _move_limits.x);
		if (_resizing_top)
			pos.y = std::max(std::min(bottom_right.y - _original_size.y, _mouse_position.y + _mouse_resize_offset.y), _move_limits.y);

		if (_resizing_right)
			bottom_right.x = std::min(std::max(pos.x + _original_size.x, _mouse_position.x + _mouse_resize_offset.x), _move_limits.z);
		if (_resizing_bottom)
			bottom_right.y = std::min(std::max(pos.y + _original_size.y, _mouse_position.y + _mouse_resize_offset.y), _move_limits.w);

		vector2df size(bottom_right - pos);

		if (size != get_size())
		{
			set_size(size);
			set_position(pos);
			updated = true;
		}
	}
	else if (_moving)
	{
		vector2df move_position = _mouse_position - _mouse_move_position;

		move_position.x = std::max(std::min(move_position.x, _move_limits.z - get_size().x), _move_limits.x);
		move_position.y = std::max(std::min(move_position.y, _move_limits.w - get_size().y), _move_limits.y);

		if (move_position != get_position())
		{
			set_position(move_position);
			updated = true;
		}
	}

	if (updated)
	{
		// As we are processed after our children, we need to call updates again to update their transformations
		for (auto& element : get_children()) {
			element->set_position(element->get_position());
			element->update_children(mouse_x, mouse_y);
		}
	}

	return GUIElement::update(mouse_x, mouse_y);
}

void WindowPane::set_move_limits(const vector4df& limits)
{
	_move_limits = limits;

	vector2df pos = get_position();
	vector2df size = get_size();

	size.x = std::min(size.x, _move_limits.z);
	size.y = std::min(size.y, _move_limits.w );

	pos.x = std::max(std::min(pos.x, _move_limits.z - size.x), _move_limits.x);
	pos.y = std::max(std::min(pos.y, _move_limits.w - size.y), _move_limits.y);

	set_position(pos);
	set_size(size);
}

void WindowPane::set_title(const std::string& title)
{
	_title->set_text(title);
}

const std::string& WindowPane::get_title() const
{
	return _title->get_text();
}

float WindowPane::get_titlebar_height() const
{
	return _title->get_text_height() + 2.f + border_size.y;
}

vector2df WindowPane::get_content_size() const
{
	return vector2df(get_size().x - (border_size.x * 2.f), get_size().y - (border_size.y * 2.f) - get_titlebar_height());
}

void WindowPane::set_keep_focus(bool focus)
{
	_keep_focus = focus;
}

void WindowPane::set_can_resize(bool resize)
{
	_can_resize = resize;
}

void WindowPane::set_close_button_function(const std::function<void()>& func)
{
	_close_function = func;
}

bool WindowPane::mouse_hover(bool entering)
{
	return _keep_focus;
}

bool WindowPane::mouse_event(int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if ((action == GLFW_PRESS || action == GLFW_DOUBLE_CLICK) && is_mouse_hovered())
		{
			if (_can_resize)
			{
				const vector2df& pos = get_position();
				const vector2df& size = get_size();
				const float grab_size(8.f);

				_resizing_left = util::is_point_inside(_mouse_position, pos, pos + vector2df(grab_size, size.y));
				_resizing_right = util::is_point_inside(_mouse_position, pos + vector2df(size.x - grab_size, 0.f), pos + size);
				_resizing_top = util::is_point_inside(_mouse_position, pos, pos + vector2df(size.x, grab_size));
				_resizing_bottom = util::is_point_inside(_mouse_position, pos + vector2df(0.f, size.y - grab_size), pos + size);

				_resizing = _resizing_left || _resizing_right || _resizing_top || _resizing_bottom;

				if (_resizing_left)
					_mouse_resize_offset.x = pos.x - _mouse_position.x;
				else if (_resizing_right)
					_mouse_resize_offset.x = pos.x + size.x - _mouse_position.x;

				if (_resizing_top)
					_mouse_resize_offset.y = pos.y - _mouse_position.y;
				else if (_resizing_bottom)
					_mouse_resize_offset.y = pos.y + size.y - _mouse_position.y;
			}
			
			_moving = !_resizing;
			_mouse_move_position = _mouse_position - get_position();
			return true;
		}
		else if (action == GLFW_RELEASE && (_moving || _resizing))
		{
			_moving = false;
			_resizing = false;
			return true;
		}
	}

	return _keep_focus;
}

bool WindowPane::scroll_event(double x, double y)
{
	return _keep_focus;
}

bool WindowPane::key_event(int key, int scancode, int action, int mods)
{
	return _keep_focus;
}

bool WindowPane::char_event(unsigned int codepoint)
{
	return _keep_focus;
}

bool WindowPane::set_mouse_cursor(size_t& cursor) const
{
	if (is_mouse_hovered())
	{
		const vector2df& pos = get_position();
		const vector2df& size = get_size();
		const float grab_size(8.f);
		
		// We have custom cursors now so can set corner resize images
		bool left_hovered = util::is_point_inside(_mouse_position, pos, pos + vector2df(grab_size, size.y));
		bool right_hovered = util::is_point_inside(_mouse_position, pos + vector2df(size.x - grab_size, 0.f), pos + size);
		bool top_hovered = util::is_point_inside(_mouse_position, pos, pos + vector2df(size.x, grab_size));
		bool bottom_hovered = util::is_point_inside(_mouse_position, pos + vector2df(0.f, size.y - grab_size), pos + size);

		if (_can_resize && left_hovered)
		{
			cursor = top_hovered ? cursor::tlbrresize : bottom_hovered ? cursor::trblresize : cursor::hresize;
			return true;
		}
		else if (_can_resize && right_hovered)
		{
			cursor = top_hovered ? cursor::trblresize : bottom_hovered ? cursor::tlbrresize : cursor::hresize;
			return true;
		}
		else if (_can_resize && (top_hovered || bottom_hovered))
		{
			cursor = cursor::vresize;
			return true;
		}
		else
		{
			cursor = cursor::arrow;
			return true;
		}
	}

	cursor = cursor::arrow;
	return _keep_focus;
}

void WindowPane::update_mesh()
{
	Mesh& mesh = get_mesh();
	const vector2df& size = get_size();
	const float title_height = _title->get_text_height() + 2.f;

	_close_button->set_position(vector2df(size.x - 23.f, 7.f));

	mesh.set_quad_position(0, shadow_offset,
		shadow_offset + vector2df(size.x, 0.f),
		shadow_offset + size,
		shadow_offset + vector2df(0.f, size.y));
	mesh.set_quad_position(1, shadow_offset * 1.5f,
		shadow_offset * 1.5f + vector2df(size.x, 0.f),
		shadow_offset * 1.5f + size,
		shadow_offset * 1.5f + vector2df(0.f, size.y));
	mesh.set_quad_position(2, vector2df(),
		vector2df(size.x, 0.f),
		size,
		vector2df(0.f, size.y));
	mesh.set_quad_position(3, border_size + vector2df(0.f, title_height),
		vector2df(size.x - border_size.x, border_size.y + title_height),
		size - border_size,
		vector2df(border_size.x, size.y - border_size.y));
}