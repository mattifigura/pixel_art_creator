#include "Util.h"

#include <algorithm>
#include <fstream>
#include <regex>

namespace util {
	// Fills in a line between two points
	// We needed this to paint properly without breaks in the line
	void get_pixel_line(const vector2di& start, const vector2di& end, std::vector<vector2di>& points)
	{
		// Bresenham's line algorithm
		const bool steep = abs(end.y - start.y) > abs(end.x - start.x);
		vector2di one = steep ? vector2di(start.y, start.x) : start;
		vector2di two = steep ? vector2di(end.y, end.x) : end;

		if (one.x > two.x)
		{
			vector2di temp = two;
			two = one;
			one = temp;
		}

		const float dx = static_cast<float>(two.x) - static_cast<float>(one.x);
		const float dy = abs(static_cast<float>(two.y) - static_cast<float>(one.y));
		float error = dx / 2.f;
		const int y_step = one.y < two.y ? 1 : -1;
		int y = one.y;

		for (int x = one.x; x <= two.x; ++x) {
			if (steep)
				points.push_back(vector2di(y, x));
			else
				points.push_back(vector2di(x, y));

			error -= dy;
			if (error < 0)
			{
				y += y_step;
				error += dx;
			}
		}
	}

	// Mix two colours together at a ratio
	GLColour mix_colours(const GLColour& one, const GLColour& two, float ratio)
	{
		ratio = std::max(std::min(ratio, 1.f), 0.f);
		float inv_ratio = 1.f - ratio;

		return GLColour(static_cast<GLubyte>(one.r * inv_ratio + two.r * ratio),
			static_cast<GLubyte>(one.g * inv_ratio + two.g * ratio),
			static_cast<GLubyte>(one.b * inv_ratio + two.b * ratio),
			static_cast<GLubyte>(one.a * inv_ratio + two.a * ratio));
	}

	// Blend two colours together
	GLColour blend_colours(const GLColour& one, const GLColour& two)
	{
		vector4df new_colour;
		vector4df foreground(static_cast<float>(two.r) / 255.f, static_cast<float>(two.g) / 255.f, static_cast<float>(two.b) / 255.f, static_cast<float>(two.a) / 255.f);
		vector4df background(static_cast<float>(one.r) / 255.f, static_cast<float>(one.g) / 255.f, static_cast<float>(one.b) / 255.f, static_cast<float>(one.a) / 255.f);
		new_colour.w = 1.f - (1.f - foreground.w) * (1.f - background.w);

		if (new_colour.w <= 0.f)
			return GLColour(0, 0);

		new_colour.x = (foreground.x * foreground.w / new_colour.w) + (background.x * background.w * (1.f - foreground.w) / new_colour.w);
		new_colour.y = (foreground.y * foreground.w / new_colour.w) + (background.y * background.w * (1.f - foreground.w) / new_colour.w);
		new_colour.z = (foreground.z * foreground.w / new_colour.w) + (background.z * background.w * (1.f - foreground.w) / new_colour.w);

		return GLColour(static_cast<GLubyte>(new_colour.x * 255.f), static_cast<GLubyte>(new_colour.y * 255.f), static_cast<GLubyte>(new_colour.z * 255.f), static_cast<GLubyte>(new_colour.w * 255.f));
	}

	// Is the point inside the given square
	template<typename T>
	bool is_point_inside(const T& point, const T& one, const T& two)
	{
		return ((point.x >= one.x && point.x <= two.x) && (point.y >= one.y && point.y <= two.y));
	}

	// Convert RGB to HSV
	HSV rgb_to_hsv(const GLColour& col)
	{
		HSV out;
		float red = static_cast<float>(col.r) / 255.f;
		float green = static_cast<float>(col.g) / 255.f;
		float blue = static_cast<float>(col.b) / 255.f;

		float min = std::min({ red, green, blue });
		float max = std::max({ red, green, blue });

		out.val = max;
		float delta = max  - min;

		// This is a grey / white / black where r = g = b (roughly)
		if (delta < 0.001)
		{
			out.hue = 0.f;
			out.sat = 0.f;
			return out;
		}

		if (max > 0.f)
			out.sat = delta / max;
		else
		{
			// This is another grey colour
			out.hue = 0.f;
			out.sat = 0.f;
			return out;
		}

		// Note we need to use >= to stop compiler complaints about ==
		if (red >= max)
			out.hue = (green - blue) / delta;
		else if (green >= max)
			out.hue = 2.f + (blue - red) / delta;
		else
			out.hue = 4.f + (red - green) / delta;

		out.hue *= 60.f;

		if (out.hue < 0.f)
			out.hue += 360.f;

		return out;
	}

	// Convert HSV to RGB
	GLColour hsv_to_rgb(const HSV& col)
	{
		GLColour out;

		if (col.sat <= 0.f)
		{
			out.set(static_cast<GLubyte>(col.val * 255.f), static_cast<GLubyte>(col.val * 255.f), static_cast<GLubyte>(col.val * 255.f), 255);
			return out;
		}

		float hue = col.hue;
		if (hue >= 360.f)
			hue = 0.f;
		hue /= 60.f;

		int i = static_cast<int>(hue);
		float value = hue - static_cast<float>(i);
		float x = col.val * (1.f - col.sat);
		float y = col.val * (1.f - (col.sat * value));
		float z = col.val * (1.f - (col.sat * (1.f - value)));

		switch (i)
		{
		case 0:
			out.r = static_cast<GLubyte>(col.val * 255.f);
			out.g = static_cast<GLubyte>(z * 255.f);
			out.b = static_cast<GLubyte>(x * 255.f);
			break;
		case 1:
			out.r = static_cast<GLubyte>(y * 255.f);
			out.g = static_cast<GLubyte>(col.val * 255.f);
			out.b = static_cast<GLubyte>(x * 255.f);
			break;
		case 2:
			out.r = static_cast<GLubyte>(x * 255.f);
			out.g = static_cast<GLubyte>(col.val * 255.f);
			out.b = static_cast<GLubyte>(z * 255.f);
			break;
		case 3:
			out.r = static_cast<GLubyte>(x * 255.f);
			out.g = static_cast<GLubyte>(y * 255.f);
			out.b = static_cast<GLubyte>(col.val * 255.f);
			break;
		case 4:
			out.r = static_cast<GLubyte>(z * 255.f);
			out.g = static_cast<GLubyte>(x * 255.f);
			out.b = static_cast<GLubyte>(col.val * 255.f);
			break;
		case 5:
		default:
			out.r = static_cast<GLubyte>(col.val * 255.f);
			out.g = static_cast<GLubyte>(x * 255.f);
			out.b = static_cast<GLubyte>(y * 255.f);
			break;
		}

		return out;
	}

	// Split word by regex
	std::vector<std::string> split_by_regex(const std::string& in, const std::string& regex_string)
	{
		std::regex regex(regex_string);
		std::sregex_token_iterator it{ in.begin(), in.end(), regex, -1 };
		return { it, {} };
	}

	// Read the given file into a string
	bool file_to_string(const std::string& filename, std::string& out)
	{
		std::ifstream file(filename, std::ios::in);
		if (file.is_open())
		{
			// Read lines and append to out
			std::string line;
			while (std::getline(file, line))
			{
				out.append(line).append("\n");
			}

			file.close();
			return true;
		}

		// Couldn't read the file
		return false;
	}

	// Remove trailing 0's
	void remove_trailing_zeroes(std::string& in)
	{
		in.erase(in.find_last_not_of('0') + 1, std::string::npos);
		if (in[in.length() - 1] == '.')
			in += "0";
	}

	// Float to string
	std::string float_to_string(float in)
	{
		std::string out(std::to_string(in));
		remove_trailing_zeroes(out);
		return out;
	}
}

template bool util::is_point_inside(const vector2df& point, const vector2df& one, const vector2df& two);
template bool util::is_point_inside(const vector2di& point, const vector2di& one, const vector2di& two);