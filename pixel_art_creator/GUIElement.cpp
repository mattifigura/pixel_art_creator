#include "GUIElement.h"
#include "ProgramManager.h"
#include "Util.h"

#include <climits>

GUIElement::GUIElement(const vector2df& pos, const vector2df& size, GUIElement* parent, bool force_mesh)
	: _parent(parent)
	, _mesh(nullptr)
	, _texture(nullptr)
	, _mesh_start(0)
	, _depth(0)
	, _visible(true)
	, _alive(true)
	, _alpha(1.f)
	, _priority(false)
	, _position(pos)
	, _size(size)
	, _scale(1.f, 1.f)
	, _cull_box()
	, _cursor(cursor::arrow)
	, _update_transform(true)
	, _transform()
	, _mouse_hover(false)
	, _mouse_block{false, false, false, false}
	, _children()
{
	// We own a mesh if we have no parent, otherwise our parent owns our mesh
	if (!_parent or force_mesh)
		_mesh.reset(new Mesh);

	if (_parent)
		_parent->add_child(this);
}

GUIElement::~GUIElement()
{
	
}

bool GUIElement::update(int mouse_x, int mouse_y)
{
	// Handle mouse hovering event
	bool hovered = is_point_inside(static_cast<float>(mouse_x), static_cast<float>(mouse_y));
	
	if (hovered != _mouse_hover)
		mouse_hover(hovered);

	_mouse_hover = hovered;

	// Check if our mesh needs creating/updating
	if (_mesh && _mesh->is_dirty())
	{
		if (VALID_BUFFER(_mesh->get_vbo()))
			_mesh->update_vbo();
		else
			_mesh->buffer();
		
		if (!_update_transform)
			return true;
	}

	// Check if our transform needs updating
	if (_update_transform)
	{
		_transform.identity();

		// If we have a parent but have our own mesh, our transform needs to be our absolute transform
		if (_parent && _mesh)
		{
			_transform.translate(get_absolute_position());
			_transform.scale(get_absolute_scale());
		}
		else
		{
			_transform.translate(_position);
			_transform.scale(_scale);
		}

		_update_transform = false;

		return true;
	}

	// Returning false means we do not need to be redrawn
	return false;
}

void GUIElement::update_children(int mouse_x, int mouse_y)
{
	update(mouse_x, mouse_y);
	
	for (auto& element : _children) {
		element->update_children(mouse_x, mouse_y);
	}
}

const Mesh* GUIElement::get_mesh_ptr() const
{
	return _mesh.get();
}

void GUIElement::force_mesh()
{
	if (!_mesh)
		_mesh.reset(new Mesh);
}

void GUIElement::delete_mesh()
{
	_mesh.reset(nullptr);
}

GUIElement* GUIElement::get_parent() const
{
	return _parent;
}

const std::vector<GUIElement*>& GUIElement::get_children() const
{
	return _children;
}

void GUIElement::set_depth(int depth)
{
	_depth = depth;
}

int GUIElement::get_depth() const
{
	// If we have priority, return a really big number, with room for child elements to draw over us correctly (and technically be processed first)
	if (_priority)
		return INT_MAX - 1000000;
	
	if (_parent)
		return _parent->get_depth() + _depth + 1;

	return _depth;
}

void GUIElement::set_priority(bool priority)
{
	_priority = priority;

	ProgramManager::instance().sort_gui_elements();
}

bool GUIElement::has_priority() const
{
	return _priority;
}

const bool GUIElement::operator<(const GUIElement& element) const
{
	return (element.get_depth() < get_depth());
}

void GUIElement::set_position(const vector2df& pos)
{
	_position = pos;
	update_transforms();

	if (!_mesh)
	{
		update_mesh();
	}
}

const vector2df& GUIElement::get_position() const
{
	return _position;
}

vector2df GUIElement::get_absolute_position() const
{
	vector2df pos(_position);
	
	if (_parent)
		pos += _parent->get_absolute_position();
	
	return pos;
}

void GUIElement::set_size(const vector2df& size)
{
	if (size != _size)
	{
		_size = size;
		update_mesh();
	}
}

const vector2df& GUIElement::get_size() const
{
	return _size;
}

void GUIElement::set_scale(const vector2df& scale)
{
	_scale = scale;
	update_transforms();
}

const vector2df& GUIElement::get_scale() const
{
	return _scale;
}

vector2df GUIElement::get_absolute_scale() const
{
	vector2df scale(_scale);

	if (_parent)
		scale *= _parent->get_absolute_scale();

	return scale;
}

void GUIElement::set_cull_box(const vector4df& cull)
{
	_cull_box = cull;
}

bool GUIElement::get_cull_box(vector4df& out) const
{
	bool has_cull_box = _cull_box.x < _cull_box.z && _cull_box.y < _cull_box.w;

	vector4df parent_cull;
	if (_parent && _parent->get_cull_box(parent_cull))
	{
		// Calculate the cull box based on ours and our parent's
		if (!has_cull_box)
			out = parent_cull;
		else
			out.set(std::max(_cull_box.x, parent_cull.x),
				std::max(_cull_box.y, parent_cull.y),
				std::min(_cull_box.z, parent_cull.z),
				std::min(_cull_box.w, parent_cull.w));

		has_cull_box = true;
	}
	else if (has_cull_box)
		out = _cull_box;

	return has_cull_box;
}

const matrix4f& GUIElement::get_transform() const
{
	return _transform;
}

void GUIElement::set_texture(const Texture* tex)
{
	_texture = tex;
}

const Texture* GUIElement::get_texture() const
{
	return _texture;
}

void GUIElement::set_visible(bool visible)
{
	// If we force a transform update it will force a redraw
	if (visible != _visible)
		_update_transform = true;
	
	_visible = visible;

	for (auto& element : _children) {
		element->set_visible(visible);
	}
}

bool GUIElement::is_visible() const
{
	bool visible = _visible;

	if (_parent)
		visible &= _parent->is_visible();

	return visible;
}

void GUIElement::kill()
{
	_alive = false;

	for (auto& element : _children) {
		element->kill();
	}
}

bool GUIElement::is_alive() const
{
	return _alive;
}

void GUIElement::set_alpha(float alpha)
{
	// Note alpha currently only works for elements with their own mesh
	// Might want to look into updating quad alphas in the future, but only really need alpha for layers so isn't important at the moment
	_alpha = std::max(std::min(alpha, 1.f), 0.f);
}

float GUIElement::get_alpha() const
{
	return _alpha;
}

void GUIElement::set_mouse_block(int action, bool block)
{
	_mouse_block[action] = block;
}

const bool GUIElement::is_point_inside(float x, float y) const
{
	bool inside = false;
	vector2df pos(get_absolute_position());
	inside = util::is_point_inside(vector2df(x, y), pos, pos + (_size * _scale));

	// Check our cull box, as our position could be outside the cull box and so we're not actually visible
	vector4df temp;
	if (get_cull_box(temp))
		inside &= util::is_point_inside(vector2df(x, y), vector2df(temp.x, temp.y), vector2df(temp.z, temp.w));

	return inside;
}

bool GUIElement::is_mouse_hovered() const
{
	return _mouse_hover;
}

bool GUIElement::mouse_hover(bool entering)
{
	return false;
}

bool GUIElement::mouse_event(int button, int action, int mods)
{
	return (_mouse_hover && _mouse_block[action]);
}

bool GUIElement::scroll_event(double x, double y)
{
	return false;
}

void GUIElement::screen_resized(int width, int height)
{
	// Does nothing in the base class
}

bool GUIElement::key_event(int key, int scancode, int action, int mods)
{
	return false;
}

bool GUIElement::char_event(unsigned int codepoint)
{
	return false;
}

bool GUIElement::set_mouse_cursor(size_t& cursor) const
{
	if (is_mouse_hovered())
	{
		cursor = _cursor;
		return true;
	}

	return false;
}

Mesh& GUIElement::get_mesh()
{
	if (_parent && !_mesh)
		return _parent->get_mesh();
	
	return *_mesh;
}

void GUIElement::add_child(GUIElement* element)
{
	_children.push_back(element);
}

void GUIElement::remove_child(GUIElement* element)
{
	auto found = std::find(_children.begin(), _children.end(), element);
	if (found != _children.end())
		_children.erase(found);
}

void GUIElement::update_mesh()
{
	// Does nothing here
}

void GUIElement::update_transforms()
{
	_update_transform = true;

	for (auto& element : _children) {
		element->update_transforms();
	}
}
