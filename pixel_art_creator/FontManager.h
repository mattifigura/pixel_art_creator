#ifndef FONT_MANAGER_H
#define FONT_MANAGER_H

#include "Font.h"
#include "ResourceManager.h"

class FontManager : public ResourceManager<Font> {
public:
	FontManager(FontManager const&) = delete;
	void operator=(FontManager const&) = delete;

	static FontManager& instance()
	{
		static FontManager singleton;
		return singleton;
	}

protected:

private:
	FontManager() : ResourceManager<Font>([](const std::string& key) { return new Font(key); }) {}
};

#endif