#include "Mesh.h"

Mesh::Mesh()
	: _vbo(0)
	, _ebo(0)
	, _vertices()
	, _indices()
	, _dirty(true)
{

}

Mesh::~Mesh()
{
	clear_data();
}

GLuint Mesh::get_vbo() const
{
	return _vbo;
}

GLuint Mesh::get_ebo() const
{
	return _ebo;
}

GLuint Mesh::get_index_buffer_size() const
{
	return static_cast<GLuint>(_indices.size());
}

bool Mesh::buffer()
{
	// Clear existing buffers
	clear_buffers();

	// Generate new buffers and buffer the data
	if (!_vertices.empty() && !_indices.empty())
	{
		glGenBuffers(1, &_vbo);
		if (VALID_BUFFER(_vbo))
		{
			glBindBuffer(GL_ARRAY_BUFFER, _vbo);
			glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(GLVertex2D), &_vertices[0], GL_STATIC_DRAW);

			glGenBuffers(1, &_ebo);
			if (VALID_BUFFER(_ebo))
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(GLuint), &_indices[0], GL_STATIC_DRAW);

				glBindBuffer(GL_ARRAY_BUFFER, 0); // Do we need this?
				_dirty = false;
				return true;
			}
		}
	}
	else
		_dirty = false;

	// If we reach here we have failed to buffer data
	clear_buffers();
	return false;
}

bool Mesh::update_vbo()
{
	if (VALID_BUFFER(_vbo))
	{
		// Update our vbo with new data
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, _vertices.size() * sizeof(GLVertex2D), &_vertices[0]);
		glBindBuffer(GL_ARRAY_BUFFER, 0); // Do we need this?

		_dirty = false;
		return true;
	}

	return false;
}

bool Mesh::is_dirty()
{
	return _dirty;
}

void Mesh::clear_data()
{
	clear_buffers();

	_vertices.clear();
	_indices.clear();

	_vertices.shrink_to_fit();
	_indices.shrink_to_fit();
}

// Add a quad and return the quad number in this mesh
size_t Mesh::add_quad(const GLVertex2D& one, const GLVertex2D& two, const GLVertex2D& three, const GLVertex2D& four, bool flip)
{
	GLuint index = static_cast<GLuint>(_vertices.size());

	_vertices.push_back(one);
	_vertices.push_back(two);
	_vertices.push_back(three);
	_vertices.push_back(four);

	// If we are flipping we'll need to do the indices differently
	if (flip)
	{
		_indices.push_back(index + 1); _indices.push_back(index + 2); _indices.push_back(index + 3);
		_indices.push_back(index + 3); _indices.push_back(index + 0); _indices.push_back(index + 1);
	}
	else
	{
		_indices.push_back(index + 0); _indices.push_back(index + 1); _indices.push_back(index + 2);
		_indices.push_back(index + 2); _indices.push_back(index + 3); _indices.push_back(index + 0);
	}

	_dirty = true;

	return static_cast<size_t>(index) / 4;
}

void Mesh::set_quad_colour(size_t quad, const GLColour& colour)
{
	size_t index = quad * 4;

	_vertices[index].col = colour;
	_vertices[index + 1].col = colour;
	_vertices[index + 2].col = colour;
	_vertices[index + 3].col = colour;

	_dirty = true;
}

void Mesh::set_quad_colour(size_t quad, const GLColour& colour1, const GLColour& colour2, const GLColour& colour3, const GLColour& colour4)
{
	size_t index = quad * 4;

	_vertices[index].col = colour1;
	_vertices[index + 1].col = colour2;
	_vertices[index + 2].col = colour3;
	_vertices[index + 3].col = colour4;

	_dirty = true;
}

void Mesh::set_quad_position(size_t quad, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4)
{
	size_t index = quad * 4;

	_vertices[index].pos = pos1;
	_vertices[index + 1].pos = pos2;
	_vertices[index + 2].pos = pos3;
	_vertices[index + 3].pos = pos4;

	_dirty = true;
}

void Mesh::translate_quad_position(size_t quad, const vector2df& translate)
{
	size_t index = quad * 4;

	_vertices[index].pos += translate;
	_vertices[index + 1].pos += translate;
	_vertices[index + 2].pos += translate;
	_vertices[index + 3].pos += translate;

	_dirty = true;
}

void Mesh::set_quad_uv(size_t quad, const vector2df& uv1, const vector2df& uv2, const vector2df& uv3, const vector2df& uv4)
{
	size_t index = quad * 4;

	_vertices[index].uv = uv1;
	_vertices[index + 1].uv = uv2;
	_vertices[index + 2].uv = uv3;
	_vertices[index + 3].uv = uv4;

	_dirty = true;
}

void Mesh::set_quad_uv(size_t quad, const vector4df& uv)
{
	size_t index = quad * 4;

	_vertices[index].uv = vector2df(uv.x, uv.y);
	_vertices[index + 1].uv = vector2df(uv.z, uv.y);
	_vertices[index + 2].uv = vector2df(uv.z, uv.w);
	_vertices[index + 3].uv = vector2df(uv.x, uv.w);

	_dirty = true;
}

size_t Mesh::get_quad_count() const
{
	return _vertices.size() / 4;
}

bool Mesh::get_quad_position(size_t quad, vector2df& pos1, vector2df& pos2, vector2df& pos3, vector2df& pos4) const
{
	if (quad < get_quad_count())
	{
		size_t index = quad * 4;

		pos1 = _vertices[index].pos;
		pos2 = _vertices[index + 1].pos;
		pos3 = _vertices[index + 2].pos;
		pos4 = _vertices[index + 3].pos;

		return true;
	}

	return false;
}

void Mesh::clear_buffers()
{
	// Clear our buffers
	if (VALID_BUFFER(_vbo))
		glDeleteBuffers(1, &_vbo);

	if (VALID_BUFFER(_ebo))
		glDeleteBuffers(1, &_ebo);

	// Reset the buffer pointers to null
	_vbo = 0;
	_ebo = 0;
}