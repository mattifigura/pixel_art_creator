#ifndef IMAGE_LAYER_H
#define IMAGE_LAYER_H

#include "GUIElement.h"

class ImageLayer : public GUIElement {
public:
	ImageLayer(const vector2df& pos, const vector2df& size, GUIElement* parent = nullptr);
	~ImageLayer();

	virtual void build();

	void clear_to_colour(const GLColour& col);
	void load_from_buffer(const std::vector<GLColour>& data, int width, int height);
	void load_from_buffer(const GLColour* data, int width, int height);

	void set_pixel(int x, int y, const GLColour& col);
	const GLColour& get_pixel(int x, int y) const;

	void buffer();

	virtual void set_size(const vector2df& size);

protected:

private:
	std::unique_ptr<Texture> _layer;
};

#endif